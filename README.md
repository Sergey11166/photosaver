## Приложение для удобной отправки фото в различные типы хранилищ. [Google Play](https://play.google.com/store/apps/details?id=ru.sevoro.photosaver) ##

## Используемые технологии: ##
- MVP
- Kotlin
- Dagger2
- RxJava2
- Unit Tests
- UI tests (Espresso)
- Circle CI (Docker, Firebase Test Lab)
- Firebase Remote Config
- Play Billing Library