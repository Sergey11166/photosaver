package net.gotev.uploadservice;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;

import java.util.ArrayList;

import static net.gotev.uploadservice.BitmapUtils.decodeFilePathToBitmap;
import static net.gotev.uploadservice.BitmapUtils.decodeResourceIdToBitmap;

/**
 * @author Aleksandar Gotev
 */

public class UploadNotificationStatusConfig implements Parcelable {

    public static final Creator<UploadNotificationStatusConfig> CREATOR = new Creator<UploadNotificationStatusConfig>() {
        @Override
        public UploadNotificationStatusConfig createFromParcel(Parcel source) {
            return new UploadNotificationStatusConfig(source);
        }

        @Override
        public UploadNotificationStatusConfig[] newArray(int size) {
            return new UploadNotificationStatusConfig[size];
        }
    };

    /**
     * Notification title.
     */
    public String title = "File Upload";

    /**
     * Notification message.
     */
    public String message;

    /**
     * Notification sub text/ See {@link Notification.Builder#setSubText(CharSequence)}
     */
    public String subText;

    /**
     * Summary notification sub text/ See {@link Notification.Builder#setSubText(CharSequence)}
     */
    public String summarySubText;

    /**
     * Clear the notification automatically.
     * This would not affect progress notification, as it's ongoing and managed by upload service.
     * It's used to be able to automatically dismiss cancelled, error or completed notifications.
     */
    public boolean autoClear;

    /**
     * Notification icon.
     */
    public int iconResID = android.R.drawable.ic_menu_upload;

    /**
     * Resource id of large notification icon
     */
    public int largeIconResId;

    /**
     * Path to file of large notification icon
     */
    public String largeIconPath;

    /**
     * Icon color tint.
     */
    public int colorResId = NotificationCompat.COLOR_DEFAULT;

    /**
     * Intent to be performed when the user taps on the notification.
     */
    public PendingIntent clickIntent;

    /**
     * Clear the notification automatically when the clickIntent is performed.
     * This would not affect progress notification, as it's ongoing and managed by upload service.
     */
    public boolean clearOnAction;

    /**
     * Notification group. See {@link NotificationCompat.Builder#setGroup(String)}
     */
    public String group = BuildConfig.APPLICATION_ID;

    /**
     * Group summary. See {@link Notification.Builder#setGroupSummary(boolean)}
     */
    public boolean groupSummary;

    /**
     * Summary notification Id
     */
    public int summaryId;

    /**
     * List of actions to be added to this notification.
     */
    public ArrayList<UploadNotificationAction> actions = new ArrayList<>(3);

    public UploadNotificationStatusConfig() {
    }

    protected UploadNotificationStatusConfig(Parcel in) {
        this.title = in.readString();
        this.message = in.readString();
        this.subText = in.readString();
        this.summarySubText = in.readString();
        this.autoClear = in.readByte() != 0;
        this.clearOnAction = in.readByte() != 0;
        this.largeIconResId = in.readInt();
        this.largeIconPath = in.readString();
        this.iconResID = in.readInt();
        this.colorResId = in.readInt();
        this.clickIntent = in.readParcelable(PendingIntent.class.getClassLoader());
        this.group = in.readString();
        this.groupSummary = in.readByte() != 0;
        this.summaryId = in.readInt();
        this.actions = in.createTypedArrayList(UploadNotificationAction.CREATOR);
    }

    final PendingIntent getClickIntent(Context context) {
        if (clickIntent == null) {
            return PendingIntent.getBroadcast(context, 0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);
        }

        return clickIntent;
    }

    final void addActionsToNotificationBuilder(NotificationCompat.Builder builder) {
        if (actions != null && !actions.isEmpty()) {
            for (UploadNotificationAction notificationAction : actions) {
                builder.addAction(notificationAction.toAction());
            }
        }
    }

    final void addLargeIconToNotificationBuilder(Context context, NotificationCompat.Builder notification, UploadNotificationStatusConfig statusConfig, int largeIconSize) {
        if (!statusConfig.largeIconPath.isEmpty()) {
            try {
                notification.setLargeIcon(decodeFilePathToBitmap(context, largeIconSize, largeIconSize, statusConfig.largeIconPath));
                return;
            } catch (Exception e) {
                Logger.error("DecodingFileToBitmap", "Error decode file to Bitmap. File path: '" + statusConfig.largeIconPath + "'");
            }
        }

        if (statusConfig.largeIconResId > 0) {
            notification.setLargeIcon(decodeResourceIdToBitmap(context, largeIconSize, largeIconSize, statusConfig.largeIconResId));
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.message);
        dest.writeString(this.subText);
        dest.writeString(this.summarySubText);
        dest.writeByte(this.autoClear ? (byte) 1 : (byte) 0);
        dest.writeByte(this.clearOnAction ? (byte) 1 : (byte) 0);
        dest.writeInt(this.largeIconResId);
        dest.writeString(this.largeIconPath);
        dest.writeInt(this.iconResID);
        dest.writeInt(this.colorResId);
        dest.writeParcelable(this.clickIntent, flags);
        dest.writeString(this.group);
        dest.writeInt(this.groupSummary ? (byte) 1 : (byte) 0);
        dest.writeInt(this.summaryId);
        dest.writeTypedList(this.actions);
    }
}
