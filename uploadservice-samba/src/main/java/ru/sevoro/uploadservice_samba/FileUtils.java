package ru.sevoro.uploadservice_samba;

import java.io.BufferedInputStream;
import java.io.IOException;

import jcifs.smb.SmbFileOutputStream;

/**
 * @author Sergey Vorobyev
 */

public class FileUtils {

    private FileUtils() {
    }

    public static void copyStream(BufferedInputStream src,
                                  SmbFileOutputStream dest,
                                  ProgressListener progressListener) throws IOException {

        byte[] buf = new byte[1024 * 8];
        int len;
        long readBytes = 0;
        while ((len = src.read(buf)) > 0) {
            readBytes += len;
            dest.write(buf, 0, len);
            progressListener.update(readBytes);
        }
    }

    public interface ProgressListener {
        void update(long readBytes) throws IOException;
    }
}
