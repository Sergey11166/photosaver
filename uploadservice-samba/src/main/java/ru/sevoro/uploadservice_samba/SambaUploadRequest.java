package ru.sevoro.uploadservice_samba;

import android.content.Context;
import android.content.Intent;

import net.gotev.uploadservice.UploadFile;
import net.gotev.uploadservice.UploadRequest;
import net.gotev.uploadservice.UploadTask;

import java.io.FileNotFoundException;

/**
 * @author Sergey Vorobyev
 */

public class SambaUploadRequest extends UploadRequest<SambaUploadRequest> {

    private SambaUploadTaskParameters sambaParams = new SambaUploadTaskParameters();

    /**
     * Creates a new upload request.
     *
     * @param context  application context
     * @param uploadId unique ID to assign to this upload request. If is null or empty, a random
     *                 UUID will be automatically generated. It's used in the broadcast receiver
     *                 when receiving updates.
     * @param path     path to the shared folder in the local network
     * @throws IllegalArgumentException if one or more arguments are not valid
     */
    public SambaUploadRequest(Context context, String uploadId, String path) throws IllegalArgumentException {
        super(context, uploadId, path);
    }

    @Override
    protected Class<? extends UploadTask> getTaskClass() {
        return SambaUploadTask.class;
    }

    @Override
    protected void initializeIntent(Intent intent) {
        super.initializeIntent(intent);
        intent.putExtra(SambaUploadTaskParameters.PARAM_SAMBA_TASK_PARAMETERS, sambaParams);
    }

    /**
     * Setup domain of local network
     *
     * @param domain of local network
     * @return {@link SambaUploadRequest}
     */
    public SambaUploadRequest setDomain(final String domain) {
        if (domain == null || domain.equals("")) {
            throw new IllegalStateException("Specify Domain of local network");
        }

        sambaParams.domain = domain;
        return this;
    }


    /**
     * Set the credentials used to login on the shared folder.
     *
     * @param username account username
     * @param password account password
     * @return {@link SambaUploadRequest}
     */
    public SambaUploadRequest setUsernameAndPassword(final String username, final String password) {
        if (username == null || "".equals(username)) {
            throw new IllegalArgumentException("Specify account username!");
        }

        if (password == null || "".equals(password)) {
            throw new IllegalArgumentException("Specify account password!");
        }

        sambaParams.username = username;
        sambaParams.password = password;
        return this;
    }

    /**
     * Add file to be uploaded
     *
     * @param filePath path to the local file on the device
     * @param subDir   sub directory in shared folder
     * @return {@link SambaUploadRequest}
     * @throws FileNotFoundException if the local file does not exist
     */
    public SambaUploadRequest addFileToUpload(final String filePath, final String subDir) throws FileNotFoundException {
        final UploadFile file = new UploadFile(filePath);

        if (subDir != null) {
            file.setProperty(SambaUploadTask.PARAM_SUB_DIRECTORY, subDir);
        }

        params.files.add(file);
        return this;
    }

    @Override
    public String startUpload() {
        if (params.files.isEmpty()) {
            throw new IllegalArgumentException("Add at least one file to start Samba upload!");
        }

        return super.startUpload();
    }
}
