package ru.sevoro.uploadservice_samba;

import android.content.Intent;
import android.util.Log;

import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadFile;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.UploadTask;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

/**
 * Implements the SAMBA upload logic.
 *
 * @author Sergey Vorobyev
 */
public class SambaUploadTask extends UploadTask {

    static final String PARAM_SUB_DIRECTORY = "PARAM_SUB_DIRECTORY";
    private final String TAG = "SambaUploadTask";
    private SambaUploadTaskParameters sambaParams = null;

    @Override
    protected void init(UploadService service, Intent intent) throws IOException {
        super.init(service, intent);
        sambaParams = intent.getParcelableExtra(SambaUploadTaskParameters.PARAM_SAMBA_TASK_PARAMETERS);
        Log.d(TAG, "sambaParams: " + sambaParams);
    }

    @Override
    protected void upload() throws Exception {
        UploadFile uploadFile = params.files.get(0);
        String filePath = uploadFile.getPath();
        final String subDir = uploadFile.getProperty(PARAM_SUB_DIRECTORY);
        String[] splitFilePath = filePath.split("/");
        String fileName = splitFilePath[splitFilePath.length - 1];
        String pathDir = subDir != null && !subDir.isEmpty() ?
                "smb://" + params.serverUrl + "/" + subDir :
                "smb://" + params.serverUrl;

        Log.d(TAG, "filePath: " + filePath);
        Log.d(TAG, "fileName: " + fileName);
        Log.d(TAG, "pathDir: " + pathDir);
        Log.d(TAG, "subDir: " + subDir);
        Log.d(TAG, "domain: " + sambaParams.domain);
        Log.d(TAG, "username: " + sambaParams.username);
        Log.d(TAG, "password: " + sambaParams.password);

        NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(
                sambaParams.domain,
                sambaParams.username,
                sambaParams.password);

        final File file = new File(filePath);
        BufferedInputStream bis = null;
        SmbFileOutputStream sfos = null;
        totalBytes = file.length();

        try {

            final SmbFile dir = new SmbFile(pathDir, auth);

            if (!dir.exists()) {
                dir.mkdirs();
                Log.d(TAG, "dir created: " + dir.getPath());
            }

            bis = new BufferedInputStream(new FileInputStream(file));
            final SmbFile outFile = new SmbFile(pathDir + "/" + fileName, auth);
            Log.d(TAG, "outFile created: " + outFile.getPath());
            sfos = new SmbFileOutputStream(outFile);

            Log.d(TAG, "start write file: " + file.getPath());
            final SmbFileOutputStream finalSfos = sfos;
            FileUtils.copyStream(bis, sfos, new FileUtils.ProgressListener() {
                @Override
                public void update(long readBytes) throws IOException {
                    if (!shouldContinue) { // operation has been canceled
                        finalSfos.close();
                        outFile.delete();
                        if (subDir != null && !subDir.isEmpty()) {
                            dir.delete();
                        }
                    } else {
                        broadcastProgress(readBytes, totalBytes);
                    }
                }
            });

            sfos.flush();
            Log.d(TAG, "send file success: " + file.getPath());

            // Broadcast completion only if the user has not cancelled the operation.
            if (shouldContinue) {
                addSuccessfullyUploadedFile(uploadFile);
                broadcastCompleted(new ServerResponse(TASK_COMPLETED_SUCCESSFULLY, EMPTY_RESPONSE, null));
            }
        } finally {
            if (bis != null) {
                bis.close();
            }

            if (sfos != null) {
                sfos.close();
            }
        }
    }
}
