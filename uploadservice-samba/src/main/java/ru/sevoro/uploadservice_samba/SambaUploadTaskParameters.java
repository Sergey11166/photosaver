package ru.sevoro.uploadservice_samba;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * SAMBA upload parameters.
 *
 * @author Sergey Vorobyev
 */
public class SambaUploadTaskParameters implements Parcelable {

    public static final Creator<SambaUploadTaskParameters> CREATOR = new Creator<SambaUploadTaskParameters>() {
        @Override
        public SambaUploadTaskParameters createFromParcel(Parcel in) {
            return new SambaUploadTaskParameters(in);
        }

        @Override
        public SambaUploadTaskParameters[] newArray(int size) {
            return new SambaUploadTaskParameters[size];
        }
    };
    static final String PARAM_SAMBA_TASK_PARAMETERS = "sambaTaskParameters";
    String domain;
    String username;
    String password;

    SambaUploadTaskParameters() {
    }

    protected SambaUploadTaskParameters(Parcel in) {
        domain = in.readString();
        username = in.readString();
        password = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(domain);
        dest.writeString(username);
        dest.writeString(password);
    }

    @Override
    public String toString() {
        return "SambaUploadTaskParameters{" +
                "domain='" + domain + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
