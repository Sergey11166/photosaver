package ru.sevoro.photosaver.data.config

/**
 * @author Sergey Vorobyev
 */

const val TYPE_LAN = "TYPE_LAN"
const val TYPE_FTP = "TYPE_FTP"
const val TYPE_HTTP = "TYPE_HTTP"
const val TYPE_GDRIVE = "TYPE_GDRIVE"

abstract class Config(
        val alias: String,
        val type: String,
        val path: String,
        var subDirChecked: Boolean,
        var subDirText: String) {

    var id: String = ""
    var created: Long = 0

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Config) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}