package ru.sevoro.photosaver.data.events

import android.app.PendingIntent
import android.content.Context
import android.content.Intent

import ru.sevoro.photosaver.BuildConfig

/**
 * @author Aleksandar Gotev
 */

const val INTENT_ACTION = BuildConfig.APPLICATION_ID + ".notification.action"

const val PARAM_ACTION = "action"
const val PARAM_UPLOAD_ID = "uploadId"
const val ACTION_CANCEL_UPLOAD = "cancelUpload"

fun getCancelUploadAction(context: Context, requestCode: Int, uploadID: String): PendingIntent {
    val intent = Intent(INTENT_ACTION)
    intent.putExtra(PARAM_ACTION, ACTION_CANCEL_UPLOAD)
    intent.putExtra(PARAM_UPLOAD_ID, uploadID)

    return PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT)
}
