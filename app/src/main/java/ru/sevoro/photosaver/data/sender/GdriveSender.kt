package ru.sevoro.photosaver.data.sender

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.config.Config
import ru.sevoro.photosaver.data.config.GdriveConfig
import ru.sevoro.photosaver.ui.screen.settings.KEY_PREF_DELETE_SUCCESS_UPLOADED
import ru.sevoro.uploadservice_gdrive.GdriveUploadRequest
import java.util.*


/**
 * @author Sergey Vorobyev
 */
private const val TAG = "GdriveSender"
private const val GDRIVE_SUMMARY_ID = 3
private const val GDRIVE_SUMMARY_SUFFIX = "GDRIVE"

class GdriveSender(
        localStorage: LocalStorage,
        private val context: Context,
        private val prefs: SharedPreferences
) : AbsSender(context, localStorage, prefs), Sender {

    override fun send(filePathList: List<String>, config: Config, subDir: String) {
        if (config !is GdriveConfig) return

        Log.d(TAG, "filePathList: $filePathList")
        Log.d(TAG, "subDir: $subDir")
        Log.d(TAG, "config: $config")

        for (filePath in filePathList) {
            val fileName = filePath.split("/").last()
            Log.d(TAG, "fileName: $fileName")

            val uploadId = UUID.randomUUID().toString()

            GdriveUploadRequest(context, uploadId, config.folderId)
                    .setNotificationConfig(createNotificationConfig(uploadId, fileName, filePath))
                    .addFileToUpload(filePath, subDir)
                    .setAccount(config.account)
                    .setAutoDeleteFilesAfterSuccessfulUpload(prefs.getBoolean(KEY_PREF_DELETE_SUCCESS_UPLOADED, false))
                    .startUpload()
        }
    }

    override fun getNotificationTitle(): String = context.getString(R.string.notification_title_gdrive_upload)

    override fun getNotificationLargeIconResId() = R.drawable.ic_folder_gdrive_accent

    override fun getNotificationSummaryGroupSuffix() = GDRIVE_SUMMARY_SUFFIX

    override fun getNotificationSummaryId() = GDRIVE_SUMMARY_ID

    override fun isAllowNotificationCancel(): Boolean = false
}