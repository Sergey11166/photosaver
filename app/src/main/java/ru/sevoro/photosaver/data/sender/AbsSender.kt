package ru.sevoro.photosaver.data.sender

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import net.gotev.uploadservice.UploadNotificationAction
import net.gotev.uploadservice.UploadNotificationConfig
import net.gotev.uploadservice.UploadNotificationStatusConfig
import ru.sevoro.photosaver.BuildConfig
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.events.getCancelUploadAction
import ru.sevoro.photosaver.ui.screen.main.MainActivity
import ru.sevoro.photosaver.ui.screen.settings.KEY_PREF_SUMMARY_NOTIFICATIONS

/**
 * @author Sergey Vorobyev
 */
abstract class AbsSender(
        private val context: Context,
        private val localStorage: LocalStorage,
        private val prefs: SharedPreferences) {

    protected fun createNotificationConfig(uploadId: String, fileName: String,
                                           filePath: String): UploadNotificationConfig {

        val title = getNotificationTitle()
        val config = UploadNotificationConfig()

        val clickIntent = PendingIntent.getActivity(context,
                localStorage.getUniqueInt(),
                Intent(context, MainActivity::class.java),
                PendingIntent.FLAG_UPDATE_CURRENT)

        config.setTitleForAllStatuses(title)
                .setSubTextForAllStatuses(fileName)
                .setIconForAllStatuses(R.drawable.ic_notifications)
                .setLargeIconResIdForAllStatuses(getNotificationLargeIconResId())
                .setLargeIconPathForAllStatuses(filePath)
                .setIconColorForAllStatuses(ContextCompat.getColor(context, R.color.color_notification))
                .setRingToneEnabled(true)
                .setClickIntentForAllStatuses(clickIntent)
                .setClearOnActionForAllStatuses(true)

        config.progress.message = context.getString(R.string.notification_message_progress)
        if (isAllowNotificationCancel()) {
            config.progress.actions.add(UploadNotificationAction(
                    android.R.drawable.ic_menu_close_clear_cancel,
                    context.getString(android.R.string.cancel),
                    getCancelUploadAction(context, localStorage.getUniqueInt(), uploadId)))
        }

        config.completed.message = context.getString(R.string.notification_message_success)
        config.error.message = context.getString(R.string.notification_message_error)
        config.cancelled.message = context.getString(R.string.notification_message_cancelled)

        if (Build.VERSION.SDK_INT >= 24 && prefs.getBoolean(KEY_PREF_SUMMARY_NOTIFICATIONS, false)) {
            addSummaryToNotificationConfig(title, config.progress, clickIntent)
            addSummaryToNotificationConfig(title, config.completed, clickIntent)
            addSummaryToNotificationConfig(title, config.error, clickIntent)
            addSummaryToNotificationConfig(title, config.cancelled, clickIntent)
        }

        return config
    }

    protected open fun isAllowNotificationCancel(): Boolean = true

    private fun addSummaryToNotificationConfig(title: String, config: UploadNotificationStatusConfig, clickIntent: PendingIntent) {
        config.group = "${BuildConfig.APPLICATION_ID}_${getNotificationSummaryGroupSuffix()}"
        config.groupSummary = true
        config.summarySubText = title
        config.summaryId = getNotificationSummaryId()
        config.clickIntent = clickIntent
    }

    abstract fun getNotificationTitle(): String

    @DrawableRes
    abstract fun getNotificationLargeIconResId(): Int

    abstract fun getNotificationSummaryGroupSuffix(): String

    abstract fun getNotificationSummaryId(): Int
}