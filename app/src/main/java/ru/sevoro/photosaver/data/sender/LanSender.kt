package ru.sevoro.photosaver.data.sender

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.config.Config
import ru.sevoro.photosaver.data.config.LanConfig
import ru.sevoro.photosaver.ui.screen.settings.KEY_PREF_DELETE_SUCCESS_UPLOADED
import ru.sevoro.uploadservice_samba.SambaUploadRequest
import java.util.*

/**
 * @author Sergey Vorobyev
 */
private const val TAG = "LanSender"
private const val LAN_SUMMARY_ID = 0
private const val LAN_SUMMARY_SUFFIX = "LAN"

class LanSender(
        localStorage: LocalStorage,
        private val context: Context,
        private val prefs: SharedPreferences
) : AbsSender(context, localStorage, prefs), Sender {

    override fun send(filePathList: List<String>, config: Config, subDir: String) {
        if (config !is LanConfig) return

        Log.d(TAG, "filePathList: $filePathList")
        Log.d(TAG, "subDir: $subDir")
        Log.d(TAG, "config: $config")

        for (filePath in filePathList) {
            val fileName = filePath.split("/").last()
            Log.d(TAG, "fileName: $fileName")

            val uploadId = UUID.randomUUID().toString()

            val request = SambaUploadRequest(context, uploadId, config.path)
                    .setNotificationConfig(createNotificationConfig(uploadId, fileName, filePath))
                    .addFileToUpload(filePath, subDir)
                    .setAutoDeleteFilesAfterSuccessfulUpload(prefs.getBoolean(KEY_PREF_DELETE_SUCCESS_UPLOADED, false))

            if (config.domain.isNotEmpty()) {
                request.setDomain(config.domain)
            }

            if (config.username.isNotEmpty() && config.password.isNotEmpty()) {
                request.setUsernameAndPassword(config.username, config.password)
            }

            request.startUpload()
        }
    }

    override fun getNotificationTitle(): String = context.getString(R.string.notification_title_lan_upload)

    override fun getNotificationLargeIconResId() = R.drawable.ic_folder_shared_accent

    override fun getNotificationSummaryGroupSuffix() = LAN_SUMMARY_SUFFIX

    override fun getNotificationSummaryId() = LAN_SUMMARY_ID
}