package ru.sevoro.photosaver.data.config

/**
 * @author Sergey Vorobyev
 */

const val AUTH_NONE = "None"
const val AUTH_BASIC = "Basic"

const val HTTP_METHOD_POST = "POST"
const val HTTP_METHOD_PUT = "PUT"
const val HTTP_METHOD_PATCH = "PATCH"
const val HTTP_METHOD_OPTIONS = "OPTIONS"

class HttpConfig(
        alias: String,
        path: String,
        val formParam: String = "",
        val authType: String = AUTH_NONE,
        val httpMethod: String = HTTP_METHOD_POST,
        val username: String = "",
        val password: String = "",
        val headers: MutableList<String> = mutableListOf()
) : Config(alias, TYPE_HTTP, path, false, "") {

    override fun toString(): String {
        return "HttpConfig(" +
                "alias='$alias', " +
                "path='$path', " +
                "formParam='$formParam'," +
                "authType='$authType'," +
                "httpMethod='$httpMethod'," +
                "username='$username'," +
                "password='$password'," +
                "headers=$headers" +
                ")"
    }
}