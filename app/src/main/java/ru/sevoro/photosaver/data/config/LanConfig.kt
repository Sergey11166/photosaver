package ru.sevoro.photosaver.data.config

/**
 * @author Sergey Vorobyev
 */
class LanConfig(
        alias: String,
        path: String,
        subDirChecked: Boolean = false,
        subDirText: String = "",
        val domain: String = "",
        val username: String = "",
        val password: String = ""
) : Config(alias, TYPE_LAN, path, subDirChecked, subDirText) {

    override fun toString(): String {
        return "LanConfig(" +
                "alias='$alias', " +
                "path='$path', " +
                "subDirChecked='$subDirChecked', " +
                "subDirText='$subDirText', " +
                "domain='$domain', " +
                "username='$username', " +
                "password='$password')"
    }
}