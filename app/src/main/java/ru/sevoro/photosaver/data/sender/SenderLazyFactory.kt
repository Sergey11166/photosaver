package ru.sevoro.photosaver.data.sender

/**
 * @author Sergey Vorobyev
 */
interface SenderLazyFactory {

    fun getSender(type: String): Sender
}