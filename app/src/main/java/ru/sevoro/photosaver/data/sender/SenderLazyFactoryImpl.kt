package ru.sevoro.photosaver.data.sender

import ru.sevoro.photosaver.data.config.TYPE_FTP
import ru.sevoro.photosaver.data.config.TYPE_GDRIVE
import ru.sevoro.photosaver.data.config.TYPE_HTTP
import javax.inject.Named

/**
 * @author Sergey Vorobyev
 */
class SenderLazyFactoryImpl(
        @Named("LanSender") private val lanSender: dagger.Lazy<Sender>,
        @Named("HttpSender") private val httpSender: dagger.Lazy<Sender>,
        @Named("FtpSender") private val ftpSender: dagger.Lazy<Sender>,
        @Named("GdriveSender") private val gdriveSender: dagger.Lazy<Sender>
) : SenderLazyFactory {

    override fun getSender(type: String): Sender {
        return when (type) {
            TYPE_HTTP -> httpSender.get()
            TYPE_FTP -> ftpSender.get()
            TYPE_GDRIVE -> gdriveSender.get()
            else -> lanSender.get()
        }
    }
}