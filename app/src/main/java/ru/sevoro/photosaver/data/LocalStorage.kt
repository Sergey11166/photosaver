package ru.sevoro.photosaver.data

import ru.sevoro.photosaver.data.config.Config

/**
 * @author Sergey Vorobyev
 */
interface LocalStorage {

    fun <T : Config> saveConfig(config: T)

    fun deleteConfig(id: String)

    fun deleteAllConfigs()

    fun <T : Config> getConfig(id: String): T

    fun getAllConfigs(): List<Config>

    fun saveSelectedConfigId(id: String)

    fun getSelectedConfigId(): String

    fun getUniqueInt(): Int
}