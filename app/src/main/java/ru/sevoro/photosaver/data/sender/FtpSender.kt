package ru.sevoro.photosaver.data.sender

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import net.gotev.uploadservice.ftp.FTPUploadRequest
import net.gotev.uploadservice.ftp.UnixPermissions
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.config.Config
import ru.sevoro.photosaver.data.config.FtpConfig
import ru.sevoro.photosaver.ui.screen.settings.KEY_PREF_DELETE_SUCCESS_UPLOADED
import java.util.*

/**
 * @author Sergey Vorobyev
 */
private const val TAG = "FtpSender"
private const val FTP_SUMMARY_ID = 1
private const val FTP_SUMMARY_SUFFIX = "FTP"

class FtpSender(
        localStorage: LocalStorage,
        private val context: Context,
        private val prefs: SharedPreferences
) : AbsSender(context, localStorage, prefs), Sender {

    override fun send(filePathList: List<String>, config: Config, subDir: String) {
        val ftpConfig = config as FtpConfig
        val splitUrl = config.path.split(":")
        val useSSL = splitUrl[0] == "ftps"
        val host = config.path.split("//")[1].split(if (splitUrl.size == 2) "/" else ":")[0]

        val port = when {
            splitUrl.size == 3 -> splitUrl[2].split("/")[0].toInt()
            splitUrl.size == 2 && useSSL -> 990
            else -> 21
        }

        Log.d(TAG, "filePathList: $filePathList")
        Log.d(TAG, "subDir: $subDir")
        Log.d(TAG, "ftpConfig: $ftpConfig")
        Log.d(TAG, "host: $host")
        Log.d(TAG, "port: $port")

        for (filePath in filePathList) {
            val fileName = filePath.split("/").last()

            Log.d(TAG, "fileName: $fileName")

            val uploadId = UUID.randomUUID().toString()

            val request = FTPUploadRequest(context, uploadId, host, port)
                    .setMaxRetries(1)
                    .setSocketTimeout(5000)
                    .setConnectTimeout(5000)
                    .useSSL(useSSL)
                    .setNotificationConfig(createNotificationConfig(uploadId, fileName, filePath))
                    .setCreatedDirectoriesPermissions(UnixPermissions("777"))
                    .addFileToUpload(filePath, "$subDir/$fileName")
                    .setAutoDeleteFilesAfterSuccessfulUpload(prefs.getBoolean(KEY_PREF_DELETE_SUCCESS_UPLOADED, false))

            if (ftpConfig.username.isNotEmpty() && ftpConfig.password.isNotEmpty()) {
                request.setUsernameAndPassword(config.username, config.password)
            }

            request.startUpload()
        }
    }

    override fun getNotificationTitle(): String = context.getString(R.string.notification_title_ftp_upload)

    override fun getNotificationLargeIconResId() = R.drawable.ic_folder_ftp_accent

    override fun getNotificationSummaryGroupSuffix() = FTP_SUMMARY_SUFFIX

    override fun getNotificationSummaryId() = FTP_SUMMARY_ID
}