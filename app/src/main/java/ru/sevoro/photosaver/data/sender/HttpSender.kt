package ru.sevoro.photosaver.data.sender

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import net.gotev.uploadservice.MultipartUploadRequest
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.config.AUTH_BASIC
import ru.sevoro.photosaver.data.config.Config
import ru.sevoro.photosaver.data.config.HttpConfig
import ru.sevoro.photosaver.ui.screen.settings.KEY_PREF_DELETE_SUCCESS_UPLOADED
import java.util.*

/**
 * @author Sergey Vorobyev
 */
private const val TAG = "HttpSender"
private const val HTTP_SUMMARY_ID = 2
private const val HTTP_SUMMARY_SUFFIX = "HTTP"

class HttpSender(
        localStorage: LocalStorage,
        private val context: Context,
        private val prefs: SharedPreferences
) : AbsSender(context, localStorage, prefs), Sender {

    override fun send(filePathList: List<String>, config: Config, subDir: String) {
        val httpConfig = config as HttpConfig

        Log.d(TAG, "httpConfig = $httpConfig")

        for (filePath in filePathList) {
            val uploadId = UUID.randomUUID().toString()
            val fileName = filePath.split("/").last()

            val request = MultipartUploadRequest(context, uploadId, httpConfig.path)
                    .setMethod(httpConfig.httpMethod)
                    .setUtf8Charset()
                    .setMaxRetries(1)
                    .setUsesFixedLengthStreamingMode(true)
                    .addFileToUpload(filePath, httpConfig.formParam)
                    .setNotificationConfig(createNotificationConfig(uploadId, fileName, filePath))
                    .setAutoDeleteFilesAfterSuccessfulUpload(prefs.getBoolean(KEY_PREF_DELETE_SUCCESS_UPLOADED, false))

            httpConfig.headers
                    .filter { it.isNotEmpty() }
                    .map { it.split(":") }
                    .forEach { request.addHeader(it[0], it[1]) }

            when (httpConfig.authType) {
                AUTH_BASIC -> request.setBasicAuth(httpConfig.username, httpConfig.password)
            }

            request.startUpload()
        }
    }

    override fun getNotificationTitle(): String = context.getString(R.string.notification_title_web_upload)

    override fun getNotificationLargeIconResId() = R.drawable.ic_folder_http_accent

    override fun getNotificationSummaryGroupSuffix() = HTTP_SUMMARY_SUFFIX

    override fun getNotificationSummaryId() = HTTP_SUMMARY_ID
}