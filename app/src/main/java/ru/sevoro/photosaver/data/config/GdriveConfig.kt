package ru.sevoro.photosaver.data.config

/**
 * @author Sergey Vorobyev
 */
class GdriveConfig(
        alias: String,
        path: String,
        subDirChecked: Boolean = false,
        subDirText: String = "",
        val account: String = "",
        var folderId: String = "root"
) : Config(alias, TYPE_GDRIVE, path, subDirChecked, subDirText) {

    override fun toString(): String {
        return "GdriveConfig(" +
                "alias='$alias', " +
                "path='$path', " +
                "subDirChecked='$subDirChecked', " +
                "subDirText='$subDirText', " +
                "account='$account'" +
                "folderId='$folderId'" +
                ")"
    }
}