package ru.sevoro.photosaver.data.events

import android.content.Context
import android.util.Log

import net.gotev.uploadservice.ServerResponse
import net.gotev.uploadservice.UploadInfo
import net.gotev.uploadservice.UploadServiceBroadcastReceiver

/**
 * This implementation is empty on purpose, just to show how it's possible to intercept
 * all the upload events app-wise with a global broadcast receiver registered in the manifest.
 *
 * @author Aleksandar Gotev
 */

private const val TAG = "UploadReceiver"

class UploadReceiver : UploadServiceBroadcastReceiver() {
    override fun onProgress(context: Context, uploadInfo: UploadInfo) {

    }

    override fun onError(context: Context, uploadInfo: UploadInfo, serverResponse: ServerResponse?,
                         e: Exception?) {

        Log.e(TAG, "onError", e)
    }

    override fun onCompleted(context: Context, uploadInfo: UploadInfo, serverResponse: ServerResponse?) {
        Log.d(TAG, "onCompleted")
    }

    override fun onCancelled(context: Context, uploadInfo: UploadInfo) {
        Log.d(TAG, "onCancelled")
    }
}
