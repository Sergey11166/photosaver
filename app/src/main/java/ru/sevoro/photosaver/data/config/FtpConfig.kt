package ru.sevoro.photosaver.data.config

/**
 * @author Sergey Vorobyev
 */
class FtpConfig(
        alias: String,
        path: String,
        subDirChecked: Boolean = false,
        subDirText: String = "",
        val username: String = "",
        val password: String = ""
) : Config(alias, TYPE_FTP, path, subDirChecked, subDirText) {

    override fun toString(): String {
        return "FtpConfig(" +
                "alias='$alias', " +
                "path='$path', " +
                "subDirChecked='$subDirChecked', " +
                "subDirText='$subDirText', " +
                "username='$username', " +
                "password='$password')"
    }
}