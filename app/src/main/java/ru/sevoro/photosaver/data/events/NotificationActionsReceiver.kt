package ru.sevoro.photosaver.data.events

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

import net.gotev.uploadservice.UploadService

private const val TAG = "NotifActionsReceiver"

class NotificationActionsReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        if (intent == null || INTENT_ACTION != intent.action) {
            return
        }

        if (ACTION_CANCEL_UPLOAD == intent.getStringExtra(PARAM_ACTION)) {
            onUserRequestedUploadCancellation(intent.getStringExtra(PARAM_UPLOAD_ID))
        }
    }

    private fun onUserRequestedUploadCancellation(uploadId: String) {
        Log.e(TAG, "User requested cancellation of upload with ID: " + uploadId)
        UploadService.stopUpload(uploadId)
    }
}
