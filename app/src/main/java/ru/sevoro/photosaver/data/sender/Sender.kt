package ru.sevoro.photosaver.data.sender

import ru.sevoro.photosaver.data.config.Config

/**
 * @author Sergey Vorobyev
 */

interface Sender {

    fun send(filePathList: List<String>, config: Config, subDir: String)
}