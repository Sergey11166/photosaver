package ru.sevoro.photosaver

import android.content.Context
import android.content.SharedPreferences
import dagger.Component
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.sender.SenderLazyFactory
import ru.sevoro.photosaver.helper.BillingHelper
import ru.sevoro.photosaver.helper.GdriveHelper
import ru.sevoro.photosaver.helper.RemoteConfigHelper
import ru.sevoro.photosaver.util.Toaster
import javax.inject.Singleton

/**
 * @author Sergey Vorobyev
 */

@Singleton
@Component(modules = [(AppModule::class), (SendersModule::class)])
interface AppComponent {

    fun appContext(): Context

    fun localStorage(): LocalStorage

    fun gdriveHelper(): GdriveHelper

    fun prefs(): SharedPreferences

    fun toaster(): Toaster

    fun senderFactory(): SenderLazyFactory

    fun remoteConfigHelper(): RemoteConfigHelper

    fun billingHelper(): BillingHelper
}