package ru.sevoro.photosaver

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.sender.*
import javax.inject.Named
import javax.inject.Singleton


/**
* @author Sergey Vorobyev
*/
@Module
class SendersModule {

    @Provides
    @Singleton
    @Named("LanSender")
    fun provideLanSender(context: Context, localStorage: LocalStorage, prefs: SharedPreferences): Sender {
        return LanSender(localStorage, context, prefs)
    }

    @Provides
    @Singleton
    @Named("HttpSender")
    fun provideHttpSender(context: Context, localStorage: LocalStorage, prefs: SharedPreferences): Sender {
        return HttpSender(localStorage, context, prefs)
    }

    @Provides
    @Singleton
    @Named("FtpSender")
    fun provideFtpSender(context: Context, localStorage: LocalStorage, prefs: SharedPreferences): Sender {
        return FtpSender(localStorage, context, prefs)
    }

    @Provides
    @Singleton
    @Named("GdriveSender")
    fun provideGdriveSender(context: Context, localStorage: LocalStorage, prefs: SharedPreferences): Sender {
        return GdriveSender(localStorage, context, prefs)
    }

    @Provides
    @Singleton
    fun provideSenderLazyFactory(@Named("LanSender") lanSender: dagger.Lazy<Sender>,
                                 @Named("HttpSender") httpSender: dagger.Lazy<Sender>,
                                 @Named("FtpSender") ftpSender: dagger.Lazy<Sender>,
                                 @Named("GdriveSender") gdriveSender: dagger.Lazy<Sender>
    ): SenderLazyFactory {
        return SenderLazyFactoryImpl(lanSender, httpSender, ftpSender, gdriveSender)
    }
}