package ru.sevoro.photosaver.helper

import android.accounts.AccountsException
import android.content.Context
import android.util.Log
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.google.api.services.drive.model.File
import io.reactivex.Single

/**
 * @author Sergey Vorobyev
 */
private const val TAG = "GdriveHelperImpl"

class GdriveHelperImpl(private val context: Context) : GdriveHelper {

    override fun getFolderIdFromPath(folderPath: String, account: String): Single<String> {
        return Single.fromCallable {
            val credential = GoogleAccountCredential
                    .usingOAuth2(context, listOf(DriveScopes.DRIVE_FILE))
                    .setBackOff(ExponentialBackOff())
                    .setSelectedAccountName(account)

            var available = false
            for (acc in credential.allAccounts) {
                if (acc.name == account) {
                    Log.d(TAG, "account '$account' is available")
                    available = true
                    break
                }
            }
            if (!available) throw AccountsException("Selected account is not available")

            val drive = Drive.Builder(AndroidHttp.newCompatibleTransport(),
                    JacksonFactory.getDefaultInstance(), credential)
                    .setApplicationName("GoogleDriveUploadService")
                    .build()

            var parentId = "root"
            val folders = folderPath.split("/")

            if (folders.isEmpty()) {
                drive.files().generateIds().execute() //any request to verify auth token
            }

            for (parent in folders) {
                if (parent.isEmpty()) continue

                val search = drive.files().list()
                        .setQ("mimeType='application/vnd.google-apps.folder' and " +
                                "name='$parent' and '$parentId' in parents")
                        .setSpaces("drive")
                        .setFields("files(id)")
                        .execute()

                Log.d(TAG, "searching with parent='$parent' and parentId='$parentId' return '$search'")

                if (search.files.isEmpty()) {
                    Log.d(TAG, "parent folder '$parent' not found")

                    val meta = File()
                    meta.name = parent
                    meta.parents = listOf(parentId)
                    meta.mimeType = "application/vnd.google-apps.folder"

                    val createFolder = drive.files()
                            .create(meta)
                            .setFields("id")
                            .execute()

                    parentId = createFolder.id
                    Log.d(TAG, "parent folder '$parent' created")
                } else {
                    Log.d(TAG, "parent folder '$parent' found")
                    parentId = search.files[0].id
                }
            }
            return@fromCallable parentId
        }
    }
}