package ru.sevoro.photosaver.helper

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonParser
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.config.*
import java.util.*


/**
* @author Sergey Vorobyev
*/

const val KEY_ALL_CONFIGS = "KEY_ALL_CONFIGS"
const val KEY_SELECTED_CONFIG_ID = "KEY_SELECTED_CONFIG_ID"
const val KEY_UNIQUE_INT = "KEY_UNIQUE_INT"

class PreferencesHelper(
        private val prefs: SharedPreferences,
        private val gson: Gson
) : LocalStorage {

    private val parser = JsonParser()

    override fun <T : Config> saveConfig(config: T) {

        val configs: MutableList<Config> = getAllConfigs()
        if (config.id.isEmpty()) {
            config.id = UUID.randomUUID().toString()
            config.created = System.currentTimeMillis()
        } else {
            val existConfigs = configs.filter { it.id == config.id }
            if (existConfigs.isNotEmpty()) {
                config.created = existConfigs[0].created
                configs.remove(existConfigs[0])
            }
        }

        configs.add(config)
        saveAllConfigs(configs)
    }

    override fun deleteConfig(id: String) {
        val configs: MutableList<Config> = getAllConfigs()
        val removing: Config = getConfig(id)
        configs.remove(removing)
        saveAllConfigs(configs)
    }

    override fun deleteAllConfigs() {
        val configs: MutableList<Config> = getAllConfigs()
        configs.clear()
        saveAllConfigs(configs)
    }

    override fun <T : Config> getConfig(id: String): T {
        @Suppress("UNCHECKED_CAST")
        return getAllConfigs().first { it.id == id } as T
    }

    override fun getAllConfigs(): MutableList<Config> {
        val result = mutableListOf<Config>()
        val json = prefs.getString(KEY_ALL_CONFIGS, "")

        if (json.isNotEmpty()) {
            val jsonArray = parser.parse(json) as JsonArray
            for (item in jsonArray) {
                val type = item.asJsonObject.get("type").asString
                when (type) {
                    TYPE_LAN -> result.add(gson.fromJson(item.toString(), LanConfig::class.java))
                    TYPE_FTP -> result.add(gson.fromJson(item.toString(), FtpConfig::class.java))
                    TYPE_HTTP -> result.add(gson.fromJson(item.toString(), HttpConfig::class.java))
                    TYPE_GDRIVE -> result.add(gson.fromJson(item.toString(), GdriveConfig::class.java))
                }
            }
        }

        return result
    }

    override fun saveSelectedConfigId(id: String) {
        saveString(KEY_SELECTED_CONFIG_ID, id)
    }

    override fun getSelectedConfigId(): String {
        return prefs.getString(KEY_SELECTED_CONFIG_ID, "")
    }

    override fun getUniqueInt(): Int {
        val prevInt = prefs.getInt(KEY_UNIQUE_INT, 0)
        val nextInt = prevInt + 1
        saveInt(KEY_UNIQUE_INT, nextInt)
        return nextInt
    }

    private fun saveString(key: String, value: String) {
        val editor = prefs.edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun saveInt(key: String, value: Int) {
        val editor = prefs.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    private fun saveAllConfigs(configs: List<Config>) {
        val json = gson.toJson(configs)
        saveString(KEY_ALL_CONFIGS, json)
    }
}