package ru.sevoro.photosaver.helper

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import io.reactivex.Single

/**
 * @author Sergey Vorobyev
 */
const val REMOTE_CONFIG_AD_DISABLED = "ad_disabled"
const val REMOTE_CONFIG_AD_DISABLING_PURCHASE_SKU = "ad_disabling_purchased"
const val REMOTE_CONFIG_PRIVACY_POLICY_URL = "privacy_policy_url"

interface RemoteConfigHelper {

    fun getUpdatedConfig(): Single<FirebaseRemoteConfig>

    fun getCachedConfig(): FirebaseRemoteConfig
}