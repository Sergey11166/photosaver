package ru.sevoro.photosaver.helper

import io.reactivex.Single

/**
 * @author Sergey Vorobyev
 */
interface GdriveHelper {

    fun getFolderIdFromPath(folderPath: String, account: String): Single<String>
}