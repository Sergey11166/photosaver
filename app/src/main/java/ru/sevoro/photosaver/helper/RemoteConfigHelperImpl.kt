package ru.sevoro.photosaver.helper

import android.util.Log
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import io.reactivex.Single
import ru.sevoro.photosaver.BuildConfig

/**
 * @author Sergey Vorobyev
 */
private const val TAG = "RemoteConfigHelperImpl"

class RemoteConfigHelperImpl(
        private val remoteConfig: FirebaseRemoteConfig
) : RemoteConfigHelper {

    private var cacheExpiration: Long = 7200 // 1 hour

    init {
        if (BuildConfig.DEBUG) {
            cacheExpiration = 0
        }
    }

    override fun getUpdatedConfig(): Single<FirebaseRemoteConfig> {
        return Single.create { emitter ->
            remoteConfig.fetch(cacheExpiration)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Log.d(TAG, "fetch successful")
                            remoteConfig.activateFetched()
                        } else {
                            Log.d(TAG, "fetch error", task.exception)
                        }
                        emitter.onSuccess(remoteConfig)
                    }
        }
    }

    override fun getCachedConfig(): FirebaseRemoteConfig = remoteConfig
}