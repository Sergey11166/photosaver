package ru.sevoro.photosaver.helper

import android.app.Activity
import android.content.Context
import android.util.Log
import com.android.billingclient.api.*
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * @author Sergey Vorobyev
 */
private const val TAG = "BillingHelperImpl"

class BillingHelperImpl(context: Context) : BillingHelper, PurchasesUpdatedListener {

    private val billingClient = BillingClient.newBuilder(context).setListener(this).build()
    private val purchasesSubject = PublishSubject.create<PurchasesUpdateResult>()

    private var isConnected = false

    override fun onPurchasesUpdated(responseCode: Int, purchases: MutableList<Purchase>?) {
        purchasesSubject.onNext(PurchasesUpdateResult(responseCode, purchases))
    }

    override fun launchInAppBillingFlow(activity: Any, sku: String) {
        if (billingClient == null) {
            throw IllegalStateException("billingClient is null")
        }

        if (!isConnected) {
            Log.d(TAG, "billingClient is not connected to service")
            return
        }

        val params = BillingFlowParams.newBuilder()
                .setSku(sku)
                .setType(BillingClient.SkuType.INAPP)
                .build()

        billingClient.launchBillingFlow(activity as Activity, params)
    }

    override fun getUpdatePurchasesSubject(): Observable<PurchasesUpdateResult> {
        connectToService()
        return purchasesSubject
    }

    private fun connectToService() {
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(responseCode: Int) {
                isConnected = true
                val result = billingClient.queryPurchases(BillingClient.SkuType.INAPP)
                purchasesSubject.onNext(PurchasesUpdateResult(result.responseCode, result.purchasesList))
            }

            override fun onBillingServiceDisconnected() {
                isConnected = false
            }
        })
    }
}