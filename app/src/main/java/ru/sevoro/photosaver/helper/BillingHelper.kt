package ru.sevoro.photosaver.helper

import com.android.billingclient.api.Purchase
import io.reactivex.Observable

/**
 * @author Sergey Vorobyev
 */
interface BillingHelper {

    fun launchInAppBillingFlow(activity: Any, sku: String)

    fun getUpdatePurchasesSubject(): Observable<PurchasesUpdateResult>
}

data class PurchasesUpdateResult(
        val responseCode: Int,
        val purchases: MutableList<Purchase>?
)