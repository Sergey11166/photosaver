package ru.sevoro.photosaver

import android.support.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import com.google.android.gms.ads.MobileAds
import net.gotev.uploadservice.Logger
import net.gotev.uploadservice.UploadService

/**
 * @author Sergey Vorobyev
 */
class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        appComponent = buildDaggerComponent()
        Stetho.initializeWithDefaults(this)

        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID
        Logger.setLogLevel(Logger.LogLevel.DEBUG)
        MobileAds.initialize(this, getString(R.string.admob_app_id))
    }

    private fun buildDaggerComponent(): AppComponent {
        return DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    companion object {
        lateinit var instance: App
        lateinit var appComponent: AppComponent
    }
}