package ru.sevoro.photosaver

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.preference.PreferenceManager
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.helper.*
import ru.sevoro.photosaver.util.Toaster
import javax.inject.Singleton


/**
 * @author Sergey Vorobyev
 */

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideAppContext(): Context {
        return context
    }

    @Provides
    @Singleton
    fun provideSharedPrefs(context: Context): SharedPreferences {
        //PreferenceManager.setDefaultValues(context, R.xml.preferences, false)
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Provides
    @Singleton
    fun provideLocalStorage(prefs: SharedPreferences): LocalStorage {
        return PreferencesHelper(prefs, Gson())
    }

    @Provides
    @Singleton
    fun provideGdriveHelper(context: Context): GdriveHelper {
        return GdriveHelperImpl(context)
    }

    @Provides
    @Singleton
    fun provideToaster(context: Context): Toaster {
        return Toaster(context)
    }

    @Provides
    @Singleton
    fun provideRemoteConfigHelper(): RemoteConfigHelper {
        val remoteConfig = FirebaseRemoteConfig.getInstance()
        remoteConfig.setDefaults(R.xml.remote_config_defaults)
        return RemoteConfigHelperImpl(remoteConfig)
    }

    @Provides
    @Singleton
    fun provideBillingHelper(context: Context): BillingHelper {
        return BillingHelperImpl(context)
    }
}