package ru.sevoro.photosaver.ui.screen.ftp_config

import dagger.Component
import ru.sevoro.photosaver.AppComponent

/**
 * @author Sergey Vorobyev
 */
@FtpConfigScope
@Component(dependencies = [(AppComponent::class)], modules = [(FtpConfigModule::class)])
interface FtpConfigComponent {
    fun inject(activity: FtpConfigActivity)
}