package ru.sevoro.photosaver.ui.screen.gdrive_config

import ru.sevoro.photosaver.ui.screen.base.ConfigBasePresenter

/**
 * @author Sergey Vorobyev
 */
interface GdriveConfigPresenter : ConfigBasePresenter<GdriveConfigView> {

    fun onAccountSelected(account: String)

    fun onEditAccountClick()

    fun onReturnFromPlayServices()
}