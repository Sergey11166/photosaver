package ru.sevoro.photosaver.ui.screen.ftp_config

import ru.sevoro.photosaver.ui.screen.Validator

/**
 * @author Sergey Vorobyev
 */
object FtpPathValidator : Validator {

    override fun isValid(text: String): Boolean {
        val trimmed = text.trim()
        return trimmed.startsWith("ftp://") || trimmed.startsWith("ftps://")
    }
}