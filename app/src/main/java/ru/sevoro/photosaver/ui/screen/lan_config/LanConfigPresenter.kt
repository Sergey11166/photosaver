package ru.sevoro.photosaver.ui.screen.lan_config

import ru.sevoro.photosaver.ui.screen.base.ConfigBasePresenter

/**
 * @author Sergey Vorobyev
 */
interface LanConfigPresenter : ConfigBasePresenter<LanConfigView> {

    fun onDomainTextChanged(text: String)
}