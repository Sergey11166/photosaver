package ru.sevoro.photosaver.ui.screen.ftp_config

import ru.sevoro.photosaver.ui.screen.base.ConfigBasePresenter

/**
 * @author Sergey Vorobyev
 */
interface FtpConfigPresenter : ConfigBasePresenter<FtpConfigView> {

    fun onUsernameTextChanged(text: String)
}