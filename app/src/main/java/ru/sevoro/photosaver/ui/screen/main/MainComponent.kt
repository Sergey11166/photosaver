package ru.sevoro.photosaver.ui.screen.main

import dagger.Component
import ru.sevoro.photosaver.AppComponent

/**
 * @author Sergey Vorobyev
 */
@MainScope
@Component(dependencies = [(AppComponent::class)], modules = [(MainModule::class)])
interface MainComponent {
    fun inject(activity: MainActivity)
}