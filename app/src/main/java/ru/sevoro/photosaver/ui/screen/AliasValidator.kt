package ru.sevoro.photosaver.ui.screen

/**
 * @author Sergey Vorobyev
 */
object AliasValidator : Validator {

    override fun isValid(text: String): Boolean {
        val trimmed = text.trim()
        return trimmed.length <= 20 && trimmed.isNotEmpty()
    }
}