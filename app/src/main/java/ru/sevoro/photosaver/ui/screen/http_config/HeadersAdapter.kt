package ru.sevoro.photosaver.ui.screen.http_config

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.ui.SimpleTextWatcher

/**
 * @author Sergey Vorobyev
 */
class HeadersAdapter(
        private val headerChangedListener: (Int, String) -> Unit,
        private val deleteClickListener: (Int) -> Unit
) : RecyclerView.Adapter<HeaderViewHolder>() {

    private lateinit var context: Context

    private val data = mutableListOf<String>()

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeaderViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_header, parent, false)
        return HeaderViewHolder(itemView, headerChangedListener, deleteClickListener, data)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: HeaderViewHolder, position: Int) {
        val header = data[position]
        holder.headerEt.setText(header)

        if (position == data.size - 1) {
            holder.deleteIv.visibility = INVISIBLE
        } else {
            holder.deleteIv.visibility = VISIBLE
            holder.headerEt.setSelection(holder.headerEt.text.toString().length)
        }
    }

    fun setData(headers: List<String>) {
        data.clear()
        headers.forEach { if (!data.contains(it)) data.add(it) }
        notifyDataSetChanged()
    }

    fun addItem(header: String) {
        if (data.contains(header)) return
        data.add(header)
        val insertedPos = data.size - 1
        notifyItemInserted(insertedPos)
        if (insertedPos >= 1) notifyItemChanged(insertedPos - 1)
    }

    fun deleteItem(pos: Int) {
        data.removeAt(pos)
        notifyItemRemoved(pos)
    }
}

class HeaderViewHolder(
        itemView: View,
        private val textChangedListener: (Int, String) -> Unit,
        private val deleteClickListener: (Int) -> Unit,
        private val data: MutableList<String>
) : RecyclerView.ViewHolder(itemView) {

    val headerEt: EditText = itemView.findViewById(R.id.et_header)
    val deleteIv: ImageView = itemView.findViewById(R.id.iv_delete_header)

    init {
        deleteIv.setOnClickListener { deleteClickListener(adapterPosition) }
        headerEt.addTextChangedListener(SimpleTextWatcher {
            data[adapterPosition] = it
            textChangedListener(adapterPosition, it)
        })
    }
}