package ru.sevoro.photosaver.ui.screen.base

import ru.sevoro.photosaver.data.config.Config

/**
 * @author Sergey Vorobyev
 */
interface ConfigModel {

    fun <T : Config> saveConfig(config: T)

    fun <T : Config> getConfig(id: String): T
}