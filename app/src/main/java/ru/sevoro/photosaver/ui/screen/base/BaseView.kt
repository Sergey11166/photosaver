package ru.sevoro.photosaver.ui.screen.base

/**
 * @author Sergey Vorobyev
 */
interface BaseView {

    fun showProgress()

    fun hideProgress()
}