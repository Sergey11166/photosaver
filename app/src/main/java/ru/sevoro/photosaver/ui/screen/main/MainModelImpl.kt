package ru.sevoro.photosaver.ui.screen.main

import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.Purchase
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.config.*
import ru.sevoro.photosaver.data.sender.SenderLazyFactory
import ru.sevoro.photosaver.helper.*

/**
 * @author Sergey Vorobyev
 */
class MainModelImpl(
        private val localStorage: LocalStorage,
        private val gdriveHelper: GdriveHelper,
        private val remoteConfigHelper: RemoteConfigHelper,
        private val billingHelper: BillingHelper,
        private val senderLazyFactory: SenderLazyFactory
) : MainModel {

    override fun getAllConfigs(): List<ConfigUIModel> {
        return localStorage.getAllConfigs()
                .sortedBy { it.created }
                .map { transformConfigToConfigUIModel(it) }
    }

    override fun getConfig(configUIModel: ConfigUIModel): Config {
        return when (configUIModel.type) {
            TYPE_FTP -> localStorage.getConfig<FtpConfig>(configUIModel.id)
            TYPE_HTTP -> localStorage.getConfig<HttpConfig>(configUIModel.id)
            TYPE_GDRIVE -> localStorage.getConfig<GdriveConfig>(configUIModel.id)
            else -> {
                localStorage.getConfig<LanConfig>(configUIModel.id)
            }
        }
    }

    override fun saveConfig(config: Config) {
        localStorage.saveConfig(config)
    }

    override fun deleteConfig(configId: String) {
        localStorage.deleteConfig(configId)
    }

    override fun saveSelectedConfigId(id: String) {
        localStorage.saveSelectedConfigId(id)
    }

    override fun getSelectedConfigId(): String {
        return localStorage.getSelectedConfigId()
    }

    override fun sendFile(files: List<String>, configId: String, subDir: String) {
        val config: Config = localStorage.getConfig(configId)
        senderLazyFactory.getSender(config.type).send(files, config, subDir)
    }

    override fun getGdriveFolderId(configId: String, subDir: String): Single<String> {
        val config = localStorage.getConfig<GdriveConfig>(configId)

        var fullPath = config.path
        if (fullPath.isNotEmpty() && fullPath.last() != '/') fullPath += "/"
        fullPath += subDir
        if (fullPath.isEmpty()) fullPath += "/"

        return gdriveHelper.getFolderIdFromPath(fullPath, config.account)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun startAdDisablingFlow(activity: Any) {
        val remoteConfig = remoteConfigHelper.getCachedConfig()
        billingHelper.launchInAppBillingFlow(activity,
                remoteConfig.getString(REMOTE_CONFIG_AD_DISABLING_PURCHASE_SKU))
    }

    override fun getAdDisablingUpdateObs(): Observable<Boolean> {
        return remoteConfigHelper.getUpdatedConfig()
                .toObservable()
                .flatMap {
                    if (it.getBoolean(REMOTE_CONFIG_AD_DISABLED)) {
                        return@flatMap Observable.just(true)
                    } else {
                        return@flatMap billingHelper.getUpdatePurchasesSubject()
                                .filter { it.responseCode == BillingClient.BillingResponse.OK }
                                .map { isAdDisablingPurchased(it.purchases) }
                    }
                }
    }

    private fun isAdDisablingPurchased(purchases: MutableList<Purchase>?): Boolean {
        val remoteConfig = remoteConfigHelper.getCachedConfig()
        purchases?.forEach {
            if (it.sku == remoteConfig.getString(REMOTE_CONFIG_AD_DISABLING_PURCHASE_SKU))
                return true
        }

        return false
    }
}