package ru.sevoro.photosaver.ui.screen.main

import ru.sevoro.photosaver.ui.screen.base.BasePresenter

/**
 * @author Sergey Vorobyev
 */
interface MainPresenter : BasePresenter<MainView> {

    fun onViewResumed()

    fun onAddConfigureButtonClick()

    fun onPrivacyPolicyButtonClick()

    fun onSettingsActionClick()

    fun onAddActionClick()

    fun onItemSelected(pos: Int)

    fun onItemEditClick(pos: Int)

    fun onItemDeleteClick(pos: Int)

    fun onSubDirTextChanged(text: String)

    fun onSubDirChecked(checked: Boolean)

    fun onGalleryButtonClick()

    fun onCameraButtonClick()

    fun onFilesSelected(pathList: List<String>)

    fun onCameraExplanationDialogYesButtonClick()

    fun onGalleryExplanationDialogYesButtonClick()

    fun onAccountsExplanationDialogForCameraYesButtonClick()

    fun onAccountsExplanationDialogForGalleryYesButtonClick()

    fun onCameraAndAccountExplanationDialogYesButtonClick()

    fun onGalleryAndAccountExplanationDialogYesButtonClick()

    fun onExplanationDialogNoButtonClick()

    fun onExplanationDialogSettingsClick()

    fun onCameraPermissionGranted()

    fun onCameraPermissionDenied()

    fun onCameraPermissionDeniedNeverAsk()

    fun onGalleryPermissionGranted()

    fun onGalleryPermissionDenied()

    fun onGalleryPermissionDeniedNeverAsk()

    fun onAccountsPermissionDeniedForCamera()

    fun onAccountsPermissionDeniedForGallery()

    fun onAccountsPermissionDeniedNeverAsk()

    fun onCameraAndAccountsPermissionsDenied()

    fun onCameraAndAccountsPermissionsDeniedNeverAsk()

    fun onGalleryAndAccountsPermissionsDenied()

    fun onGalleryAndAccountsPermissionsDeniedNeverAsk()

    fun onRecoveryGoogleAuthTokenSuccess()

    fun onCloseAdButtonClick()
}