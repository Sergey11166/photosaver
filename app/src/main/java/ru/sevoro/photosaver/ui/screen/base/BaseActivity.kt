@file:Suppress("DEPRECATION")

package ru.sevoro.photosaver.ui.screen.base

import android.app.ProgressDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.annotation.NonNull
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.util.Toaster
import javax.inject.Inject


/**
 * @author Sergey Vorobyev
 */

private const val IS_PROGRESS_SHOWING_KEY = "IS_PROGRESS_SHOWING_KEY"

abstract class BaseActivity : AppCompatActivity(), BaseView {

    @Inject
    protected lateinit var toaster: Toaster
    protected val handler = Handler(Looper.getMainLooper())
    private lateinit var progressDialog: ProgressDialog
    private var isProgressShowing = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDaggerComponent()
        initProgressDialog()

        if (savedInstanceState != null && savedInstanceState.getBoolean(IS_PROGRESS_SHOWING_KEY)) {
            showProgress()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(IS_PROGRESS_SHOWING_KEY, isProgressShowing)
        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        progressDialog.dismiss()
        super.onDestroy()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(this, R.style.CustomProgress)
        progressDialog.setCancelable(false)
        progressDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun showProgress() {
        progressDialog.show()
        progressDialog.setContentView(R.layout.progress_splash)
        isProgressShowing = true
    }

    override fun hideProgress() {
        handler.post { progressDialog.hide() }
        isProgressShowing = false
    }

    protected fun showFragmentDialog(@NonNull dialog: DialogFragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(dialog, dialog::class.java.name)
        transaction.commitAllowingStateLoss()
    }

    abstract fun initDaggerComponent()
}