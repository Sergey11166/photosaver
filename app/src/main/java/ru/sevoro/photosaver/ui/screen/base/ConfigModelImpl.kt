package ru.sevoro.photosaver.ui.screen.base

import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.config.Config

/**
 * @author Sergey Vorobyev
 */
class ConfigModelImpl(private val localStorage: LocalStorage) : ConfigModel {

    override fun <T : Config> saveConfig(config: T) {
        localStorage.saveConfig(config)
    }

    override fun <T : Config> getConfig(id: String): T {
        return localStorage.getConfig(id)
    }
}