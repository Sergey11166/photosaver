package ru.sevoro.photosaver.ui.screen.ftp_config

import ru.sevoro.photosaver.ui.screen.base.ConfigBaseView

/**
 * @author Sergey Vorobyev
 */
interface FtpConfigView : ConfigBaseView {

    fun enableUsernameInputError(enabled: Boolean)

    fun isUsernameInputHasError(): Boolean

    fun getEnteredUsername(): String

    fun setInputUsername(username: String)

    fun getEnteredPassword(): String

    fun setInputPassword(password: String)
}