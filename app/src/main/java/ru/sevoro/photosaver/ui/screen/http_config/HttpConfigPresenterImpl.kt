@file:Suppress("PackageName")

package ru.sevoro.photosaver.ui.screen.http_config

import ru.sevoro.photosaver.data.config.AUTH_BASIC
import ru.sevoro.photosaver.data.config.HttpConfig
import ru.sevoro.photosaver.ui.screen.AliasValidator
import ru.sevoro.photosaver.ui.screen.base.AbsConfigBasePresenter
import ru.sevoro.photosaver.ui.screen.base.ConfigModel

/**
 * @author Sergey Vorobyev
 */
class HttpConfigPresenterImpl(
        private val model: ConfigModel
) : AbsConfigBasePresenter<HttpConfigView>(HttpPathValidator), HttpConfigPresenter {

    private var config: HttpConfig? = null
    private var headers = mutableListOf<String>()

    override fun onViewCreated(v: HttpConfigView) {
        super.onViewCreated(v)
        v.addHeader("")
    }

    override fun onViewCreatedInEditMode(v: HttpConfigView, configId: String) {
        super.onViewCreated(v)
        config = model.getConfig(configId)
        headers = config!!.headers
        v.setInputAlias(config!!.alias)
        v.setInputPath(config!!.path)
        v.setInputFormParam(config!!.formParam)
        v.selectAuthType(config!!.authType)
        v.selectHttpMethod(config!!.httpMethod)

        when (config!!.authType) {
            AUTH_BASIC -> {
                v.setInputUsername(config!!.username)
                v.setInputPassword(config!!.password)
            }
        }

        v.showHeaders(config!!.headers)
        v.addHeader("")
    }

    override fun onFormParamTextChanged(text: String) {
        view?.enableFormParamInputError(!FormParamValidator.isValid(text))
    }

    override fun onTypeAuthSelected(authType: String) {
        setupAuthInputs(authType)
    }

    override fun onHeaderTextChanged(position: Int, text: String) {
        // update exist header
        if (position < headers.size) {
            headers[position] = text
        }

        // add new header
        if (position == headers.size && text.isNotEmpty()) {
            // add new item
            headers.add(text)
            view?.addHeader("")
        }

        // remove exist item
        if (text.isEmpty() && position < headers.size) {
            headers.removeAt(position)
            view?.deleteHeader(position)
        }
    }

    override fun onHeaderDeleteClick(position: Int) {
        view?.deleteHeader(position)
        headers.removeAt(position)
    }

    override fun onDoneActionClick() {
        view?.let {
            val alias = it.getEnteredAlias()
            val path = it.getEnteredPath()
            val formParam = it.getEnteredFormParam()

            val isAliasValid = AliasValidator.isValid(alias)
            val isPathValid = HttpPathValidator.isValid(path)
            val isFormParamValid = FormParamValidator.isValid(formParam)

            it.enableAliasInputError(!isAliasValid)
            it.enablePathInputError(!isPathValid)
            it.enableFormParamInputError(!isFormParamValid)

            if (!isAliasValid || !isPathValid || !isFormParamValid) return

            val httpMethod = it.getSelectedHttpMethod()
            val authType = it.getSelectedAuthType()
            var username = ""
            var password = ""

            when (authType) {
                AUTH_BASIC -> {
                    username = it.getEnteredUsername()
                    password = it.getEnteredPassword()
                }
            }

            val newConfig = HttpConfig(
                    alias,
                    path,
                    formParam,
                    authType,
                    httpMethod,
                    username,
                    password,
                    headers)

            newConfig.id = if (config != null) config!!.id else ""
            model.saveConfig(newConfig)
            it.navigateToMainScreen()
        }
    }

    private fun setupAuthInputs(authType: String) {
        when (authType) {
            AUTH_BASIC -> {
                view?.setUserInputVisibility(true)
                view?.setPasswordInputVisibility(true)
            }
            else -> {
                view?.setUserInputVisibility(false)
                view?.setPasswordInputVisibility(false)
            }
        }
    }
}