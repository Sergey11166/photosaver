package ru.sevoro.photosaver.ui.screen.base

/**
 * @author Sergey Vorobyev
 */
interface BasePresenter<in V> {

    fun onViewCreated(v: V)

    fun onViewDestroyed()

    fun onScopeDestroyed()

    fun onViewCreatedInEditMode(v: V, configId: String)
}