package ru.sevoro.photosaver.ui.screen.lan_config

import dagger.Component
import ru.sevoro.photosaver.AppComponent

/**
 * @author Sergey Vorobyev
 */
@LanConfigScope
@Component(dependencies = [(AppComponent::class)], modules = arrayOf(LanConfigModule::class))
interface LanConfigComponent {
    fun inject(activity: LanConfigActivity)
}