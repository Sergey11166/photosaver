package ru.sevoro.photosaver.ui.screen.base

import ru.sevoro.photosaver.ui.screen.AliasValidator
import ru.sevoro.photosaver.ui.screen.Validator

/**
 * @author Sergey Vorobyev
 */
abstract class AbsConfigBasePresenter<V : ConfigBaseView>(
        private val pathValidator: Validator
) : AbsPresenter<V>(), ConfigBasePresenter<V> {

    override fun onAliasTextChanged(text: String) {
        view?.enableAliasInputError(!AliasValidator.isValid(text))
    }

    override fun onPathTextChanged(text: String) {
        view?.enablePathInputError(!pathValidator.isValid(text))
    }
}