package ru.sevoro.photosaver.ui.screen.base

/**
 * @author Sergey Vorobyev
 */
interface ConfigBaseView : BaseView {

    fun enableAliasInputError(enabled: Boolean)

    fun enablePathInputError(enabled: Boolean)

    fun isAliasInputHasError(): Boolean

    fun isPathInputHasError(): Boolean

    fun getEnteredAlias(): String

    fun setInputAlias(alias: String)

    fun getEnteredPath(): String

    fun setInputPath(path: String)

    fun navigateToMainScreen()
}