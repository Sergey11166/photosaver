package ru.sevoro.photosaver.ui.screen.ftp_config

import javax.inject.Scope

/**
 * @author Sergey Vorobyev
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FtpConfigScope