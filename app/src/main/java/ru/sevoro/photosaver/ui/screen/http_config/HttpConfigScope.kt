package ru.sevoro.photosaver.ui.screen.http_config

import javax.inject.Scope

/**
 * @author Sergey Vorobyev
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class HttpConfigScope