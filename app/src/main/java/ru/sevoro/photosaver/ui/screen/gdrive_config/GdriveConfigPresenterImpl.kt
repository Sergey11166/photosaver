package ru.sevoro.photosaver.ui.screen.gdrive_config

import com.google.android.gms.common.ConnectionResult
import ru.sevoro.photosaver.data.config.GdriveConfig
import ru.sevoro.photosaver.ui.screen.AliasValidator
import ru.sevoro.photosaver.ui.screen.base.AbsConfigBasePresenter
import ru.sevoro.photosaver.ui.screen.base.ConfigModel

/**
 * @author Sergey Vorobyev
 */
class GdriveConfigPresenterImpl(
        private val model: ConfigModel
) : AbsConfigBasePresenter<GdriveConfigView>(CloudPathValidator), GdriveConfigPresenter {

    private var config: GdriveConfig? = null

    override fun onViewCreatedInEditMode(v: GdriveConfigView, configId: String) {
        onViewCreated(v)
        config = model.getConfig(configId)
        v.setInputAlias(config!!.alias)
        v.setInputPath(config!!.path)
        v.setSelectedAccount(config!!.account)
    }

    override fun onAccountSelected(account: String) {
        view?.setSelectedAccount(account)
    }

    override fun onEditAccountClick() {
        view?.let {
            val code = it.getPlayServicesAvailabilityStatusCode()
            if (code != ConnectionResult.SUCCESS) {
                if (it.isUserResolvablePlayServicesError(code)) {
                    it.showResolvePlayServicesErrorDialog(code)
                } else {
                    it.showPlayServicesNotAvailableToast()
                }
            } else {
                it.startChoosingAccount()
            }
        }
    }

    override fun onReturnFromPlayServices() {
        onEditAccountClick()
    }

    override fun onDoneActionClick() {
        view?.let {
            val alias = it.getEnteredAlias()
            val path = it.getEnteredPath()
            val account = it.getSelectedAccount()

            val isAliasValid = AliasValidator.isValid(alias)
            val isPathValid = CloudPathValidator.isValid(path)
            val isAccountValid = account.isNotEmpty()

            it.enableAliasInputError(!isAliasValid)
            it.enablePathInputError(!isPathValid)

            if (!isAliasValid || !isPathValid) return

            if (!isAccountValid) {
                it.showSelectAccountToast()
                return
            }

            val newConfig = GdriveConfig(
                    it.getEnteredAlias(),
                    it.getEnteredPath(),
                    if (config != null) config!!.subDirChecked else false,
                    if (config != null) config!!.subDirText else "",
                    it.getSelectedAccount())

            newConfig.id = if (config != null) config!!.id else ""
            model.saveConfig(newConfig)
            it.navigateToMainScreen()
        }
    }
}