package ru.sevoro.photosaver.ui.screen.main

import io.reactivex.Observable
import io.reactivex.Single
import ru.sevoro.photosaver.data.config.Config

/**
 * @author Sergey Vorobyev
 */
interface MainModel {

    fun getAllConfigs(): List<ConfigUIModel>

    fun getConfig(configUIModel: ConfigUIModel): Config

    fun saveConfig(config: Config)

    fun deleteConfig(configId: String)

    fun saveSelectedConfigId(id: String)

    fun getSelectedConfigId(): String

    fun sendFile(files: List<String>, configId: String, subDir: String)

    fun getGdriveFolderId(configId: String, subDir: String): Single<String>

    fun startAdDisablingFlow(activity: Any)

    fun getAdDisablingUpdateObs(): Observable<Boolean>
}