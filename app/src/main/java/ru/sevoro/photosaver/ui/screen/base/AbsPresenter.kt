package ru.sevoro.photosaver.ui.screen.base

/**
 * @author Sergey Vorobyev
 */
abstract class AbsPresenter<V> : BasePresenter<V> {

    protected var view: V? = null

    override fun onViewCreated(v: V) {
        this.view = v
    }

    override fun onViewDestroyed() {
        this.view = null
    }

    override fun onScopeDestroyed() {
    }

    override fun onViewCreatedInEditMode(v: V, configId: String) {
    }
}