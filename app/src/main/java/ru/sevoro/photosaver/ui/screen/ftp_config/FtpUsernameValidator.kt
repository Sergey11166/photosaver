package ru.sevoro.photosaver.ui.screen.ftp_config

/**
 * @author Sergey Vorobyev
 */
object FtpUsernameValidator {

    fun isValid(text: String): Boolean {
        return text.trim().isNotEmpty()
    }
}