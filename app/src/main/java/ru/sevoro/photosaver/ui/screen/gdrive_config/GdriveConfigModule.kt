package ru.sevoro.photosaver.ui.screen.gdrive_config

import dagger.Module
import dagger.Provides
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.ui.screen.base.ConfigModel
import ru.sevoro.photosaver.ui.screen.base.ConfigModelImpl

/**
 * @author Sergey Vorobyev
 */
@Module
class GdriveConfigModule {

    @Provides
    @GdriveConfigScope
    fun providePresenter(model: ConfigModel): GdriveConfigPresenter {
        return GdriveConfigPresenterImpl(model)
    }

    @Provides
    @GdriveConfigScope
    fun provideModel(localStorage: LocalStorage): ConfigModel {
        return ConfigModelImpl(localStorage)
    }
}