package ru.sevoro.photosaver.ui

import android.text.Editable
import android.text.TextWatcher

/**
 * @author Sergey Vorobyev
 */
class SimpleTextWatcher(private val afterTextChangeListener: (String) -> Unit) : TextWatcher {

    override fun afterTextChanged(text: Editable) {
        afterTextChangeListener(text.toString())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
}