package ru.sevoro.photosaver.ui.screen.settings

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.preference.PreferenceFragmentCompat
import android.view.MenuItem
import ru.sevoro.photosaver.R

/**
 * @author Sergey Vorobyev
 */

const val KEY_PREF_DELETE_SUCCESS_UPLOADED = "KEY_PREF_DELETE_SUCCESS_UPLOADED"
const val KEY_PREF_KEEP_SCREEN_ON = "KEY_PREF_KEEP_SCREEN_ON"
const val KEY_PREF_SUMMARY_NOTIFICATIONS = "KEY_PREF_SUMMARY_NOTIFICATIONS"

class SettingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportFragmentManager.beginTransaction().replace(android.R.id.content, SettingsFragment()).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}

class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }
}
