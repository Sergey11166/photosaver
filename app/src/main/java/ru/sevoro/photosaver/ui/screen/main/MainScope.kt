package ru.sevoro.photosaver.ui.screen.main

import javax.inject.Scope

/**
 * @author Sergey Vorobyev
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MainScope