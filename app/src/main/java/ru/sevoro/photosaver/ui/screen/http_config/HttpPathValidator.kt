package ru.sevoro.photosaver.ui.screen.http_config

import ru.sevoro.photosaver.ui.screen.Validator

/**
 * @author Sergey Vorobyev
 */
object HttpPathValidator : Validator {

    override fun isValid(text: String): Boolean {
        val trimmed = text.trim()
        return trimmed.startsWith("http://") || trimmed.startsWith("https://")
    }
}