package ru.sevoro.photosaver.ui.screen.http_config

/**
 * @author Sergey Vorobyev
 */
object FormParamValidator {

    fun isValid(text: String): Boolean {
        return text.trim().isNotEmpty()
    }
}