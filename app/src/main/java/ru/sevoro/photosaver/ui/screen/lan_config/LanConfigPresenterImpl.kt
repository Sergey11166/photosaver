package ru.sevoro.photosaver.ui.screen.lan_config

import ru.sevoro.photosaver.data.config.LanConfig
import ru.sevoro.photosaver.ui.screen.AliasValidator
import ru.sevoro.photosaver.ui.screen.base.AbsConfigBasePresenter
import ru.sevoro.photosaver.ui.screen.base.ConfigModel

/**
 * @author Sergey Vorobyev
 */
class LanConfigPresenterImpl(
        private val model: ConfigModel
) : AbsConfigBasePresenter<LanConfigView>(LanPathValidator), LanConfigPresenter {

    private var config: LanConfig? = null

    override fun onViewCreatedInEditMode(v: LanConfigView, configId: String) {
        onViewCreated(v)
        config = model.getConfig(configId)
        v.setInputAlias(config!!.alias)
        v.setInputPath(config!!.path)
        v.setInputDomain(config!!.domain)
        v.setInputUsername(config!!.username)
        v.setInputPassword(config!!.password)
    }

    override fun onDomainTextChanged(text: String) {
        view?.enableDomainInputError(!DomainValidator.isValid(text))
    }

    override fun onDoneActionClick() {
        view?.let {
            val alias = it.getEnteredAlias()
            val path = it.getEnteredPath()
            val domain = it.getEnteredDomain()

            val isAliasValid = AliasValidator.isValid(alias)
            val isPathValid = LanPathValidator.isValid(path)
            val isDomainValid = DomainValidator.isValid(domain)

            it.enableAliasInputError(!isAliasValid)
            it.enablePathInputError(!isPathValid)
            it.enableDomainInputError(!isDomainValid)

            if (!isAliasValid || !isPathValid || !isDomainValid) return

            val newConfig = LanConfig(
                    it.getEnteredAlias(),
                    it.getEnteredPath(),
                    if (config != null) config!!.subDirChecked else false,
                    if (config != null) config!!.subDirText else "",
                    it.getEnteredDomain(),
                    it.getEnteredUsername(),
                    it.getEnteredPassword())

            newConfig.id = if (config != null) config!!.id else ""
            model.saveConfig(newConfig)
            it.navigateToMainScreen()
        }
    }
}