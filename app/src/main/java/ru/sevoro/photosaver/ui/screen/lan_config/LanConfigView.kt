package ru.sevoro.photosaver.ui.screen.lan_config

import ru.sevoro.photosaver.ui.screen.base.ConfigBaseView

/**
 * @author Sergey Vorobyev
 */
interface LanConfigView : ConfigBaseView {

    fun enableDomainInputError(enabled: Boolean)

    fun isDomainInputHasError(): Boolean

    fun getEnteredDomain(): String

    fun setInputDomain(domain: String)

    fun getEnteredUsername(): String

    fun setInputUsername(username: String)

    fun getEnteredPassword(): String

    fun setInputPassword(password: String)
}