package ru.sevoro.photosaver.ui.screen.main

import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import ru.sevoro.photosaver.ui.screen.base.BaseView

/**
 * @author Sergey Vorobyev
 */
interface MainView : BaseView {

    fun showNoConfigLayout(animate: Boolean)

    fun showMainLayout(animate: Boolean)

    fun showPrivacyPolicy()

    fun navigateToSettingsScreen()

    fun navigateToTypeSelectionScreen()

    fun navigateToEditConfigScreen(configId: String, configType: String)

    fun startGalleryApp()

    fun startCameraApp()

    fun startSettingsApp()

    fun checkCameraPermission(): Boolean

    fun checkGalleryPermission(): Boolean

    fun checkAccountsPermission(): Boolean

    fun requestCameraPermission()

    fun requestCameraAndAccountsPermissions()

    fun requestGalleryPermission()

    fun requestGalleryAndAccountsPermissions()

    fun requestAccountsPermissionForCamera()

    fun requestAccountsPermissionForGallery()

    fun showConfigs(configs: List<ConfigUIModel>)

    fun selectItem(pos: Int)

    fun deleteItem(pos: Int)

    fun getEnteredSubDir(): String

    fun isSubDirChecked(): Boolean

    fun isSubDirInputHasError(): Boolean

    fun enableGalleryAndCameraButtons(enabled: Boolean)

    fun setSubDirText(text: String)

    fun setSubDirChecked(checked: Boolean)

    fun setEnabledSubDirCheckbox(enabled: Boolean)

    fun showExplanationDialogForCamera()

    fun showExplanationDialogForCameraNeverAsk()

    fun showExplanationDialogForGallery()

    fun showExplanationDialogForGalleryNeverAsk()

    fun showExplanationDialogAccountsForCamera()

    fun showExplanationDialogAccountsForGallery()

    fun showExplanationDialogForAccountsNeverAsk()

    fun showExplanationDialogForCameraAndAccounts()

    fun showExplanationDialogForCameraAndAccountsNeverAsk()

    fun showExplanationDialogForGalleryAndAccounts()

    fun showExplanationDialogForGalleryAndAccountsNeverAsk()

    fun hideExplanationDialog()

    fun showErrorPrepareGdriveToast()

    fun recoverGoogleAuthToken(exception: UserRecoverableAuthIOException)

    fun showAd()

    fun hideAd()
}