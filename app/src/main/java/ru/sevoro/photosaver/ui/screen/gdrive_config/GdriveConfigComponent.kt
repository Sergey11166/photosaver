package ru.sevoro.photosaver.ui.screen.gdrive_config

import dagger.Component
import ru.sevoro.photosaver.AppComponent

/**
 * @author Sergey Vorobyev
 */
@GdriveConfigScope
@Component(dependencies = [(AppComponent::class)], modules = [(GdriveConfigModule::class)])
interface GdriveConfigComponent {
    fun inject(activity: GdriveConfigActivity)
}