package ru.sevoro.photosaver.ui.screen.gdrive_config

import ru.sevoro.photosaver.ui.screen.base.ConfigBaseView

/**
 * @author Sergey Vorobyev
 */
interface GdriveConfigView : ConfigBaseView {

    fun setSelectedAccount(account: String)

    fun getSelectedAccount(): String

    fun startChoosingAccount()

    fun getPlayServicesAvailabilityStatusCode(): Int

    fun isUserResolvablePlayServicesError(statusCode: Int): Boolean

    fun showResolvePlayServicesErrorDialog(statusCode: Int)

    fun showPlayServicesNotAvailableToast()

    fun showSelectAccountToast()
}