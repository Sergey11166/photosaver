package ru.sevoro.photosaver.ui.screen.gdrive_config

import javax.inject.Scope

/**
 * @author Sergey Vorobyev
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class GdriveConfigScope