package ru.sevoro.photosaver.ui.screen.http_config

import dagger.Component
import ru.sevoro.photosaver.AppComponent

/**
 * @author Sergey Vorobyev
 */
@HttpConfigScope
@Component(dependencies = [(AppComponent::class)], modules = [(HttpConfigModule::class)])
interface HttpConfigComponent {
    fun inject(activity: HttpConfigActivity)
}