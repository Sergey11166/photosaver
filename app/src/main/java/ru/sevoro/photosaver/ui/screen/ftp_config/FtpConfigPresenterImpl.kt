package ru.sevoro.photosaver.ui.screen.ftp_config

import ru.sevoro.photosaver.data.config.FtpConfig
import ru.sevoro.photosaver.ui.screen.AliasValidator
import ru.sevoro.photosaver.ui.screen.base.AbsConfigBasePresenter
import ru.sevoro.photosaver.ui.screen.base.ConfigModel

/**
 * @author Sergey Vorobyev
 */
class FtpConfigPresenterImpl(
        private val model: ConfigModel
) : AbsConfigBasePresenter<FtpConfigView>(FtpPathValidator), FtpConfigPresenter {

    private var config: FtpConfig? = null

    override fun onViewCreatedInEditMode(v: FtpConfigView, configId: String) {
        onViewCreated(v)
        config = model.getConfig(configId)
        v.setInputAlias(config!!.alias)
        v.setInputPath(config!!.path)
        v.setInputUsername(config!!.username)
        v.setInputPassword(config!!.password)
    }

    override fun onUsernameTextChanged(text: String) {
        view?.enableUsernameInputError(!FtpUsernameValidator.isValid(text))
    }

    override fun onDoneActionClick() {
        view?.let {
            val alias = it.getEnteredAlias()
            val path = it.getEnteredPath()
            val username = it.getEnteredUsername()

            val isAliasValid = AliasValidator.isValid(alias)
            val isPathValid = FtpPathValidator.isValid(path)
            val isUsernameValid = FtpUsernameValidator.isValid(username)

            it.enableAliasInputError(!isAliasValid)
            it.enablePathInputError(!isPathValid)
            it.enableUsernameInputError(!isUsernameValid)

            if (!isAliasValid || !isPathValid || !isUsernameValid) return

            val newConfig = FtpConfig(
                    it.getEnteredAlias(),
                    it.getEnteredPath(),
                    if (config != null) config!!.subDirChecked else false,
                    if (config != null) config!!.subDirText else "",
                    it.getEnteredUsername(),
                    it.getEnteredPassword())

            newConfig.id = if (config != null) config!!.id else ""
            model.saveConfig(newConfig)
            it.navigateToMainScreen()
        }
    }
}