package ru.sevoro.photosaver.ui.screen.lan_config

import ru.sevoro.photosaver.ui.screen.Validator

/**
 * @author Sergey Vorobyev
 */
object LanPathValidator : Validator {

    override fun isValid(text: String): Boolean {
        val trimmed = text.trim()
        return trimmed.length >= 3 && trimmed.contains("/")
    }
}