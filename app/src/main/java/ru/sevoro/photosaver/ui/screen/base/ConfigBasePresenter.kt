package ru.sevoro.photosaver.ui.screen.base

/**
 * @author Sergey Vorobyev
 */
interface ConfigBasePresenter<in V : ConfigBaseView> : BasePresenter<V> {

    fun onAliasTextChanged(text: String)

    fun onPathTextChanged(text: String)

    fun onDoneActionClick()
}