package ru.sevoro.photosaver.ui.screen.lan_config

/**
 * @author Sergey Vorobyev
 */
object DomainValidator {

    fun isValid(text: String): Boolean {
        val trimmed = text.trim()
        return trimmed.length < 50
    }
}