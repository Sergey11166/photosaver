package ru.sevoro.photosaver.ui.screen

/**
 * @author Sergey Vorobyev
 */
interface Validator {
    fun isValid(text: String): Boolean
}