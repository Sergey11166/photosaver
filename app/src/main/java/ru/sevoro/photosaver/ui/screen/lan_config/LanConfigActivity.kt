package ru.sevoro.photosaver.ui.screen.lan_config

import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import ru.sevoro.photosaver.App
import ru.sevoro.photosaver.DaggerComponentManager
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.ui.SimpleTextWatcher
import ru.sevoro.photosaver.ui.screen.base.ConfigBaseActivity
import ru.sevoro.photosaver.ui.screen.base.KEY_CONFIG_ID
import javax.inject.Inject

/**
 * @author Sergey Vorobyev
 */

class LanConfigActivity : ConfigBaseActivity(), LanConfigView {

    @Inject
    lateinit var presenter: LanConfigPresenter

    private lateinit var aliasInput: TextInputLayout
    private lateinit var pathInput: TextInputLayout
    private lateinit var domainInput: TextInputLayout
    private lateinit var usernameInput: TextInputLayout
    private lateinit var passwordInput: TextInputLayout
    private lateinit var aliasEt: EditText
    private lateinit var pathEt: EditText
    private lateinit var domainEt: EditText
    private lateinit var usernameEt: EditText
    private lateinit var passwordEt: EditText

    override fun isAliasInputHasError() = aliasInput.isErrorEnabled

    override fun isPathInputHasError() = pathInput.isErrorEnabled

    override fun isDomainInputHasError() = domainInput.isErrorEnabled

    override fun getEnteredAlias() = aliasEt.text.toString()

    override fun getEnteredPath() = pathEt.text.toString()

    override fun getEnteredDomain() = domainEt.text.toString()

    override fun getEnteredUsername() = usernameEt.text.toString()

    override fun getEnteredPassword() = passwordEt.text.toString()

    override fun setInputUsername(username: String) { usernameEt.setText(username) }

    override fun setInputPassword(password: String) { passwordEt.setText(password) }

    override fun enableAliasInputError(enabled: Boolean) {
        aliasInput.isErrorEnabled = enabled
        aliasInput.error = if (enabled) getString(R.string.common_config_wrong_alias) else null
    }

    override fun enablePathInputError(enabled: Boolean) {
        pathInput.isErrorEnabled = enabled
        pathInput.error = if (enabled) getString(R.string.common_config_wrong_path) else null
    }

    override fun enableDomainInputError(enabled: Boolean) {
        domainInput.isErrorEnabled = enabled
        domainInput.error = if (enabled) getString(R.string.lan_config_wrong_domen) else null
    }

    override fun setInputAlias(alias: String) {
        aliasEt.setText(alias)
        presenter.onAliasTextChanged(alias)
    }

    override fun setInputPath(path: String) {
        pathEt.setText(path)
        presenter.onPathTextChanged(path)
    }

    override fun setInputDomain(domain: String) {
        domainEt.setText(domain)
        presenter.onDomainTextChanged(domain)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lan_config)
        initWidgets()

        val configId = intent.extras?.get(KEY_CONFIG_ID)
        if (configId == null) presenter.onViewCreated(this)
        else presenter.onViewCreatedInEditMode(this, configId as String)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_item_done) presenter.onDoneActionClick()
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        presenter.onViewDestroyed()
        if (isFinishing) DaggerComponentManager.unregisterScope(LanConfigScope::class.java)
        super.onDestroy()
    }

    private fun initWidgets() {
        aliasInput = findViewById(R.id.input_alias)
        pathInput = findViewById(R.id.input_path)
        domainInput = findViewById(R.id.input_domain)
        usernameInput = findViewById(R.id.input_username)
        passwordInput = findViewById(R.id.input_password)

        aliasEt = findViewById(R.id.et_alias)
        pathEt = findViewById(R.id.et_path)
        domainEt = findViewById(R.id.et_domain)
        usernameEt = findViewById(R.id.et_username)
        passwordEt = findViewById(R.id.et_password)

        aliasEt.addTextChangedListener(SimpleTextWatcher { presenter.onAliasTextChanged(it) })
        aliasEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) aliasEt.setSelection(aliasEt.text.length)
        }

        pathEt.addTextChangedListener(SimpleTextWatcher { presenter.onPathTextChanged(it) })
        pathEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) pathEt.setSelection(pathEt.text.length)
        }

        domainEt.addTextChangedListener(SimpleTextWatcher { presenter.onDomainTextChanged(it) })
        domainEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) domainEt.setSelection(domainEt.text.length)
        }
    }

    override fun initDaggerComponent() {
        val componentKey = LanConfigComponent::class.java.name
        var component = DaggerComponentManager.getComponent<LanConfigComponent>(componentKey)
        if (component == null) {
            component = DaggerLanConfigComponent.builder()
                    .appComponent(App.appComponent)
                    .lanConfigModule(LanConfigModule())
                    .build()
            DaggerComponentManager.registerComponent(componentKey, component)
        }
        component?.inject(this)
    }
}