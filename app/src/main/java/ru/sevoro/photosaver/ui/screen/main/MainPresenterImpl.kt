package ru.sevoro.photosaver.ui.screen.main

import android.util.Log
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import io.reactivex.disposables.CompositeDisposable
import ru.sevoro.photosaver.data.config.GdriveConfig
import ru.sevoro.photosaver.data.config.TYPE_GDRIVE
import ru.sevoro.photosaver.data.config.TYPE_HTTP
import ru.sevoro.photosaver.ui.screen.base.AbsPresenter

/**
 * @author Sergey Vorobyev
 */
private const val TAG = "MainPresenterImpl"

class MainPresenterImpl(
        private val model: MainModel
) : AbsPresenter<MainView>(), MainPresenter {

    private var configs = mutableListOf<ConfigUIModel>()
    private var selectedItemPos = 0
    private val disposables = CompositeDisposable()
    private var lastSelectedFiles: List<String>? = null

    override fun onViewCreated(v: MainView) {
        super.onViewCreated(v)
        disposables.add(model.getAdDisablingUpdateObs().subscribe(
                { isAdDisabled ->
                    view?.let { if (isAdDisabled) it.hideAd() else it.showAd() }
                },
                {
                    Log.e(TAG, "getUpdatePurchasesSubject error", it)
                }
        ))
    }

    override fun onViewResumed() {
        configs.clear()
        configs.addAll(model.getAllConfigs())
        view?.let {
            if (configs.isEmpty()) {
                it.showNoConfigLayout(false)
            } else {
                it.showMainLayout(animate = false)
                it.showConfigs(configs)
                selectedItemPos = getSavedItemPos()
                it.selectItem(selectedItemPos)
                it.setSubDirChecked(configs[selectedItemPos].subDirChecked)
                it.setSubDirText(configs[selectedItemPos].subDirText)
                it.setEnabledSubDirCheckbox(configs[selectedItemPos].type != TYPE_HTTP)
            }
        }
    }

    override fun onScopeDestroyed() {
        disposables.dispose()
    }

    override fun onAddConfigureButtonClick() {
        view?.navigateToTypeSelectionScreen()
    }

    override fun onPrivacyPolicyButtonClick() {
        view?.showPrivacyPolicy()
    }

    override fun onSettingsActionClick() {
        view?.navigateToSettingsScreen()
    }

    override fun onAddActionClick() {
        view?.navigateToTypeSelectionScreen()
    }

    override fun onItemSelected(pos: Int) {
        selectedItemPos = pos
        model.saveSelectedConfigId(configs[selectedItemPos].id)
        view?.let {
            it.selectItem(selectedItemPos)
            it.setSubDirChecked(configs[selectedItemPos].subDirChecked)
            it.setSubDirText(configs[selectedItemPos].subDirText)
            it.setEnabledSubDirCheckbox(configs[selectedItemPos].type != TYPE_HTTP)
        }
    }

    override fun onItemEditClick(pos: Int) {
        view?.navigateToEditConfigScreen(configs[pos].id, configs[pos].type)
    }

    override fun onItemDeleteClick(pos: Int) {
        model.deleteConfig(configs[pos].id)
        configs.removeAt(pos)

        if (configs.isEmpty()) {
            view?.showNoConfigLayout(false)
            return
        }

        if (selectedItemPos == pos) {
            if (pos == 0) {
                onItemSelected(0)
            } else {
                onItemSelected(pos - 1)
            }
        } else if (pos < selectedItemPos) {
            selectedItemPos--
        }

        view?.deleteItem(pos)
    }

    override fun onSubDirTextChanged(text: String) {
        saveSubDirText(text)
        view?.let { it.enableGalleryAndCameraButtons(validateSubDir(text) || !it.isSubDirChecked()) }
    }

    override fun onSubDirChecked(checked: Boolean) {
        view?.let {
            if (!checked) {
                it.enableGalleryAndCameraButtons(true)
            } else {
                it.enableGalleryAndCameraButtons(validateSubDir(it.getEnteredSubDir()))
            }
        }

        configs[selectedItemPos].subDirChecked = checked
        val config = model.getConfig(configs[selectedItemPos])
        config.subDirChecked = checked
        model.saveConfig(config)
    }

    override fun onCameraButtonClick() {
        view?.let {
            val typeConfig = configs[selectedItemPos].type
            if (it.checkCameraPermission()) {
                if (typeConfig == TYPE_GDRIVE) {
                    if (it.checkAccountsPermission()) {
                        it.startCameraApp()
                    } else {
                        it.requestAccountsPermissionForCamera()
                    }
                } else {
                    it.startCameraApp()
                }
            } else {
                if (typeConfig == TYPE_GDRIVE) {
                    if (it.checkAccountsPermission()) {
                        it.requestCameraPermission()
                    } else {
                        it.requestCameraAndAccountsPermissions()
                    }
                } else {
                    it.requestCameraPermission()
                }
            }
        }
    }

    override fun onGalleryButtonClick() {
        view?.let {
            val typeConfig = configs[selectedItemPos].type
            if (it.checkGalleryPermission()) {
                if (typeConfig == TYPE_GDRIVE) {
                    if (it.checkAccountsPermission()) {
                        it.startGalleryApp()
                    } else {
                        it.requestAccountsPermissionForGallery()
                    }
                } else {
                    it.startGalleryApp()
                }
            } else {
                if (typeConfig == TYPE_GDRIVE) {
                    if (it.checkAccountsPermission()) {
                        it.requestGalleryPermission()
                    } else {
                        it.requestGalleryAndAccountsPermissions()
                    }
                } else {
                    it.requestGalleryPermission()
                }
            }
        }
    }

    override fun onFilesSelected(pathList: List<String>) {
        lastSelectedFiles = pathList
        val selectedConfig = configs[selectedItemPos]
        when (selectedConfig.type) {
            TYPE_GDRIVE -> sendToGdrive(selectedConfig)
            else -> {
                sendSelectedFiles(pathList)
            }
        }
    }

    override fun onCameraExplanationDialogYesButtonClick() {
        view?.hideExplanationDialog()
        view?.requestCameraPermission()
    }

    override fun onGalleryExplanationDialogYesButtonClick() {
        view?.hideExplanationDialog()
        view?.requestGalleryPermission()
    }

    override fun onAccountsExplanationDialogForCameraYesButtonClick() {
        view?.hideExplanationDialog()
        view?.requestAccountsPermissionForCamera()
    }

    override fun onAccountsExplanationDialogForGalleryYesButtonClick() {
        view?.hideExplanationDialog()
        view?.requestAccountsPermissionForGallery()
    }

    override fun onCameraAndAccountExplanationDialogYesButtonClick() {
        view?.hideExplanationDialog()
        view?.requestCameraAndAccountsPermissions()
    }

    override fun onGalleryAndAccountExplanationDialogYesButtonClick() {
        view?.hideExplanationDialog()
        view?.requestGalleryAndAccountsPermissions()
    }

    override fun onExplanationDialogNoButtonClick() {
        view?.hideExplanationDialog()
    }

    override fun onExplanationDialogSettingsClick() {
        view?.hideExplanationDialog()
        view?.startSettingsApp()
    }

    override fun onCameraPermissionGranted() {
        view?.startCameraApp()
    }

    override fun onCameraPermissionDenied() {
        view?.showExplanationDialogForCamera()
    }

    override fun onCameraPermissionDeniedNeverAsk() {
        view?.showExplanationDialogForCameraNeverAsk()
    }

    override fun onGalleryPermissionGranted() {
        view?.startGalleryApp()
    }

    override fun onGalleryPermissionDenied() {
        view?.showExplanationDialogForGallery()
    }

    override fun onGalleryPermissionDeniedNeverAsk() {
        view?.showExplanationDialogForGalleryNeverAsk()
    }

    override fun onAccountsPermissionDeniedForCamera() {
        view?.showExplanationDialogAccountsForCamera()
    }

    override fun onAccountsPermissionDeniedForGallery() {
        view?.showExplanationDialogAccountsForGallery()
    }

    override fun onAccountsPermissionDeniedNeverAsk() {
        view?.showExplanationDialogForAccountsNeverAsk()
    }

    override fun onCameraAndAccountsPermissionsDenied() {
        view?.showExplanationDialogForCameraAndAccounts()
    }

    override fun onCameraAndAccountsPermissionsDeniedNeverAsk() {
        view?.showExplanationDialogForCameraAndAccountsNeverAsk()
    }

    override fun onGalleryAndAccountsPermissionsDenied() {
        view?.showExplanationDialogForGalleryAndAccounts()
    }

    override fun onGalleryAndAccountsPermissionsDeniedNeverAsk() {
        view?.showExplanationDialogForGalleryAndAccountsNeverAsk()
    }

    override fun onRecoveryGoogleAuthTokenSuccess() {
        sendToGdrive(configs[selectedItemPos])
    }

    override fun onCloseAdButtonClick() {
        view?.let { model.startAdDisablingFlow(view!!) }
    }

    private fun validateSubDir(text: String): Boolean = text.isNotEmpty()

    private fun getSavedItemPos(): Int {
        val savedItemId = model.getSelectedConfigId()
        val savedList = configs.filter { it.id == savedItemId }
        if (savedList.isNotEmpty()) {
            return configs.indexOf(savedList[0])
        }
        return 0
    }

    private fun sendSelectedFiles(pathList: List<String>) {
        view?.let {
            model.sendFile(pathList, configs[selectedItemPos].id,
                    if (it.isSubDirChecked()) it.getEnteredSubDir() else "")
        }
    }

    private fun updateGdriveConfig(gdriveFolderId: String) {
        val config = model.getConfig(configs[selectedItemPos]) as GdriveConfig
        config.folderId = gdriveFolderId
        model.saveConfig(config)
    }

    private fun saveSubDirText(subDirText: String) {
        view?.let {
            if (configs.isEmpty()) return
            configs[selectedItemPos].subDirText = subDirText
            val config = model.getConfig(configs[selectedItemPos])
            config.subDirText = subDirText
            model.saveConfig(config)
        }
    }

    private fun sendToGdrive(selectedConfig: ConfigUIModel) {
        view?.let { v ->
            disposables.add(model.getGdriveFolderId(selectedConfig.id, v.getEnteredSubDir())
                    .doOnSubscribe { v.showProgress() }
                    .doOnEvent { _, _ -> v.hideProgress() }
                    .subscribe(
                            { folderId ->
                                if (lastSelectedFiles != null) {
                                    updateGdriveConfig(folderId)
                                    sendSelectedFiles(lastSelectedFiles!!)
                                }
                            },
                            { error ->
                                if (error is UserRecoverableAuthIOException) {
                                    v.recoverGoogleAuthToken(error)
                                } else {
                                    v.showErrorPrepareGdriveToast()
                                    Log.e(TAG, "getGdriveFolderId id error", error)
                                    error.printStackTrace()
                                }
                            }
                    ))
        }
    }
}