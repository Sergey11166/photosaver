package ru.sevoro.photosaver.ui.screen.http_config

import ru.sevoro.photosaver.ui.screen.base.ConfigBaseView

/**
 * @author Sergey Vorobyev
 */
interface HttpConfigView : ConfigBaseView {

    fun showHeaders(headers: List<String>)

    fun addHeader(header: String)

    fun deleteHeader(position: Int)

    fun enableFormParamInputError(enabled: Boolean)

    fun isFormParamHasError(): Boolean

    fun getEnteredFormParam(): String

    fun setInputFormParam(param: String)

    fun getSelectedHttpMethod(): String

    fun selectHttpMethod(method: String)

    fun getSelectedAuthType(): String

    fun selectAuthType(authType: String)

    fun getEnteredUsername(): String

    fun setInputUsername(username: String)

    fun getEnteredPassword(): String

    fun setInputPassword(password: String)

    fun setUserInputVisibility(visible: Boolean)

    fun setPasswordInputVisibility(visible: Boolean)
}