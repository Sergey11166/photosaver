package ru.sevoro.photosaver.ui.screen.http_config

import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import ru.sevoro.photosaver.App
import ru.sevoro.photosaver.DaggerComponentManager
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.data.config.*
import ru.sevoro.photosaver.ui.SimpleTextWatcher
import ru.sevoro.photosaver.ui.screen.base.ConfigBaseActivity
import ru.sevoro.photosaver.ui.screen.base.KEY_CONFIG_ID
import javax.inject.Inject

/**
 * @author Sergey Vorobyev
 */
class HttpConfigActivity : ConfigBaseActivity(), HttpConfigView {

    @Inject
    lateinit var presenter: HttpConfigPresenter

    private lateinit var aliasInput: TextInputLayout
    private lateinit var pathInput: TextInputLayout
    private lateinit var formParamInput: TextInputLayout
    private lateinit var usernameInput: TextInputLayout
    private lateinit var passwordInput: TextInputLayout
    private lateinit var aliasEt: EditText
    private lateinit var pathEt: EditText
    private lateinit var formParamEt: EditText
    private lateinit var usernameEt: EditText
    private lateinit var passwordEt: EditText
    private lateinit var methodSpinner: Spinner
    private lateinit var authSpinner: Spinner
    private lateinit var headersRv: RecyclerView

    private val adapter = HeadersAdapter(
            headerChangedListener = { pos: Int, text: String -> presenter.onHeaderTextChanged(pos, text) },
            deleteClickListener = { pos: Int -> presenter.onHeaderDeleteClick(pos) })

    override fun showHeaders(headers: List<String>) {
        adapter.setData(headers)
    }

    override fun addHeader(header: String) {
        adapter.addItem(header)
    }

    override fun deleteHeader(position: Int) {
        handler.postDelayed({ adapter.deleteItem(position) }, 300)
    }

    override fun enableAliasInputError(enabled: Boolean) {
        aliasInput.isErrorEnabled = enabled
        aliasInput.error = if (enabled) getString(R.string.common_config_wrong_alias) else null
    }

    override fun enablePathInputError(enabled: Boolean) {
        pathInput.isErrorEnabled = enabled
        pathInput.error = if (enabled) getString(R.string.common_config_wrong_path) else null
    }

    override fun enableFormParamInputError(enabled: Boolean) {
        formParamInput.isErrorEnabled = enabled
        formParamInput.error = if (enabled) getString(R.string.http_config_wrong_param) else null
    }

    override fun isAliasInputHasError() = aliasInput.isErrorEnabled

    override fun isPathInputHasError() = pathInput.isErrorEnabled

    override fun isFormParamHasError() = formParamInput.isErrorEnabled

    override fun getEnteredAlias() = aliasEt.text.toString()

    override fun getEnteredPath() = pathEt.text.toString()

    override fun getEnteredFormParam() = formParamEt.text.toString()

    override fun getEnteredUsername() = usernameEt.text.toString()

    override fun getEnteredPassword() = passwordEt.text.toString()

    override fun setInputUsername(username: String) { usernameEt.setText(username) }

    override fun setInputPassword(password: String) { passwordEt.setText(password) }

    override fun setInputAlias(alias: String) {
        aliasEt.setText(alias)
        if (alias.isNotEmpty()) presenter.onAliasTextChanged(alias)
    }

    override fun setInputPath(path: String) {
        pathEt.setText(path)
        if (path.isNotEmpty()) presenter.onPathTextChanged(path)
    }

    override fun setInputFormParam(param: String) {
        formParamEt.setText(param)
        if (param.isNotEmpty()) presenter.onFormParamTextChanged(param)
    }

    override fun getSelectedHttpMethod() = methodSpinner.selectedItem as String

    override fun selectHttpMethod(method: String) {
        when (method) {
            HTTP_METHOD_POST -> methodSpinner.setSelection(0)
            HTTP_METHOD_PUT -> methodSpinner.setSelection(1)
            HTTP_METHOD_PATCH -> methodSpinner.setSelection(2)
            HTTP_METHOD_OPTIONS -> methodSpinner.setSelection(3)
        }
    }

    override fun getSelectedAuthType() = authSpinner.selectedItem as String

    override fun selectAuthType(authType: String) {
        when (authType) {
            AUTH_NONE -> authSpinner.setSelection(0)
            AUTH_BASIC -> authSpinner.setSelection(1)
        }
    }

    override fun setUserInputVisibility(visible: Boolean) {
        usernameInput.visibility = if (visible) VISIBLE else GONE
    }

    override fun setPasswordInputVisibility(visible: Boolean) {
        passwordInput.visibility = if (visible) VISIBLE else GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_http_config)
        initWidgets()

        val configId = intent.extras?.get(KEY_CONFIG_ID)
        if (configId == null) presenter.onViewCreated(this)
        else presenter.onViewCreatedInEditMode(this, configId as String)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_item_done) presenter.onDoneActionClick()
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        presenter.onViewDestroyed()
        if (isFinishing) {
            presenter.onScopeDestroyed()
            DaggerComponentManager.unregisterScope(HttpConfigScope::class.java)
        }
        super.onDestroy()
    }

    private fun initWidgets() {
        aliasInput = findViewById(R.id.input_alias)
        pathInput = findViewById(R.id.input_path)
        formParamInput = findViewById(R.id.input_form_param)
        usernameInput = findViewById(R.id.input_username)
        passwordInput = findViewById(R.id.input_password)

        aliasEt = findViewById(R.id.et_alias)
        pathEt = findViewById(R.id.et_path)
        formParamEt = findViewById(R.id.et_form_param)
        usernameEt = findViewById(R.id.et_username)
        passwordEt = findViewById(R.id.et_password)

        aliasEt.addTextChangedListener(SimpleTextWatcher { presenter.onAliasTextChanged(it) })
        aliasEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) aliasEt.setSelection(aliasEt.text.length)
        }

        pathEt.addTextChangedListener(SimpleTextWatcher { presenter.onPathTextChanged(it) })
        pathEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) pathEt.setSelection(pathEt.text.length)
        }

        formParamEt.addTextChangedListener(SimpleTextWatcher { presenter.onFormParamTextChanged(it) })
        formParamEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) formParamEt.setSelection(formParamEt.text.length)
        }

        headersRv = findViewById(R.id.rv_headers)
        headersRv.adapter = adapter

        methodSpinner = findViewById(R.id.spinner_method)
        methodSpinner.adapter = ArrayAdapter.createFromResource(
                this, R.array.http_config_http_methods_array, android.R.layout.simple_spinner_dropdown_item)

        authSpinner = findViewById(R.id.spinner_auth)
        authSpinner.adapter = ArrayAdapter.createFromResource(
                this, R.array.http_config_auth_types_array, android.R.layout.simple_spinner_dropdown_item)
        authSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                presenter.onTypeAuthSelected(authSpinner.selectedItem as String)
            }
        }
    }

    override fun initDaggerComponent() {
        val componentKey = HttpConfigComponent::class.java.name
        var component = DaggerComponentManager.getComponent<HttpConfigComponent>(componentKey)
        if (component == null) {
            component = DaggerHttpConfigComponent.builder()
                    .appComponent(App.appComponent)
                    .httpConfigModule(HttpConfigModule())
                    .build()
            DaggerComponentManager.registerComponent(componentKey, component)
        }
        component?.inject(this)
    }
}