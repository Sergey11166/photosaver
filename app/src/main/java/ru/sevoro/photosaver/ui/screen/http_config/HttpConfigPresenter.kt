package ru.sevoro.photosaver.ui.screen.http_config

import ru.sevoro.photosaver.ui.screen.base.ConfigBasePresenter

/**
 * @author Sergey Vorobyev
 */
interface HttpConfigPresenter : ConfigBasePresenter<HttpConfigView> {

    fun onFormParamTextChanged(text: String)

    fun onTypeAuthSelected(authType: String)

    fun onHeaderTextChanged(position: Int, text: String)

    fun onHeaderDeleteClick(position: Int)
}