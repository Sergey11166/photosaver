package ru.sevoro.photosaver.ui.screen.ftp_config

import dagger.Module
import dagger.Provides
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.ui.screen.base.ConfigModel
import ru.sevoro.photosaver.ui.screen.base.ConfigModelImpl

/**
 * @author Sergey Vorobyev
 */
@Module
class FtpConfigModule {

    @Provides
    @FtpConfigScope
    fun providePresenter(model: ConfigModel): FtpConfigPresenter {
        return FtpConfigPresenterImpl(model)
    }

    @Provides
    @FtpConfigScope
    fun provideModel(localStorage: LocalStorage): ConfigModel {
        return ConfigModelImpl(localStorage)
    }
}