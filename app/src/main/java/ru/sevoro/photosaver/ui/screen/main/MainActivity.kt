package ru.sevoro.photosaver.ui.screen.main

import android.Manifest.permission.GET_ACCOUNTS
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.DialogInterface.BUTTON_NEGATIVE
import android.content.DialogInterface.BUTTON_POSITIVE
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityCompat.requestPermissions
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat.checkSelfPermission
import android.support.v4.content.ContextCompat.getColor
import android.support.v4.content.PermissionChecker.PERMISSION_GRANTED
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.WindowManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import com.sangcomz.fishbun.FishBun
import com.sangcomz.fishbun.define.Define.ALBUM_REQUEST_CODE
import com.sangcomz.fishbun.define.Define.INTENT_PATH
import ru.sevoro.photosaver.App
import ru.sevoro.photosaver.DaggerComponentManager
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.data.config.TYPE_FTP
import ru.sevoro.photosaver.data.config.TYPE_GDRIVE
import ru.sevoro.photosaver.data.config.TYPE_HTTP
import ru.sevoro.photosaver.data.config.TYPE_LAN
import ru.sevoro.photosaver.helper.REMOTE_CONFIG_PRIVACY_POLICY_URL
import ru.sevoro.photosaver.helper.RemoteConfigHelper
import ru.sevoro.photosaver.ui.SimpleTextWatcher
import ru.sevoro.photosaver.ui.screen.*
import ru.sevoro.photosaver.ui.screen.base.BaseActivity
import ru.sevoro.photosaver.ui.screen.base.KEY_CONFIG_ID
import ru.sevoro.photosaver.ui.screen.ftp_config.FtpConfigActivity
import ru.sevoro.photosaver.ui.screen.gdrive_config.GdriveConfigActivity
import ru.sevoro.photosaver.ui.screen.http_config.HttpConfigActivity
import ru.sevoro.photosaver.ui.screen.lan_config.LanConfigActivity
import ru.sevoro.photosaver.ui.screen.settings.KEY_PREF_KEEP_SCREEN_ON
import ru.sevoro.photosaver.ui.screen.settings.SettingActivity
import ru.sevoro.photosaver.ui.screen.type_choosing.TypeChoosingActivity
import ru.sevoro.photosaver.util.*
import ru.sevoro.photosaver.util.FileProviderUtils.getPath
import java.io.File
import javax.inject.Inject

private const val REQUEST_CODE_CAMERA = 0
private const val PERMISSION_REQUEST_CAMERA = 10
private const val PERMISSION_REQUEST_GALLERY = 20
private const val PERMISSION_REQUEST_ACCOUNTS_FOR_CAMERA = 30
private const val PERMISSION_REQUEST_ACCOUNTS_FOR_GALLERY = 40
private const val PERMISSION_REQUEST_CAMERA_AND_ACCOUNTS = 50
private const val PERMISSION_REQUEST_GALLERY_AND_ACCOUNTS = 60
private const val REQUEST_CODE_RECOVER_GOOGLE_AUTH_TOKEN = 70

class MainActivity : MainView, BaseActivity() {

    @Inject
    lateinit var presenter: MainPresenter

    @Inject
    lateinit var prefs: SharedPreferences

    @Inject
    lateinit var remoteConfigHelper: RemoteConfigHelper

    private var photoFile: File? = null

    private lateinit var noConfigLayout: View
    private lateinit var mainLayout: View

    private lateinit var subDirCb: CheckBox
    private lateinit var subDirInput: TextInputLayout
    private lateinit var subDirEt: EditText

    private lateinit var galleryBtn: Button
    private lateinit var cameraBtn: Button
    private lateinit var closeAdBtn: ImageView
    private lateinit var adView: AdView

    private var showedDialog: DialogFragment? = null

    private val adapter = ConfigsAdapter(
            itemClickListener = { pos: Int -> presenter.onItemSelected(pos) },
            deleteClickListener = { pos: Int -> presenter.onItemDeleteClick(pos) },
            editClickListener = { pos: Int -> presenter.onItemEditClick(pos) })

    override fun showNoConfigLayout(animate: Boolean) {
        if (animate) {

        } else {
            mainLayout.visibility = GONE
            noConfigLayout.visibility = VISIBLE
        }
    }

    override fun showMainLayout(animate: Boolean) {
        if (animate) {

        } else {
            mainLayout.visibility = VISIBLE
            noConfigLayout.visibility = GONE
        }
    }

    override fun showPrivacyPolicy() {
        openUrlInBrowser(remoteConfigHelper
                .getCachedConfig().getString(REMOTE_CONFIG_PRIVACY_POLICY_URL))
    }

    override fun navigateToSettingsScreen() =
            startActivity(Intent(this, SettingActivity::class.java))

    override fun navigateToTypeSelectionScreen() =
            startActivity(Intent(this, TypeChoosingActivity::class.java))

    override fun navigateToEditConfigScreen(configId: String, configType: String) {
        var activityClass: Class<*>? = null

        when (configType) {
            TYPE_LAN -> activityClass = LanConfigActivity::class.java
            TYPE_FTP -> activityClass = FtpConfigActivity::class.java
            TYPE_HTTP -> activityClass = HttpConfigActivity::class.java
            TYPE_GDRIVE -> activityClass = GdriveConfigActivity::class.java
        }

        val intent = Intent(this, activityClass)
        intent.putExtra(KEY_CONFIG_ID, configId)
        startActivity(intent)
    }

    override fun startGalleryApp() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"

        FishBun.with(this)
                .MultiPageMode()
                .setIsUseDetailView(true)
                .setMaxCount(100)
                .setActionBarColor(getColor(this, R.color.color_primary), getColor(this, R.color.color_primary_dark), false)
                .textOnImagesSelectionLimitReached(getString(R.string.custom_gallery_message_limit_reached))
                .textOnNothingSelected(getString(R.string.custom_gallery_message_nothing_selected))
                .setAllViewTitle(getString(R.string.custom_gallery_all_items_title))
                .setActionBarTitle(getString(R.string.custom_gallery_action_bar_title))
                .startAlbum()
    }

    override fun startCameraApp() {
        photoFile = createImageFile("IMG")
        goToCameraApp(photoFile!!, REQUEST_CODE_CAMERA)
    }

    override fun startSettingsApp() = goToSettingsApp()

    override fun checkCameraPermission() = checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) == 0

    override fun checkGalleryPermission() = checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) == 0

    override fun checkAccountsPermission() = checkSelfPermission(this, GET_ACCOUNTS) == 0

    override fun requestCameraPermission() =
            requestPermissions(this, arrayOf(WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST_CAMERA)

    override fun requestGalleryPermission() =
            requestPermissions(this, arrayOf(WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST_GALLERY)

    override fun requestAccountsPermissionForCamera() =
            requestPermissions(this, arrayOf(GET_ACCOUNTS), PERMISSION_REQUEST_ACCOUNTS_FOR_CAMERA)

    override fun requestAccountsPermissionForGallery() =
            requestPermissions(this, arrayOf(GET_ACCOUNTS), PERMISSION_REQUEST_ACCOUNTS_FOR_GALLERY)

    override fun requestCameraAndAccountsPermissions() = requestPermissions(this,
            arrayOf(WRITE_EXTERNAL_STORAGE, GET_ACCOUNTS), PERMISSION_REQUEST_CAMERA_AND_ACCOUNTS)

    override fun requestGalleryAndAccountsPermissions() = requestPermissions(this,
            arrayOf(WRITE_EXTERNAL_STORAGE, GET_ACCOUNTS), PERMISSION_REQUEST_GALLERY_AND_ACCOUNTS)

    override fun showConfigs(configs: List<ConfigUIModel>) = adapter.setData(configs)

    override fun selectItem(pos: Int) {
        handler.postDelayed({ adapter.selectItem(pos) }, 300)
    }

    override fun deleteItem(pos: Int) = adapter.deleteItem(pos)

    override fun getEnteredSubDir() = subDirEt.text.toString()

    override fun isSubDirChecked() = subDirCb.isChecked

    override fun isSubDirInputHasError() = subDirInput.isErrorEnabled

    override fun enableGalleryAndCameraButtons(enabled: Boolean) {
        galleryBtn.isEnabled = enabled
        cameraBtn.isEnabled = enabled
    }

    override fun setSubDirText(text: String) {
        subDirEt.setText(text)
        subDirEt.setSelection(subDirEt.text.length)
        presenter.onSubDirTextChanged(text)
    }

    override fun setSubDirChecked(checked: Boolean) {
        subDirCb.isChecked = checked
        subDirInput.isEnabled = checked
        subDirEt.requestFocus()
        presenter.onSubDirChecked(checked)
    }

    override fun setEnabledSubDirCheckbox(enabled: Boolean) {
        subDirCb.isEnabled = enabled
    }

    override fun showExplanationDialogForCamera() {
        showedDialog = createCameraExplanationDialog { _, which ->
            when (which) {
                BUTTON_POSITIVE -> presenter.onCameraExplanationDialogYesButtonClick()
                BUTTON_NEGATIVE -> presenter.onExplanationDialogNoButtonClick()
            }
        }
        showFragmentDialog(showedDialog!!)
    }

    override fun showExplanationDialogForCameraNeverAsk() {
        showedDialog = createCameraExplanationDialog { _, which ->
            when (which) {
                BUTTON_POSITIVE -> presenter.onExplanationDialogSettingsClick()
                BUTTON_NEGATIVE -> presenter.onExplanationDialogNoButtonClick()
            }
        }
        showFragmentDialog(showedDialog!!)
    }

    override fun showExplanationDialogForGallery() {
        showedDialog = createGalleryExplanationDialog { _, which ->
            when (which) {
                BUTTON_POSITIVE -> presenter.onGalleryExplanationDialogYesButtonClick()
                BUTTON_NEGATIVE -> presenter.onExplanationDialogNoButtonClick()
            }
        }
        showFragmentDialog(showedDialog!!)
    }

    override fun showExplanationDialogForGalleryNeverAsk() {
        showedDialog = createGalleryExplanationDialog { _, which ->
            when (which) {
                BUTTON_POSITIVE -> presenter.onExplanationDialogSettingsClick()
                BUTTON_NEGATIVE -> presenter.onExplanationDialogNoButtonClick()
            }
        }
        showFragmentDialog(showedDialog!!)
    }

    override fun showExplanationDialogAccountsForCamera() {
        showedDialog = createAccountsExplanationDialog { _, which ->
            when (which) {
                BUTTON_POSITIVE -> presenter.onAccountsExplanationDialogForCameraYesButtonClick()
                BUTTON_NEGATIVE -> presenter.onExplanationDialogNoButtonClick()
            }
        }
        showFragmentDialog(showedDialog!!)
    }

    override fun showExplanationDialogAccountsForGallery() {
        showedDialog = createAccountsExplanationDialog { _, which ->
            when (which) {
                BUTTON_POSITIVE -> presenter.onAccountsExplanationDialogForGalleryYesButtonClick()
                BUTTON_NEGATIVE -> presenter.onExplanationDialogNoButtonClick()
            }
        }
        showFragmentDialog(showedDialog!!)
    }

    override fun showExplanationDialogForAccountsNeverAsk() {
        showedDialog = createAccountsExplanationDialog { _, which ->
            when (which) {
                BUTTON_POSITIVE -> presenter.onExplanationDialogSettingsClick()
                BUTTON_NEGATIVE -> presenter.onExplanationDialogNoButtonClick()
            }
        }
        showFragmentDialog(showedDialog!!)
    }

    override fun showExplanationDialogForCameraAndAccounts() {
        showedDialog = createCameraAndAccountsExplanationDialog { _, which ->
            when (which) {
                BUTTON_POSITIVE -> presenter.onCameraAndAccountExplanationDialogYesButtonClick()
                BUTTON_NEGATIVE -> presenter.onExplanationDialogNoButtonClick()
            }
        }
        showFragmentDialog(showedDialog!!)
    }

    override fun showExplanationDialogForCameraAndAccountsNeverAsk() {
        showedDialog = createCameraAndAccountsExplanationDialog { _, which ->
            when (which) {
                BUTTON_POSITIVE -> presenter.onExplanationDialogSettingsClick()
                BUTTON_NEGATIVE -> presenter.onExplanationDialogNoButtonClick()
            }
        }
        showFragmentDialog(showedDialog!!)
    }

    override fun showExplanationDialogForGalleryAndAccounts() {
        showedDialog = createGalleryAndAccountsExplanationDialog { _, which ->
            when (which) {
                BUTTON_POSITIVE -> presenter.onGalleryAndAccountExplanationDialogYesButtonClick()
                BUTTON_NEGATIVE -> presenter.onExplanationDialogNoButtonClick()
            }
        }
        showFragmentDialog(showedDialog!!)
    }

    override fun showExplanationDialogForGalleryAndAccountsNeverAsk() {
        showedDialog = createGalleryAndAccountsExplanationDialog { _, which ->
            when (which) {
                BUTTON_POSITIVE -> presenter.onExplanationDialogSettingsClick()
                BUTTON_NEGATIVE -> presenter.onExplanationDialogNoButtonClick()
            }
        }
        showFragmentDialog(showedDialog!!)
    }

    override fun hideExplanationDialog() {
        showedDialog?.dismiss()
    }

    override fun showErrorPrepareGdriveToast() {
        toaster.showToast(R.string.error_message_prepare_gdrive_error)
    }

    override fun recoverGoogleAuthToken(exception: UserRecoverableAuthIOException) {
        startActivityForResult(exception.intent, REQUEST_CODE_RECOVER_GOOGLE_AUTH_TOKEN)
    }

    override fun showAd() {
        adView.visibility = VISIBLE
        adView.loadAd(AdRequest.Builder().build())
        adView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                closeAdBtn.visibility = VISIBLE
            }
        }
    }

    override fun hideAd() {
        adView.visibility = GONE
        closeAdBtn.visibility = GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        setContentView(R.layout.activity_main)
        initWidgets()
        presenter.onViewCreated(this)
    }

    override fun onResume() {
        super.onResume()
        enableKeepingMainScreenOn(prefs.getBoolean(KEY_PREF_KEEP_SCREEN_ON, false))
        presenter.onViewResumed()
    }

    override fun onDestroy() {
        presenter.onViewDestroyed()
        if (isFinishing) DaggerComponentManager.unregisterScope(MainScope::class.java)
        super.onDestroy()
    }

    private fun enableKeepingMainScreenOn(enabled: Boolean) {
        if (enabled) {
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
    }

    private fun initWidgets() {
        findViewById<View>(R.id.btn_add_config)
                .setOnClickListener { presenter.onAddConfigureButtonClick() }

        findViewById<View>(R.id.btn_privacy_policy)
                .setOnClickListener { presenter.onPrivacyPolicyButtonClick() }

        findViewById<RecyclerView>(R.id.rv_configs).adapter = adapter
        noConfigLayout = findViewById(R.id.layout_no_config)
        mainLayout = findViewById(R.id.layout_main)
        subDirCb = findViewById(R.id.cb_sub_dir)
        subDirInput = findViewById(R.id.input_sub_dir)
        subDirEt = findViewById(R.id.et_sub_dir)
        galleryBtn = findViewById(R.id.btn_gallery)
        cameraBtn = findViewById(R.id.btn_camera)

        galleryBtn.setOnClickListener { presenter.onGalleryButtonClick() }
        cameraBtn.setOnClickListener { presenter.onCameraButtonClick() }
        subDirEt.addTextChangedListener(SimpleTextWatcher { presenter.onSubDirTextChanged(it) })
        subDirCb.setOnCheckedChangeListener { _, isChecked ->
            subDirInput.isEnabled = isChecked
            presenter.onSubDirChecked(isChecked)
        }
        closeAdBtn = findViewById(R.id.btn_close_ad)
        closeAdBtn.setOnClickListener { presenter.onCloseAdButtonClick() }
        adView = findViewById(R.id.adView)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_action_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_item_settings -> presenter.onSettingsActionClick()
            R.id.menu_item_add -> presenter.onAddActionClick()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_REQUEST_CAMERA -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PERMISSION_GRANTED) {
                    presenter.onCameraPermissionGranted()
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)) {
                        presenter.onCameraPermissionDenied()
                    } else {
                        presenter.onCameraPermissionDeniedNeverAsk()
                    }
                }
            }

            PERMISSION_REQUEST_GALLERY -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PERMISSION_GRANTED) {
                    presenter.onGalleryPermissionGranted()
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)) {
                        presenter.onGalleryPermissionDenied()
                    } else {
                        presenter.onGalleryPermissionDeniedNeverAsk()
                    }
                }
            }

            PERMISSION_REQUEST_ACCOUNTS_FOR_CAMERA -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PERMISSION_GRANTED) {
                    presenter.onCameraPermissionGranted()
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, GET_ACCOUNTS)) {
                        presenter.onAccountsPermissionDeniedForCamera()
                    } else {
                        presenter.onAccountsPermissionDeniedNeverAsk()
                    }
                }
            }

            PERMISSION_REQUEST_ACCOUNTS_FOR_GALLERY -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PERMISSION_GRANTED) {
                    presenter.onGalleryPermissionGranted()
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, GET_ACCOUNTS)) {
                        presenter.onAccountsPermissionDeniedForGallery()
                    } else {
                        presenter.onAccountsPermissionDeniedNeverAsk()
                    }
                }
            }

            PERMISSION_REQUEST_CAMERA_AND_ACCOUNTS -> {
                if (grantResults.size == 2) {
                    if (grantResults[0] == PERMISSION_GRANTED && grantResults[1] == PERMISSION_GRANTED) {
                        presenter.onCameraPermissionGranted()
                    } else if (grantResults[0] != PERMISSION_GRANTED && grantResults[1] != PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)
                                && ActivityCompat.shouldShowRequestPermissionRationale(this, GET_ACCOUNTS)) {
                            presenter.onCameraAndAccountsPermissionsDenied()
                        } else {
                            presenter.onCameraAndAccountsPermissionsDeniedNeverAsk()
                        }
                    } else if (grantResults[0] != PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)) {
                            presenter.onCameraPermissionDenied()
                        } else {
                            presenter.onCameraPermissionDeniedNeverAsk()
                        }
                    } else if (grantResults[1] != PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, GET_ACCOUNTS)) {
                            presenter.onAccountsPermissionDeniedForCamera()
                        } else {
                            presenter.onAccountsPermissionDeniedNeverAsk()
                        }
                    }
                }
            }

            PERMISSION_REQUEST_GALLERY_AND_ACCOUNTS -> {
                if (grantResults.size == 2) {
                    if (grantResults[0] == PERMISSION_GRANTED && grantResults[1] == PERMISSION_GRANTED) {
                        presenter.onGalleryPermissionGranted()
                    } else if (grantResults[0] != PERMISSION_GRANTED && grantResults[1] != PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)
                                && ActivityCompat.shouldShowRequestPermissionRationale(this, GET_ACCOUNTS)) {
                            presenter.onGalleryAndAccountsPermissionsDenied()
                        } else {
                            presenter.onGalleryAndAccountsPermissionsDeniedNeverAsk()
                        }
                    } else if (grantResults[0] != PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)) {
                            presenter.onGalleryPermissionDenied()
                        } else {
                            presenter.onGalleryPermissionDeniedNeverAsk()
                        }
                    } else if (grantResults[1] != PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, GET_ACCOUNTS)) {
                            presenter.onAccountsPermissionDeniedForGallery()
                        } else {
                            presenter.onAccountsPermissionDeniedNeverAsk()
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        when (requestCode) {
            REQUEST_CODE_CAMERA -> {
                if (resultCode == RESULT_OK && photoFile != null) {
                    presenter.onFilesSelected(listOf(photoFile.toString()))
                    createMediaEntryInContentProvider(photoFile!!.absolutePath)
                }
            }

            ALBUM_REQUEST_CODE -> {
                if (resultCode == RESULT_OK && intent != null) {
                    val selectedImages: ArrayList<Uri> = intent.getParcelableArrayListExtra(INTENT_PATH)
                    presenter.onFilesSelected(selectedImages.map { getPath(this, it) })
                }
            }

            REQUEST_CODE_RECOVER_GOOGLE_AUTH_TOKEN -> {
                if (resultCode == RESULT_OK) {
                    presenter.onRecoveryGoogleAuthTokenSuccess()
                }
            }
        }
    }

    override fun initDaggerComponent() {
        val componentKey = MainComponent::class.java.name
        var component = DaggerComponentManager.getComponent<MainComponent>(componentKey)
        if (component == null) {
            component = DaggerMainComponent.builder()
                    .appComponent(App.appComponent)
                    .mainModule(MainModule())
                    .build()
            DaggerComponentManager.registerComponent(componentKey, component)
        }
        component?.inject(this)
    }
}
