package ru.sevoro.photosaver.ui.screen.main

import dagger.Module
import dagger.Provides
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.sender.SenderLazyFactory
import ru.sevoro.photosaver.helper.BillingHelper
import ru.sevoro.photosaver.helper.GdriveHelper
import ru.sevoro.photosaver.helper.RemoteConfigHelper

/**
 * @author Sergey Vorobyev
 */
@Module
class MainModule {

    @Provides
    @MainScope
    fun providePresenter(model: MainModel): MainPresenter {
        return MainPresenterImpl(model)
    }

    @Provides
    @MainScope
    fun provideModel(
            localStorage: LocalStorage,
            gdriveHelper: GdriveHelper,
            remoteConfigHelper: RemoteConfigHelper,
            billingHelper: BillingHelper,
            senderLazyFactory: SenderLazyFactory): MainModel {

        return MainModelImpl(localStorage, gdriveHelper, remoteConfigHelper,
                billingHelper, senderLazyFactory)
    }
}