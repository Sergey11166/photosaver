package ru.sevoro.photosaver.ui.screen.http_config

import dagger.Module
import dagger.Provides
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.ui.screen.base.ConfigModel
import ru.sevoro.photosaver.ui.screen.base.ConfigModelImpl

/**
 * @author Sergey Vorobyev
 */
@Module
class HttpConfigModule {

    @Provides
    @HttpConfigScope
    fun providePresenter(model: ConfigModel): HttpConfigPresenter {
        return HttpConfigPresenterImpl(model)
    }

    @Provides
    @HttpConfigScope
    fun provideModel(localStorage: LocalStorage): ConfigModel {
        return ConfigModelImpl(localStorage)
    }
}