package ru.sevoro.photosaver.ui.screen.base

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.view.Menu
import android.view.MenuItem
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.ui.screen.main.MainActivity

/**
 * @author Sergey Vorobyev
 */
const val KEY_CONFIG_ID = "KEY_CONFIG_ID"

abstract class ConfigBaseActivity : BaseActivity(), ConfigBaseView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) NavUtils.navigateUpFromSameTask(this)
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.config_action_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun navigateToMainScreen() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}