package ru.sevoro.photosaver.ui.screen.type_choosing

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.ui.screen.ftp_config.FtpConfigActivity
import ru.sevoro.photosaver.ui.screen.gdrive_config.GdriveConfigActivity
import ru.sevoro.photosaver.ui.screen.http_config.HttpConfigActivity
import ru.sevoro.photosaver.ui.screen.lan_config.LanConfigActivity

/**
 * @author Sergey Vorobyev
 */
class TypeChoosingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_type_choosing)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initWidgets()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun initWidgets() {
        findViewById<View>(R.id.item_lan).setOnClickListener { navigateToActivity(LanConfigActivity::class.java) }
        findViewById<View>(R.id.item_ftp).setOnClickListener { navigateToActivity(FtpConfigActivity::class.java) }
        findViewById<View>(R.id.item_web).setOnClickListener { navigateToActivity(HttpConfigActivity::class.java) }
        findViewById<View>(R.id.item_gdrive).setOnClickListener { navigateToActivity(GdriveConfigActivity::class.java) }
    }

    private fun navigateToActivity(activityClass: Class<out Activity>) {
        startActivity(Intent(this, activityClass))
    }
}