package ru.sevoro.photosaver.ui.screen.ftp_config

import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import ru.sevoro.photosaver.App
import ru.sevoro.photosaver.DaggerComponentManager
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.ui.SimpleTextWatcher
import ru.sevoro.photosaver.ui.screen.base.ConfigBaseActivity
import ru.sevoro.photosaver.ui.screen.base.KEY_CONFIG_ID
import javax.inject.Inject

/**
 * @author Sergey Vorobyev
 */

class FtpConfigActivity : ConfigBaseActivity(), FtpConfigView {

    @Inject
    lateinit var presenter: FtpConfigPresenter

    private lateinit var aliasInput: TextInputLayout
    private lateinit var pathInput: TextInputLayout
    private lateinit var usernameInput: TextInputLayout
    private lateinit var passwordInput: TextInputLayout
    private lateinit var aliasEt: EditText
    private lateinit var pathEt: EditText
    private lateinit var usernameEt: EditText
    private lateinit var passwordEt: EditText

    override fun enableAliasInputError(enabled: Boolean) {
        aliasInput.isErrorEnabled = enabled
        aliasInput.error = if (enabled) getString(R.string.common_config_wrong_alias) else null
    }

    override fun enablePathInputError(enabled: Boolean) {
        pathInput.isErrorEnabled = enabled
        pathInput.error = if (enabled) getString(R.string.common_config_wrong_path) else null
    }

    override fun enableUsernameInputError(enabled: Boolean) {
        usernameInput.isErrorEnabled = enabled
        usernameInput.error = if (enabled) getString(R.string.ftp_config_wrong_username) else null
    }

    override fun isAliasInputHasError() = aliasInput.isErrorEnabled

    override fun isPathInputHasError() = pathInput.isErrorEnabled

    override fun isUsernameInputHasError() = usernameInput.isErrorEnabled

    override fun getEnteredAlias() = aliasEt.text.toString()

    override fun getEnteredPath() = pathEt.text.toString()

    override fun getEnteredUsername() = usernameEt.text.toString()

    override fun getEnteredPassword() = passwordEt.text.toString()

    override fun setInputUsername(username: String) { usernameEt.setText(username) }

    override fun setInputPassword(password: String) { passwordEt.setText(password) }

    override fun setInputAlias(alias: String) {
        aliasEt.setText(alias)
        presenter.onAliasTextChanged(alias)
    }

    override fun setInputPath(path: String) {
        pathEt.setText(path)
        presenter.onPathTextChanged(path)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ftp_config)
        initWidgets()

        val configId = intent.extras?.get(KEY_CONFIG_ID)
        if (configId == null) presenter.onViewCreated(this)
        else presenter.onViewCreatedInEditMode(this, configId as String)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_item_done) presenter.onDoneActionClick()
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        presenter.onViewDestroyed()
        if (isFinishing) DaggerComponentManager.unregisterScope(FtpConfigScope::class.java)
        super.onDestroy()
    }

    private fun initWidgets() {
        aliasInput = findViewById(R.id.input_alias)
        pathInput = findViewById(R.id.input_path)
        usernameInput = findViewById(R.id.input_username)
        passwordInput = findViewById(R.id.input_password)

        aliasEt = findViewById(R.id.et_alias)
        pathEt = findViewById(R.id.et_path)
        usernameEt = findViewById(R.id.et_username)
        passwordEt = findViewById(R.id.et_password)

        aliasEt.addTextChangedListener(SimpleTextWatcher { presenter.onAliasTextChanged(it) })
        aliasEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) aliasEt.setSelection(aliasEt.text.length)
        }

        pathEt.addTextChangedListener(SimpleTextWatcher { presenter.onPathTextChanged(it) })
        pathEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) pathEt.setSelection(pathEt.text.length)
        }

        usernameEt.addTextChangedListener(SimpleTextWatcher { presenter.onUsernameTextChanged(it) })
        usernameEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) usernameEt.setSelection(usernameEt.text.length)
        }

        passwordEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) passwordEt.setSelection(passwordEt.text.length)
        }
    }

    override fun initDaggerComponent() {
        val componentKey = FtpConfigComponent::class.java.name
        var component = DaggerComponentManager.getComponent<FtpConfigComponent>(componentKey)
        if (component == null) {
            component = DaggerFtpConfigComponent.builder()
                    .appComponent(App.appComponent)
                    .ftpConfigModule(FtpConfigModule())
                    .build()
            DaggerComponentManager.registerComponent(componentKey, component)
        }
        component?.inject(this)
    }
}