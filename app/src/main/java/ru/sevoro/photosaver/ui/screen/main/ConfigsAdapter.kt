package ru.sevoro.photosaver.ui.screen.main

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.data.config.*

/**
 * @author Sergey Vorobyev
 */
class ConfigsAdapter(
        private val itemClickListener: (Int) -> Unit,
        private val deleteClickListener: (Int) -> Unit,
        private val editClickListener: (Int) -> Unit
) : RecyclerView.Adapter<ConfigViewHolder>() {

    private lateinit var context: Context

    private val data = mutableListOf<ConfigUIModel>()

    private var lanDrawable: Drawable? = null
    private var httpDrawable: Drawable? = null
    private var ftpDrawable: Drawable? = null
    private var gdriveDrawable: Drawable? = null
    private var defaultDrawable: Drawable? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConfigViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_config, parent, false)
        return ConfigViewHolder(itemView, itemClickListener, deleteClickListener, editClickListener)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ConfigViewHolder, position: Int) {
        val config = data[position]
        holder.iconIv.setImageDrawable(getIconDrawable(config.type))
        holder.aliasTv.text = config.alias
        holder.descTv.text = config.desc
        holder.selectRBtn.isChecked = config.checked
    }

    fun setData(data: List<ConfigUIModel>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun deleteItem(pos: Int) {
        data.removeAt(pos)
        notifyItemRemoved(pos)
    }

    fun selectItem(pos: Int) {
        if (pos < data.size && pos >= 0) {
            val selectedPosition = getSelectedPosition()
            if (selectedPosition >= 0 && selectedPosition < data.size) {
                data[selectedPosition].checked = false
                notifyItemChanged(selectedPosition)
            }
            data[pos].checked = true
            notifyItemChanged(pos)
        }
    }

    private fun getSelectedPosition(): Int {
        val selectedItems = data.filter { it.checked }
        return if (selectedItems.isNotEmpty()) data.indexOf(selectedItems[0]) else -1
    }

    private fun getIconDrawable(configType: String): Drawable {
        when (configType) {
            TYPE_LAN -> {
                if (lanDrawable == null) {
                    lanDrawable = ContextCompat.getDrawable(context, R.drawable.ic_folder_shared_accent)
                }
                return lanDrawable!!
            }
            TYPE_HTTP -> {
                if (httpDrawable == null) {
                    httpDrawable = ContextCompat.getDrawable(context, R.drawable.ic_folder_http_accent)
                }
                return httpDrawable!!
            }

            TYPE_FTP -> {
                if (ftpDrawable == null) {
                    ftpDrawable = ContextCompat.getDrawable(context, R.drawable.ic_folder_ftp_accent)
                }
                return ftpDrawable!!
            }

            TYPE_GDRIVE -> {
                if (gdriveDrawable == null) {
                    gdriveDrawable = ContextCompat.getDrawable(context, R.drawable.ic_folder_gdrive_accent)
                }
                return gdriveDrawable!!
            }

            else -> {
                if (defaultDrawable == null) {
                    defaultDrawable = ContextCompat.getDrawable(context, R.drawable.ic_folder_shared_accent)
                }
                return defaultDrawable!!
            }
        }
    }
}

class ConfigViewHolder(
        itemView: View,
        private val itemClickListener: (Int) -> Unit,
        private val deleteClickListener: (Int) -> Unit,
        private val editClickListener: (Int) -> Unit
) : RecyclerView.ViewHolder(itemView) {

    val iconIv: ImageView = itemView.findViewById(R.id.iv_icon)
    val aliasTv: TextView = itemView.findViewById(R.id.tv_alias)
    val descTv: TextView = itemView.findViewById(R.id.tv_desc)
    val selectRBtn: RadioButton = itemView.findViewById(R.id.rb_select)
    private val menuBtn: ImageView = itemView.findViewById(R.id.iv_menu)

    init {
        itemView.setOnClickListener { itemClickListener(adapterPosition) }
        menuBtn.setOnClickListener {
            val popupMenu = PopupMenu(itemView.context, menuBtn)
            popupMenu.inflate(R.menu.menu_config_item)
            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.menu_item_edit -> {
                        editClickListener(adapterPosition); true
                    }
                    R.id.menu_item_delete -> {
                        deleteClickListener(adapterPosition); true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }
}

data class ConfigUIModel(
        val id: String = "",
        val type: String = TYPE_LAN,
        val alias: String = "",
        val desc: String = "",
        var checked: Boolean = false,
        var subDirChecked: Boolean = false,
        var subDirText: String = "")

fun transformConfigToConfigUIModel(source: Config): ConfigUIModel {
    var desc = ""

    when (source.type) {
        TYPE_LAN, TYPE_FTP, TYPE_HTTP -> {
            desc = source.path
        }
        TYPE_GDRIVE -> {
            desc = "${(source as GdriveConfig).account}: ${source.path}"
        }
    }

    return ConfigUIModel(source.id, source.type, source.alias, desc, false,
            source.subDirChecked, source.subDirText)
}