package ru.sevoro.photosaver.ui.screen.lan_config

import dagger.Module
import dagger.Provides
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.ui.screen.base.ConfigModel
import ru.sevoro.photosaver.ui.screen.base.ConfigModelImpl

/**
 * @author Sergey Vorobyev
 */
@Module
class LanConfigModule {

    @Provides
    @LanConfigScope
    fun providePresenter(model: ConfigModel): LanConfigPresenter {
        return LanConfigPresenterImpl(model)
    }

    @Provides
    @LanConfigScope
    fun provideModel(localStorage: LocalStorage): ConfigModel {
        return ConfigModelImpl(localStorage)
    }
}