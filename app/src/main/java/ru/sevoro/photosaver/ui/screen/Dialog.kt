package ru.sevoro.photosaver.ui.screen

import android.app.Dialog
import android.content.DialogInterface
import android.content.DialogInterface.*
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import ru.sevoro.photosaver.R

/**
 * @author Sergey Vorobyev
 */

private const val KEY_NEUTRAL_BUTTON_TEXT_ID = "KEY_NEUTRAL_BUTTON_TEXT_ID"
private const val KEY_DIALOG_MASSAGE = "KEY_DIALOG_MASSAGE"

fun createCameraExplanationDialog(onButtonsClickListener: (dialog: DialogInterface, which: Int) -> Unit): DialogFragment {
    return createDialog(R.string.dialog_message_camera_explanation, onButtonsClickListener)
}

fun createGalleryExplanationDialog(onButtonsClickListener: (dialog: DialogInterface, which: Int) -> Unit): DialogFragment {
    return createDialog(R.string.dialog_message_gallery_explanation, onButtonsClickListener)
}

fun createAccountsExplanationDialog(onButtonsClickListener: (dialog: DialogInterface, which: Int) -> Unit): DialogFragment {
    return createDialog(R.string.dialog_message_accounts_explanation, onButtonsClickListener)
}

fun createCameraAndAccountsExplanationDialog(onButtonsClickListener: (dialog: DialogInterface, which: Int) -> Unit): DialogFragment {
    return createDialog(R.string.dialog_message_camera_and_accounts_explanation, onButtonsClickListener)
}

fun createGalleryAndAccountsExplanationDialog(onButtonsClickListener: (dialog: DialogInterface, which: Int) -> Unit): DialogFragment {
    return createDialog(R.string.dialog_message_gallery_and_accounts_explanation, onButtonsClickListener)
}

private fun createDialog(msg: Int, listener: (dialog: DialogInterface, which: Int) -> Unit): DialogFragment {
    val arguments = Bundle()
    arguments.putInt(KEY_DIALOG_MASSAGE, msg)
    val dialog = Dialog()
    dialog.arguments = arguments
    dialog.onButtonsClickListener = listener
    return dialog
}

class Dialog : DialogFragment() {

    lateinit var onButtonsClickListener: (dialog: DialogInterface, which: Int) -> Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val messageResId = arguments!!.getInt(KEY_DIALOG_MASSAGE)
        val neutralButtonTextResId = arguments!!.getInt(KEY_NEUTRAL_BUTTON_TEXT_ID)

        val builder = AlertDialog.Builder(activity!!)
                .setMessage(messageResId)
                .setPositiveButton(getString(R.string.button_yes), null)
                .setNegativeButton(getString(R.string.button_no), null)

        if (neutralButtonTextResId > 0) {
            builder.setNeutralButton(neutralButtonTextResId, null)
        }

        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.setOnShowListener {
            dialog.getButton(BUTTON_POSITIVE).setOnClickListener { onButtonsClickListener(dialog, BUTTON_POSITIVE) }
            dialog.getButton(BUTTON_NEGATIVE).setOnClickListener { onButtonsClickListener(dialog, BUTTON_NEGATIVE) }
            if (neutralButtonTextResId > 0) {
                dialog.getButton(BUTTON_NEUTRAL).setOnClickListener { onButtonsClickListener(dialog, BUTTON_NEUTRAL) }
            }
        }

        return dialog
    }

    override fun onDestroyView() {
        //https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && retainInstance) {
            dialog.setDismissMessage(null)
        }
        super.onDestroyView()
    }
}