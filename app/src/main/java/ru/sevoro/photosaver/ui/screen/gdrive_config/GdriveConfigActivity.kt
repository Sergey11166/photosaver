package ru.sevoro.photosaver.ui.screen.gdrive_config

import android.accounts.AccountManager
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.common.GoogleApiAvailability
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.drive.DriveScopes
import ru.sevoro.photosaver.App
import ru.sevoro.photosaver.DaggerComponentManager
import ru.sevoro.photosaver.R
import ru.sevoro.photosaver.ui.SimpleTextWatcher
import ru.sevoro.photosaver.ui.screen.base.ConfigBaseActivity
import ru.sevoro.photosaver.ui.screen.base.KEY_CONFIG_ID
import javax.inject.Inject

/**
 * @author Sergey Vorobyev
 */
const val KEY_SELECTED_ACCOUNT = "KEY_SELECTED_ACCOUNT"
const val REQUEST_CODE_ACCOUNT = 0
const val REQUEST_CODE_PLAY_SERVICES = 1

class GdriveConfigActivity : ConfigBaseActivity(), GdriveConfigView {

    @Inject
    lateinit var presenter: GdriveConfigPresenter

    private lateinit var aliasInput: TextInputLayout
    private lateinit var pathInput: TextInputLayout
    private lateinit var aliasEt: EditText
    private lateinit var pathEt: EditText
    private lateinit var accountTv: TextView

    override fun enableAliasInputError(enabled: Boolean) {
        aliasInput.isErrorEnabled = enabled
        aliasInput.error = if (enabled) getString(R.string.common_config_wrong_alias) else null
    }

    override fun enablePathInputError(enabled: Boolean) {
        pathInput.isErrorEnabled = enabled
        pathInput.error = if (enabled) getString(R.string.common_config_wrong_path) else null
    }

    override fun isAliasInputHasError() = aliasInput.isErrorEnabled

    override fun isPathInputHasError() = pathInput.isErrorEnabled

    override fun setSelectedAccount(account: String) {
        accountTv.text = account
    }

    override fun getSelectedAccount(): String {
        val account = accountTv.text.toString()
        return if (account == getString(R.string.gdrive_config_empty_account)) "" else account
    }

    override fun setInputAlias(alias: String) {
        aliasEt.setText(alias)
        if (alias.isNotEmpty()) presenter.onAliasTextChanged(alias)
    }

    override fun getEnteredAlias() = aliasEt.text.toString()

    override fun setInputPath(path: String) {
        pathEt.setText(path)
        if (path.isNotEmpty()) presenter.onPathTextChanged(path)
    }

    override fun getEnteredPath() = pathEt.text.toString()

    override fun startChoosingAccount() {
        val credential = GoogleAccountCredential
                .usingOAuth2(applicationContext, listOf(DriveScopes.DRIVE_FILE))
                .setBackOff(ExponentialBackOff())
        startActivityForResult(credential.newChooseAccountIntent(), REQUEST_CODE_ACCOUNT)
    }

    override fun getPlayServicesAvailabilityStatusCode() =
            GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)

    override fun isUserResolvablePlayServicesError(statusCode: Int) =
            GoogleApiAvailability.getInstance().isUserResolvableError(statusCode)

    override fun showResolvePlayServicesErrorDialog(statusCode: Int) {
        GoogleApiAvailability.getInstance()
                .getErrorDialog(this, statusCode, REQUEST_CODE_PLAY_SERVICES)
                .show()
    }

    override fun showPlayServicesNotAvailableToast() {
        toaster.showToast(R.string.error_message_play_services_not_available)
    }

    override fun showSelectAccountToast() {
        toaster.showToast(R.string.error_massage_account_not_selected)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gdrive_config)
        initWidgets()

        savedInstanceState?.let { accountTv.text = it.getString(KEY_SELECTED_ACCOUNT) }

        val configId = intent.extras?.get(KEY_CONFIG_ID)
        if (configId == null) presenter.onViewCreated(this)
        else presenter.onViewCreatedInEditMode(this, configId as String)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_item_done) presenter.onDoneActionClick()
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE_ACCOUNT -> {
                if (resultCode == RESULT_OK && data != null) {
                    presenter.onAccountSelected(data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME))
                }
            }
            REQUEST_CODE_PLAY_SERVICES -> presenter.onReturnFromPlayServices()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_SELECTED_ACCOUNT, accountTv.text.toString())
    }

    override fun onDestroy() {
        presenter.onViewDestroyed()
        if (isFinishing) DaggerComponentManager.unregisterScope(GdriveConfigScope::class.java)
        super.onDestroy()
    }

    private fun initWidgets() {
        aliasInput = findViewById(R.id.input_alias)
        pathInput = findViewById(R.id.input_path)
        aliasEt = findViewById(R.id.et_alias)
        pathEt = findViewById(R.id.et_path)

        aliasEt.addTextChangedListener(SimpleTextWatcher { presenter.onAliasTextChanged(it) })
        aliasEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) aliasEt.setSelection(aliasEt.text.length)
        }

        pathEt.addTextChangedListener(SimpleTextWatcher { presenter.onPathTextChanged(it) })
        pathEt.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) pathEt.setSelection(pathEt.text.length)
        }

        accountTv = findViewById(R.id.tv_account)
        accountTv.setOnClickListener { presenter.onEditAccountClick() }
        findViewById<ImageView>(R.id.iv_change_account).setOnClickListener { presenter.onEditAccountClick() }
    }

    override fun initDaggerComponent() {
        val componentKey = GdriveConfigComponent::class.java.name
        var component = DaggerComponentManager.getComponent<GdriveConfigComponent>(componentKey)
        if (component == null) {
            component = DaggerGdriveConfigComponent.builder()
                    .appComponent(App.appComponent)
                    .gdriveConfigModule(GdriveConfigModule())
                    .build()
            DaggerComponentManager.registerComponent(componentKey, component)
        }
        component?.inject(this)
    }
}