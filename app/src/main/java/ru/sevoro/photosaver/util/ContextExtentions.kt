package ru.sevoro.photosaver.util

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment.getExternalStoragePublicDirectory
import android.provider.MediaStore
import android.provider.Settings
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author Sergey Vorobyev
 */

fun Context.createImageFile(filename: String): File {
    val timestamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
            .format(Date(System.currentTimeMillis()))
    val imageFileName = "${filename}_$timestamp"
    val storageDir = getExternalStoragePublicDirectory(this.getString(this.applicationInfo.labelRes))
    storageDir.mkdirs()
    return createTempFile(imageFileName, ".jpg", storageDir)
}

fun Context.createMediaEntryInContentProvider(fileAbsolutePath: String) {
    val values = ContentValues()
            .also { it.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis()) }
            .also { it.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg") }
            .also { it.put(MediaStore.Images.Media.DATA, fileAbsolutePath) }
    this.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
}

private fun createTempFile(prefix: String, s: String?, directory: File?): File {
    var suffix = s
    if (prefix.length < 3)
        throw IllegalArgumentException("Prefix string too short")
    if (suffix == null)
        suffix = ".tmp"

    val tmpdir = directory ?: File(System.getProperty("java.io.tmpdir", "."))
    var f: File
    try {
        do {
            f = File(tmpdir, prefix + suffix)
        } while (f.exists())
        if (!f.createNewFile())
            throw IOException("Unable to create temporary file")
    } catch (se: SecurityException) {
        // don't reveal temporary directory location
        if (directory == null)
            throw SecurityException("Unable to create temporary file")
        throw se
    }
    return f
}


/**
 * Open system settings of this app
 */
fun Context.goToSettingsApp() {
    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:" + this.packageName))
    this.startActivity(intent)
}

/**
 * Open url by one of installed browsers
 */
fun Context.openUrlInBrowser(url: String) {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    this.startActivity(intent)
}