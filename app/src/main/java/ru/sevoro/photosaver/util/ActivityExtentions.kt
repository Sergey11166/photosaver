package ru.sevoro.photosaver.util

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import ru.sevoro.photosaver.BuildConfig.APPLICATION_ID
import java.io.File


/**
 * @author Sergey Vorobyev
 */

/**
 * Open camera app to take photo
 *
 * @param tempFile    File to write items from camera
 * *
 * @param requestCode Code for [Activity.onActivityResult]
 */
fun Activity.goToCameraApp(tempFile: File, requestCode: Int) {
    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    val photoUri = FileProvider.getUriForFile(this, APPLICATION_ID + ".file_provider", tempFile)
    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)

    val resolveInfoList = this.packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
    resolveInfoList
            .map { it.activityInfo.packageName }
            .forEach {
                this.grantUriPermission(it, photoUri,
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }

    this.startActivityForResult(intent, requestCode)
}