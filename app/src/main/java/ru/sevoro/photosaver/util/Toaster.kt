package ru.sevoro.photosaver.util

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.support.annotation.StringRes
import android.widget.Toast

/**
 * @author Sergey Vorobyev
 */
class Toaster(private val context: Context) {

    private val handler = Handler(Looper.getMainLooper())

    fun showToast(@StringRes id: Int, vararg args: Any) {
        handler.post { Toast.makeText(context, context.getString(id, args), Toast.LENGTH_SHORT).show() }
    }

    fun showToast(msg: String) {
        handler.post { Toast.makeText(context, msg, Toast.LENGTH_SHORT).show() }
    }
}