package ru.sevoro.photosaver

/**
 * @author Sergey Vorobyev
 */

object DaggerComponentManager {

    private var componentsStore = HashMap<String, Any>()

    fun <T> getComponent(key: String): T? {
        @Suppress("UNCHECKED_CAST")
        return componentsStore[key] as T
    }

    fun registerComponent(key: String, component: Any) {
        componentsStore.put(key, component)
    }

    fun unregisterComponent(key: String) {
        componentsStore.remove(key)
    }

    fun unregisterScope(scope: Class<out Annotation>) {
        val iterator = componentsStore.entries.iterator()
        while (iterator.hasNext()) {
            val entry = iterator.next()
            if (entry.key.contains(scope.name)) {
                iterator.remove()
            }
        }
    }
}