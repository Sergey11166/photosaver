package ru.sevoro.photosaver

import org.mockito.Mockito

/**
 * @author Sergey Vorobyev
 */

fun <T> nullableAny(): T {
    Mockito.any<T>()
    @Suppress("UNCHECKED_CAST")
    return null as T
}
