package ru.sevoro.photosaver.ui.screen.http_config

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import ru.sevoro.photosaver.data.config.*
import ru.sevoro.photosaver.nullableAny
import ru.sevoro.photosaver.ui.screen.base.ConfigModel
import org.mockito.Mockito.`when` as _when

/**
 * @author Sergey Vorobyev
 */
@Suppress("FunctionName")
class HttpConfigPresenterImplTest {

    private val mockModel = mock(ConfigModel::class.java)
    private val mockView = mock(HttpConfigView::class.java)
    private val presenter = HttpConfigPresenterImpl(mockModel)

    @Before
    fun setUp() {
        presenter.onViewCreated(mockView)
    }

    @Test
    fun onViewCreated() {
        verify(mockView, only()).addHeader("")
    }

    @Test
    fun onViewCreatedInEditMode_WhenAuthTypeIsNone() {
        val id = "id"
        val alias = "alias"
        val path = "path"
        val formParam = "param"
        val config = HttpConfig(alias, path, formParam = formParam)

        _when(mockModel.getConfig<Config>(id)).thenReturn(config)

        presenter.onViewCreatedInEditMode(mockView, id)

        verify(mockView, times(1)).setInputAlias(alias)
        verify(mockView, times(1)).setInputPath(path)
        verify(mockView, times(1)).setInputFormParam(formParam)
        verify(mockView, times(1)).selectHttpMethod(HTTP_METHOD_POST)
        verify(mockView, times(1)).selectAuthType(AUTH_NONE)
        verify(mockView, times(1)).showHeaders(config.headers)
        verify(mockView, times(2)).addHeader("")

        verify(mockView, never()).setInputUsername(nullableAny())
        verify(mockView, never()).setInputPassword(nullableAny())
    }

    @Test
    fun onViewCreatedInEditMode_WhenAuthTypeIsBasic() {
        val id = "id"
        val alias = "alias"
        val path = "path"
        val formParam = "param"
        val username = "username"
        val password = "password"
        val config = HttpConfig(alias, path, formParam = formParam, authType = AUTH_BASIC,
                username = username, password = password)

        _when(mockModel.getConfig<Config>(id)).thenReturn(config)

        presenter.onViewCreatedInEditMode(mockView, id)

        verify(mockView, times(1)).setInputAlias(alias)
        verify(mockView, times(1)).setInputPath(path)
        verify(mockView, times(1)).setInputFormParam(formParam)
        verify(mockView, times(1)).selectHttpMethod(HTTP_METHOD_POST)
        verify(mockView, times(1)).selectAuthType(AUTH_BASIC)
        verify(mockView, times(1)).setInputUsername(username)
        verify(mockView, times(1)).setInputPassword(password)
        verify(mockView, times(1)).showHeaders(config.headers)
        verify(mockView, times(2)).addHeader("")
    }

    @Test
    fun onPathTextChanged_WhenTextIsValid() {
        presenter.onPathTextChanged("http://")
        presenter.onPathTextChanged("https://")
        verify(mockView, times(2)).enablePathInputError(false)
    }

    @Test
    fun onPathTextChanged_WhenTextIsNotValid() {
        presenter.onPathTextChanged("not_valid_path")
        verify(mockView, times(1)).enablePathInputError(true)
    }

    @Test
    fun onFormParamTextChanged_WhenTextIsValid() {
        presenter.onFormParamTextChanged("valid_param")
        verify(mockView, times(1)).enableFormParamInputError(false)
    }

    @Test
    fun onFormParamTextChanged_WhenTextIsNotValid() {
        presenter.onFormParamTextChanged("")
        verify(mockView, times(1)).enableFormParamInputError(true)
    }

    @Test
    fun onTypeAuthSelected_WhenSelectNone() {
        presenter.onTypeAuthSelected(AUTH_NONE)
        verify(mockView, times(1)).setUserInputVisibility(false)
        verify(mockView, times(1)).setPasswordInputVisibility(false)
    }

    @Test
    fun onTypeAuthSelected_WhenSelectBasic() {
        presenter.onTypeAuthSelected(AUTH_BASIC)
        verify(mockView, times(1)).setUserInputVisibility(true)
        verify(mockView, times(1)).setPasswordInputVisibility(true)
    }

    @Test
    fun onHeaderTextChanged_WhenAddHeader() {
        val existConfig = HttpConfig("alias", "http://path", formParam = "param",
                headers = mutableListOf())
        val enteredPosition = 0
        val enteredHeader = "newHeaderText"

        _when(mockModel.getConfig<Config>(nullableAny())).thenReturn(existConfig)

        presenter.onViewCreatedInEditMode(mockView, "id")
        presenter.onHeaderTextChanged(enteredPosition, enteredHeader)

        verify(mockView, times(3)).addHeader("")
        verify(mockView, never()).deleteHeader(anyInt())
        assertEquals(enteredHeader, existConfig.headers[enteredPosition])
    }

    @Test
    fun onHeaderTextChanged_WhenRemoveHeader() {
        val existConfig = HttpConfig("alias", "http://path", formParam = "param",
                headers = mutableListOf("header"))
        val enteredPosition = 0
        val enteredHeader = ""

        _when(mockModel.getConfig<Config>(nullableAny())).thenReturn(existConfig)

        presenter.onViewCreatedInEditMode(mockView, "id")
        presenter.onHeaderTextChanged(enteredPosition, enteredHeader)

        verify(mockView, times(1)).deleteHeader(enteredPosition)
        assertTrue(existConfig.headers.isEmpty())
    }

    @Test
    fun onHeaderTextChanged_WhenOnlyEditHeader() {
        val existConfig = HttpConfig("alias", "http://path", formParam = "param",
                headers = mutableListOf("header"))
        val enteredPosition = 0
        val enteredHeader = "newHeaderText"

        _when(mockModel.getConfig<Config>(nullableAny())).thenReturn(existConfig)
        presenter.onViewCreatedInEditMode(mockView, "id")
        presenter.onHeaderTextChanged(enteredPosition, enteredHeader)

        verify(mockView, never()).deleteHeader(anyInt())
        assertEquals(enteredHeader, existConfig.headers[enteredPosition])
    }

    @Test
    fun onHeaderDeleteClick() {
        val deletePosition = 1
        val existConfig = HttpConfig("alias", "path", headers = mutableListOf("header1", "header2"))

        _when(mockModel.getConfig<Config>(nullableAny())).thenReturn(existConfig)
        presenter.onViewCreatedInEditMode(mockView, "someId")
        presenter.onHeaderDeleteClick(deletePosition)

        verify(mockView, times(1)).deleteHeader(deletePosition)
        assertTrue(existConfig.headers.size == 1)
    }

    @Test
    fun onDoneActionClick_WhenNotAllInputsIsValid() {
        val enteredAlias = "valid alias"
        val enteredPath = "not valid path"
        val enteredFormParam = "formParam"

        _when(mockView.getEnteredAlias()).thenReturn(enteredAlias)
        _when(mockView.getEnteredPath()).thenReturn(enteredPath)
        _when(mockView.getEnteredFormParam()).thenReturn(enteredFormParam)

        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(false)
        verify(mockView, times(1)).enablePathInputError(true)
        verify(mockView, times(1)).enableFormParamInputError(false)
        verify(mockView, never()).navigateToMainScreen()
        verify(mockModel, never()).saveConfig(nullableAny())
    }

    @Test
    fun onDoneActionClick_WhenAllInputsIsValidAndSelectAuthNone() {
        val enteredAlias = "valid alias"
        val enteredPath = "http://valid_path"
        val enteredFormParam = "formParam"
        val expectedNewConfig = HttpConfig(enteredAlias, enteredPath, formParam = enteredFormParam)

        _when(mockView.getEnteredAlias()).thenReturn(enteredAlias)
        _when(mockView.getEnteredPath()).thenReturn(enteredPath)
        _when(mockView.getEnteredFormParam()).thenReturn(enteredFormParam)
        _when(mockView.getSelectedHttpMethod()).thenReturn(HTTP_METHOD_POST)
        _when(mockView.getSelectedAuthType()).thenReturn(AUTH_NONE)

        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(false)
        verify(mockView, times(1)).enablePathInputError(false)
        verify(mockView, times(1)).enableFormParamInputError(false)
        verify(mockView, times(1)).navigateToMainScreen()
        verify(mockModel, times(1)).saveConfig(expectedNewConfig)

        verify(mockView, never()).getEnteredUsername()
        verify(mockView, never()).getEnteredPassword()
    }

    @Test
    fun onDoneActionClick_WhenAllInputsIsValidAndSelectAuthBasic() {
        val enteredAlias = "valid alias"
        val enteredPath = "http://valid_path"
        val enteredFormParam = "formParam"
        val enteredUsername = "username"
        val enteredPassword = "password"
        val expectedNewConfig = HttpConfig(enteredAlias, enteredPath, formParam = enteredFormParam,
                authType = AUTH_BASIC, username = enteredUsername, password = enteredPassword)

        _when(mockView.getEnteredAlias()).thenReturn(enteredAlias)
        _when(mockView.getEnteredPath()).thenReturn(enteredPath)
        _when(mockView.getEnteredFormParam()).thenReturn(enteredFormParam)
        _when(mockView.getEnteredUsername()).thenReturn(enteredUsername)
        _when(mockView.getEnteredPassword()).thenReturn(enteredPassword)
        _when(mockView.getSelectedHttpMethod()).thenReturn(HTTP_METHOD_POST)
        _when(mockView.getSelectedAuthType()).thenReturn(AUTH_BASIC)

        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(false)
        verify(mockView, times(1)).enablePathInputError(false)
        verify(mockView, times(1)).enableFormParamInputError(false)
        verify(mockView, times(1)).getEnteredUsername()
        verify(mockView, times(1)).getEnteredPassword()
        verify(mockModel, times(1)).saveConfig(expectedNewConfig)
        verify(mockView, times(1)).navigateToMainScreen()
    }
}