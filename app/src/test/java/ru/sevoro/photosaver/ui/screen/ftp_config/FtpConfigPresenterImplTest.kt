package ru.sevoro.photosaver.ui.screen.ftp_config

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import ru.sevoro.photosaver.data.config.Config
import ru.sevoro.photosaver.data.config.FtpConfig
import ru.sevoro.photosaver.nullableAny
import ru.sevoro.photosaver.ui.screen.base.ConfigModel
import org.mockito.Mockito.`when` as _when

/**
 * @author Sergey Vorobyev
 */
@Suppress("FunctionName")
class FtpConfigPresenterImplTest {

    private val mockView = mock(FtpConfigView::class.java)
    private val mockModel = mock(ConfigModel::class.java)
    private val presenter = FtpConfigPresenterImpl(mockModel)

    @Before
    fun setUp() {
        presenter.onViewCreated(mockView)
    }

    @Test
    fun onViewCreatedInEditMode() {
        val id = "id"
        val alias = "alias"
        val path = "path"
        val username = "username"
        val password = "password"

        _when(mockModel.getConfig<Config>(id)).thenReturn(FtpConfig(alias, path,
                username = username, password = password))

        presenter.onViewCreatedInEditMode(mockView, id)

        verify(mockView, times(1)).setInputAlias(alias)
        verify(mockView, times(1)).setInputPath(path)
        verify(mockView, times(1)).setInputUsername(username)
        verify(mockView, times(1)).setInputPassword(password)
    }

    @Test
    fun onPathTextChanged_WhenTextIsValid() {
        presenter.onPathTextChanged("ftp://")
        presenter.onPathTextChanged("ftps://")
        verify(mockView, times(2)).enablePathInputError(false)
    }

    @Test
    fun onPathTextChanged_WhenTextIsNotValid() {
        presenter.onPathTextChanged("not_valid_path")
        verify(mockView, times(1)).enablePathInputError(true)
    }

    @Test
    fun onUsernameTextChanged_WhenTextIsValid() {
        val username = "username"
        presenter.onUsernameTextChanged(username)
        verify(mockView, times(1)).enableUsernameInputError(false)
    }

    @Test
    fun onUsernameTextChanged_WhenTextIsNotValid() {
        val username = ""
        presenter.onUsernameTextChanged(username)
        verify(mockView, times(1)).enableUsernameInputError(true)
    }

    @Test
    fun onDoneActionClick_WhenNotAllInputsIsValid() {
        val enteredAlias = "valid alias"
        val enteredPath = "not valid path"
        val enteredUsername = "username"

        _when(mockView.getEnteredAlias()).thenReturn(enteredAlias)
        _when(mockView.getEnteredPath()).thenReturn(enteredPath)
        _when(mockView.getEnteredUsername()).thenReturn(enteredUsername)

        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(false)
        verify(mockView, times(1)).enablePathInputError(true)
        verify(mockView, times(1)).enableUsernameInputError(false)
        verify(mockView, never()).navigateToMainScreen()
        verify(mockModel, never()).saveConfig(nullableAny())
    }

    @Test
    fun onDoneActionClick_WhenAllInputsIsValidAndNotEditMode() {
        val alias = "alias"
        val path = "ftp://path"
        val username = "username"
        val password = "pass"
        val expectedConfig = FtpConfig(alias, path, username = username, password = password)

        _when(mockView.getEnteredAlias()).thenReturn(alias)
        _when(mockView.getEnteredPath()).thenReturn(path)
        _when(mockView.getEnteredUsername()).thenReturn(username)
        _when(mockView.getEnteredPassword()).thenReturn(password)

        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(false)
        verify(mockView, times(1)).enablePathInputError(false)
        verify(mockView, times(1)).enableUsernameInputError(false)
        verify(mockModel, times(1)).saveConfig(expectedConfig)
        verify(mockView, times(1)).navigateToMainScreen()
    }

    @Test
    fun onDoneActionClick_WhenAllInputsIsValidAndEditMode() {
        val configId = "id"
        val alias = "alias"
        val path = "ftp://path"
        val username = "username"
        val password = "pass"
        val expectedConfig = FtpConfig(alias, path, username = username, password = password)
        expectedConfig.id = configId

        _when(mockModel.getConfig<Config>(configId)).thenReturn(expectedConfig)
        _when(mockView.getEnteredAlias()).thenReturn(alias)
        _when(mockView.getEnteredPath()).thenReturn(path)
        _when(mockView.getEnteredUsername()).thenReturn(username)
        _when(mockView.getEnteredPassword()).thenReturn(password)

        presenter.onViewCreatedInEditMode(mockView, configId)
        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(false)
        verify(mockView, times(1)).enablePathInputError(false)
        verify(mockView, times(1)).enableUsernameInputError(false)
        verify(mockModel, times(1)).saveConfig(expectedConfig)
        verify(mockView, times(1)).navigateToMainScreen()
    }
}