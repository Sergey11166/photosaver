package ru.sevoro.photosaver.ui.screen.main

import android.content.Intent
import com.google.android.gms.auth.UserRecoverableAuthException
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import ru.sevoro.photosaver.data.config.*
import ru.sevoro.photosaver.nullableAny
import org.mockito.Mockito.`when` as _when


/**
 * @author Sergey Vorobyev
 */

private const val NOT_VALID_SUB_DIR_TEXT = ""
private const val VALID_SUB_DIR_TEXT = "valid text"

@Suppress("FunctionName")
class MainPresenterImplTest {

    private val mockView = Mockito.mock(MainView::class.java)
    private val mockModel = Mockito.mock(MainModel::class.java)
    private val presenter = MainPresenterImpl(mockModel)

    @Before
    fun setUp() {
        _when(mockModel.getAdDisablingUpdateObs()).thenReturn(Observable.just(true))
    }

    @Test
    fun onViewCreated_WhenAdDisabled() {
        _when(mockModel.getAdDisablingUpdateObs()).thenReturn(Observable.just(true))
        presenter.onViewCreated(mockView)
        verify(mockView, only()).hideAd()
    }

    @Test
    fun onViewCreated_WhenAdNotDisabled() {
        _when(mockModel.getAdDisablingUpdateObs()).thenReturn(Observable.just(false))
        presenter.onViewCreated(mockView)
        verify(mockView, only()).showAd()
    }

    @Test
    fun onViewResumed_WhenSavedConfigsExist() {
        val selectedItemId = "id2"
        val subDirText = "subDir"
        val savedConfigs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_HTTP, "alias", "desc",
                        subDirChecked = true, subDirText = subDirText))

        _when(mockModel.getAllConfigs()).thenReturn(savedConfigs)
        _when(mockModel.getSelectedConfigId()).thenReturn(selectedItemId)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()

        verify(mockView, times(1)).showMainLayout(anyBoolean())
        verify(mockView, times(1)).showConfigs(savedConfigs)
        verify(mockView, times(1)).selectItem(1)
        verify(mockView, times(1)).setSubDirChecked(true)
        verify(mockView, times(1)).setSubDirText(subDirText)
        verify(mockView, times(1)).setEnabledSubDirCheckbox(false)
        verify(mockView, never()).showNoConfigLayout(anyBoolean())
    }

    @Test
    fun onViewResumed_WhenSavedConfigsDoNotExist() {
        val savedConfigs = emptyList<ConfigUIModel>()

        _when(mockModel.getAllConfigs()).thenReturn(savedConfigs)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()

        verify(mockView, times(1)).showNoConfigLayout(anyBoolean())

        verify(mockView, never()).showMainLayout(anyBoolean())
        verify(mockView, never()).showConfigs(anyList())
        verify(mockView, never()).selectItem(anyInt())
        verify(mockView, never()).setSubDirChecked(anyBoolean())
        verify(mockView, never()).setSubDirText(anyString())
    }

    @Test
    fun onConfigureButtonClick() {
        presenter.onViewCreated(mockView)
        presenter.onAddConfigureButtonClick()
        verify(mockView, times(1)).navigateToTypeSelectionScreen()
    }

    @Test
    fun onSettingsActionClick() {
        presenter.onViewCreated(mockView)
        presenter.onSettingsActionClick()
        verify(mockView, times(1)).navigateToSettingsScreen()
    }

    @Test
    fun onAddActionClick() {
        presenter.onViewCreated(mockView)
        presenter.onAddActionClick()
        verify(mockView, times(1)).navigateToTypeSelectionScreen()
    }

    @Test
    fun onItemSelected() {
        val selectedItemPos = 1
        val subDirText = "subDir"
        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_LAN, "alias", "desc",
                        subDirChecked = true, subDirText = subDirText))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)

        verify(mockModel, times(1)).saveSelectedConfigId(configs[selectedItemPos].id)
        verify(mockView, times(1)).selectItem(selectedItemPos)
        verify(mockView, times(1)).setSubDirChecked(true)
        verify(mockView, times(1)).setSubDirText(subDirText)
        verify(mockView, times(2)).setEnabledSubDirCheckbox(true)
    }

    @Test
    fun onItemEditClick() {
        val editPosition = 1
        val expectedId = "id2"
        val expectedType = TYPE_LAN
        val configs = listOf(
                ConfigUIModel("id1", TYPE_FTP, "alias", "desc"),
                ConfigUIModel(expectedId, expectedType, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemEditClick(editPosition)

        verify(mockView, times(1)).navigateToEditConfigScreen(expectedId, expectedType)
    }

    @Test
    fun onItemDeleteClick_WhenDeleteSingleItem() {
        val deleteItemPos = 0
        val configs = listOf(ConfigUIModel("id1", TYPE_LAN, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemDeleteClick(deleteItemPos)

        verify(mockModel, times(1)).deleteConfig(configs[deleteItemPos].id)
        verify(mockView, times(1)).showNoConfigLayout(anyBoolean())

        verify(mockModel, never()).saveSelectedConfigId(anyString())
        verify(mockView, never()).deleteItem(deleteItemPos)
    }

    @Test
    fun onItemDeleteClick_WhenDeleteSelectedAndFirstItem() {
        val deleteItemPos = 0
        val selectedItemPos = 0
        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_LAN, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockModel.getSelectedConfigId()).thenReturn(configs[selectedItemPos].id)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemDeleteClick(deleteItemPos)

        verify(mockModel, times(1)).deleteConfig(configs[deleteItemPos].id)
        verify(mockView, times(2)).selectItem(0)
        verify(mockView, times(2)).setSubDirChecked(false)
        verify(mockView, times(2)).setSubDirText("")
        verify(mockView, times(2)).setEnabledSubDirCheckbox(true)
        verify(mockView, times(1)).deleteItem(deleteItemPos)
        verify(mockModel, times(1)).saveSelectedConfigId("id2")
        verify(mockView, never()).showNoConfigLayout(anyBoolean())
    }

    @Test
    fun onItemDeleteClick_WhenDeleteSelectedAndNotFirstItem() {
        val deleteItemPos = 1
        val selectedItemPos = 1
        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_LAN, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockModel.getSelectedConfigId()).thenReturn(configs[selectedItemPos].id)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemDeleteClick(deleteItemPos)

        verify(mockModel, times(1)).deleteConfig(configs[deleteItemPos].id)
        verify(mockView, times(1)).selectItem(deleteItemPos - 1)
        verify(mockView, times(2)).setSubDirChecked(false)
        verify(mockView, times(2)).setSubDirText("")
        verify(mockView, times(2)).setEnabledSubDirCheckbox(true)
        verify(mockView, times(1)).deleteItem(deleteItemPos)
        verify(mockModel, times(1)).saveSelectedConfigId("id1")
        verify(mockView, never()).showNoConfigLayout(anyBoolean())
    }

    @Test
    fun onItemDeleteClick_WhenDeleteNotSelectedItem() {
        val deleteItemPos = 1
        val selectedItemPos = 2
        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id3", TYPE_LAN, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockModel.getSelectedConfigId()).thenReturn(configs[selectedItemPos].id)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemDeleteClick(deleteItemPos)

        verify(mockView, times(1)).deleteItem(deleteItemPos)

        verify(mockModel, never()).saveSelectedConfigId(anyString())
        verify(mockView, never()).showNoConfigLayout(anyBoolean())
    }

    @Test
    fun onSubDirTextChanged_WhenTextIsValidAndSubDirIsChecked() {
        val selectedItemPos = 2
        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id3", TYPE_LAN, "alias", "desc"))

        val saveConfig = LanConfig("id", "path")

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockModel.getSelectedConfigId()).thenReturn(configs[selectedItemPos].id)
        _when(mockModel.getConfig(configs[selectedItemPos])).thenReturn(saveConfig)
        _when(mockView.isSubDirChecked()).thenReturn(true)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onSubDirTextChanged(VALID_SUB_DIR_TEXT)

        verify(mockView, times(1)).enableGalleryAndCameraButtons(true)
        verify(mockModel, times(1)).saveConfig(saveConfig)
    }

    @Test
    fun onSubDirTextChanged_WhenTextIsValidAndSubDirNotChecked() {
        val selectedItemPos = 2
        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id3", TYPE_LAN, "alias", "desc"))

        val saveConfig = LanConfig("id", "path")

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockModel.getSelectedConfigId()).thenReturn(configs[selectedItemPos].id)
        _when(mockModel.getConfig(configs[selectedItemPos])).thenReturn(saveConfig)
        _when(mockView.isSubDirChecked()).thenReturn(false)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onSubDirTextChanged(VALID_SUB_DIR_TEXT)

        verify(mockView, times(1)).enableGalleryAndCameraButtons(true)
        verify(mockModel, times(1)).saveConfig(saveConfig)
    }

    @Test
    fun onSubDirTextChanged_WhenTextIsNotValidAndSubDirIsChecked() {
        val selectedItemPos = 2
        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id3", TYPE_LAN, "alias", "desc"))

        val saveConfig = LanConfig("id", "path")

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockModel.getSelectedConfigId()).thenReturn(configs[selectedItemPos].id)
        _when(mockModel.getConfig(configs[selectedItemPos])).thenReturn(saveConfig)
        _when(mockView.isSubDirChecked()).thenReturn(true)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onSubDirTextChanged(NOT_VALID_SUB_DIR_TEXT)

        verify(mockView, times(1)).enableGalleryAndCameraButtons(false)
        verify(mockModel, times(1)).saveConfig(saveConfig)
    }

    @Test
    fun onSubDirTextChanged_WhenTextIsNotValidAndSubDirNotChecked() {
        val selectedItemPos = 2
        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id3", TYPE_LAN, "alias", "desc"))

        val saveConfig = LanConfig("id", "path")

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockModel.getSelectedConfigId()).thenReturn(configs[selectedItemPos].id)
        _when(mockModel.getConfig(configs[selectedItemPos])).thenReturn(saveConfig)
        _when(mockView.isSubDirChecked()).thenReturn(false)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onSubDirTextChanged(NOT_VALID_SUB_DIR_TEXT)

        verify(mockView, times(1)).enableGalleryAndCameraButtons(true)
        verify(mockModel, times(1)).saveConfig(saveConfig)
    }

    @Test
    fun onSubDirChecked_WhenCheckedFalse() {
        val selectedItemPos = 2
        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id3", TYPE_LAN, "alias", "desc"))

        val saveConfig = LanConfig("id", "path", subDirChecked = true)

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockModel.getSelectedConfigId()).thenReturn(configs[selectedItemPos].id)
        _when(mockModel.getConfig(configs[selectedItemPos])).thenReturn(saveConfig)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onSubDirChecked(false)

        verify(mockView, times(1)).enableGalleryAndCameraButtons(true)
        verify(mockModel, times(1)).saveConfig(saveConfig)
        verify(mockView, never()).getEnteredSubDir()
        assertEquals(saveConfig.subDirChecked, false)
    }

    @Test
    fun onSubDirChecked_WhenCheckedTrueAndSubDirTextValid() {
        val selectedItemPos = 2
        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id3", TYPE_LAN, "alias", "desc"))

        val saveConfig = LanConfig("id", "path", subDirChecked = false)

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockModel.getSelectedConfigId()).thenReturn(configs[selectedItemPos].id)
        _when(mockModel.getConfig(configs[selectedItemPos])).thenReturn(saveConfig)
        _when(mockView.getEnteredSubDir()).thenReturn(VALID_SUB_DIR_TEXT)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onSubDirChecked(true)

        verify(mockView, times(1)).enableGalleryAndCameraButtons(true)
        verify(mockModel, times(1)).saveConfig(saveConfig)
        assertEquals(saveConfig.subDirChecked, true)
    }

    @Test
    fun onSubDirChecked_WhenCheckedTrueAndSubDirTextNotValid() {
        val selectedItemPos = 2
        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id3", TYPE_LAN, "alias", "desc"))

        val saveConfig = LanConfig("id", "path", subDirChecked = false)

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockModel.getSelectedConfigId()).thenReturn(configs[selectedItemPos].id)
        _when(mockModel.getConfig(configs[selectedItemPos])).thenReturn(saveConfig)
        _when(mockView.getEnteredSubDir()).thenReturn(NOT_VALID_SUB_DIR_TEXT)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onSubDirChecked(true)

        verify(mockView, times(1)).enableGalleryAndCameraButtons(false)
        verify(mockModel, times(1)).saveConfig(saveConfig)
        assertEquals(saveConfig.subDirChecked, true)
    }

    @Test
    fun onCameraButtonClick_WhenCameraAndAccountsPermissionGrantedAndGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkCameraPermission()).thenReturn(true)
        _when(mockView.checkAccountsPermission()).thenReturn(true)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onCameraButtonClick()

        verify(mockView, times(1)).checkCameraPermission()
        verify(mockView, times(1)).checkAccountsPermission()
        verify(mockView, times(1)).startCameraApp()

        verify(mockView, never()).requestCameraPermission()
        verify(mockView, never()).requestAccountsPermissionForCamera()
        verify(mockView, never()).requestCameraAndAccountsPermissions()
    }

    @Test
    fun onCameraButtonClick_WhenCameraPermissionGrantedAndAccountDeniedAndGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkCameraPermission()).thenReturn(true)
        _when(mockView.checkAccountsPermission()).thenReturn(false)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onCameraButtonClick()

        verify(mockView, times(1)).checkCameraPermission()
        verify(mockView, times(1)).checkAccountsPermission()
        verify(mockView, times(1)).requestAccountsPermissionForCamera()

        verify(mockView, never()).startCameraApp()
        verify(mockView, never()).requestCameraPermission()
        verify(mockView, never()).requestCameraAndAccountsPermissions()
    }

    @Test
    fun onCameraButtonClick_WhenCameraPermissionDeniedAndAccountGrantedAndGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkCameraPermission()).thenReturn(false)
        _when(mockView.checkAccountsPermission()).thenReturn(true)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onCameraButtonClick()

        verify(mockView, times(1)).checkCameraPermission()
        verify(mockView, times(1)).checkAccountsPermission()
        verify(mockView, times(1)).requestCameraPermission()

        verify(mockView, never()).startCameraApp()
        verify(mockView, never()).requestAccountsPermissionForCamera()
        verify(mockView, never()).requestCameraAndAccountsPermissions()
    }

    @Test
    fun onCameraButtonClick_WhenCameraAndAccountsPermissionDeniedAndGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkCameraPermission()).thenReturn(false)
        _when(mockView.checkAccountsPermission()).thenReturn(false)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onCameraButtonClick()

        verify(mockView, times(1)).checkCameraPermission()
        verify(mockView, times(1)).checkAccountsPermission()
        verify(mockView, times(1)).requestCameraAndAccountsPermissions()

        verify(mockView, never()).startCameraApp()
        verify(mockView, never()).requestCameraPermission()
        verify(mockView, never()).requestAccountsPermissionForCamera()
    }

    @Test
    fun onCameraButtonClick_WhenCameraPermissionGrantedAndNotGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 0

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkCameraPermission()).thenReturn(true)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onCameraButtonClick()

        verify(mockView, times(1)).checkCameraPermission()
        verify(mockView, times(1)).startCameraApp()

        verify(mockView, never()).checkAccountsPermission()
        verify(mockView, never()).requestCameraPermission()
        verify(mockView, never()).requestCameraAndAccountsPermissions()
        verify(mockView, never()).requestAccountsPermissionForCamera()
    }

    @Test
    fun onCameraButtonClick_WhenCameraPermissionDeniedAndNotGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 0

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkCameraPermission()).thenReturn(false)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onCameraButtonClick()

        verify(mockView, times(1)).checkCameraPermission()
        verify(mockView, times(1)).requestCameraPermission()

        verify(mockView, never()).startCameraApp()
        verify(mockView, never()).checkAccountsPermission()
        verify(mockView, never()).requestCameraAndAccountsPermissions()
        verify(mockView, never()).requestAccountsPermissionForCamera()
    }

    @Test
    fun onGalleryButtonClick_WhenGalleryAndAccountsPermissionGrantedAndGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkGalleryPermission()).thenReturn(true)
        _when(mockView.checkAccountsPermission()).thenReturn(true)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onGalleryButtonClick()

        verify(mockView, times(1)).checkGalleryPermission()
        verify(mockView, times(1)).checkAccountsPermission()
        verify(mockView, times(1)).startGalleryApp()

        verify(mockView, never()).requestGalleryPermission()
        verify(mockView, never()).requestAccountsPermissionForGallery()
        verify(mockView, never()).requestGalleryAndAccountsPermissions()
    }

    @Test
    fun onGalleryButtonClick_WhenGalleryPermissionGrantedAndAccountDeniedAndGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkGalleryPermission()).thenReturn(true)
        _when(mockView.checkAccountsPermission()).thenReturn(false)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onGalleryButtonClick()

        verify(mockView, times(1)).checkGalleryPermission()
        verify(mockView, times(1)).checkAccountsPermission()
        verify(mockView, times(1)).requestAccountsPermissionForGallery()

        verify(mockView, never()).startGalleryApp()
        verify(mockView, never()).requestGalleryPermission()
        verify(mockView, never()).requestGalleryAndAccountsPermissions()
    }

    @Test
    fun onGalleryButtonClick_WhenGalleryPermissionDeniedAndAccountGrantedAndGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkGalleryPermission()).thenReturn(false)
        _when(mockView.checkAccountsPermission()).thenReturn(true)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onGalleryButtonClick()

        verify(mockView, times(1)).checkGalleryPermission()
        verify(mockView, times(1)).checkAccountsPermission()
        verify(mockView, times(1)).requestGalleryPermission()

        verify(mockView, never()).startGalleryApp()
        verify(mockView, never()).requestAccountsPermissionForGallery()
        verify(mockView, never()).requestGalleryAndAccountsPermissions()
    }

    @Test
    fun onGalleryButtonClick_WhenGalleryAndAccountsPermissionDeniedAndGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkGalleryPermission()).thenReturn(false)
        _when(mockView.checkAccountsPermission()).thenReturn(false)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onGalleryButtonClick()

        verify(mockView, times(1)).checkGalleryPermission()
        verify(mockView, times(1)).checkAccountsPermission()
        verify(mockView, times(1)).requestGalleryAndAccountsPermissions()

        verify(mockView, never()).startGalleryApp()
        verify(mockView, never()).requestGalleryPermission()
        verify(mockView, never()).requestAccountsPermissionForGallery()
    }

    @Test
    fun onGalleryButtonClick_WhenGalleryPermissionGrantedAndNotGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 0

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkGalleryPermission()).thenReturn(true)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onGalleryButtonClick()

        verify(mockView, times(1)).checkGalleryPermission()
        verify(mockView, times(1)).startGalleryApp()

        verify(mockView, never()).checkAccountsPermission()
        verify(mockView, never()).requestGalleryPermission()
        verify(mockView, never()).requestGalleryAndAccountsPermissions()
        verify(mockView, never()).requestAccountsPermissionForGallery()
    }

    @Test
    fun onGalleryButtonClick_WhenGalleryPermissionDeniedAndNotGdriveConfigSelected() {
        val subDir = "sub_dir"
        val selectedItemPos = 0

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.checkGalleryPermission()).thenReturn(false)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onGalleryButtonClick()

        verify(mockView, times(1)).checkGalleryPermission()
        verify(mockView, times(1)).requestGalleryPermission()

        verify(mockView, never()).startGalleryApp()
        verify(mockView, never()).checkAccountsPermission()
        verify(mockView, never()).requestGalleryAndAccountsPermissions()
        verify(mockView, never()).requestAccountsPermissionForGallery()
    }

    @Test
    fun onPhotoSelected_WhenGdriveConfigSelectedAndGetFolderIdSuccessAndSubDirIsChecked() {
        val files = listOf("file_path")
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        val folderId = "folderId"

        val savedConfig = GdriveConfig("alias", "path", subDirChecked = false)
        val expectedSavedConfig = savedConfig.also { it.folderId = folderId }

        val getFolderIdSingle = Single.just(folderId)

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.isSubDirChecked()).thenReturn(true)
        _when(mockModel.getGdriveFolderId("id2", subDir)).thenReturn(getFolderIdSingle)
        _when(mockModel.getConfig(configs[selectedItemPos])).thenReturn(savedConfig)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onFilesSelected(files)

        verify(mockView, times(1)).hideProgress()
        verify(mockModel, times(1)).sendFile(files, configs[selectedItemPos].id, subDir)
        verify(mockModel, times(1)).saveConfig(expectedSavedConfig)
    }

    @Test
    fun onPhotoSelected_WhenGdriveConfigSelectedAndGetFolderIdSuccessAndSubDirNotChecked() {
        val files = listOf("file_path")
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        val folderId = "folderId"

        val savedConfig = GdriveConfig("alias", "path", subDirChecked = false)
        val expectedSavedConfig = savedConfig.also { it.folderId = folderId }

        val getFolderIdSingle = Single.just(folderId)

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.isSubDirChecked()).thenReturn(false)
        _when(mockModel.getGdriveFolderId("id2", subDir)).thenReturn(getFolderIdSingle)
        _when(mockModel.getConfig(configs[selectedItemPos])).thenReturn(savedConfig)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onFilesSelected(files)

        verify(mockView, times(1)).hideProgress()
        verify(mockModel, times(1)).sendFile(files, configs[selectedItemPos].id, "")
        verify(mockModel, times(1)).saveConfig(expectedSavedConfig)
    }

    @Test
    fun onPhotoSelected_WhenGdriveConfigSelectedAndGetFolderIdThrowUserRecoverableAuthIOException() {
        val files = listOf("file_path")
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        val folderId = "folderId"

        val savedConfig = GdriveConfig("alias", "path", subDirChecked = false)

        val exception = UserRecoverableAuthIOException(UserRecoverableAuthException("", Intent()))
        val getFolderIdSingle = Single.just(folderId).doOnSubscribe { throw exception }

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.isSubDirChecked()).thenReturn(false)
        _when(mockModel.getGdriveFolderId("id2", subDir)).thenReturn(getFolderIdSingle)
        _when(mockModel.getConfig(configs[selectedItemPos])).thenReturn(savedConfig)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onFilesSelected(files)

        verify(mockView, times(1)).hideProgress()
        verify(mockView, times(1)).recoverGoogleAuthToken(exception)
        verify(mockModel, never()).sendFile(anyList(), anyString(), anyString())
        verify(mockModel, never()).saveConfig(nullableAny())
    }

    @Test()
    fun onPhotoSelected_WhenGdriveConfigSelectedAndGetFolderIdThrowNotUserRecoverableAuthIOException() {
        val files = listOf("file_path")
        val subDir = "sub_dir"
        val selectedItemPos = 1

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        val folderId = "folderId"

        val savedConfig = GdriveConfig("alias", "path", subDirChecked = false)

        val exception = Exception()
        val getFolderIdSingle = Single.just(folderId).doOnSubscribe { throw exception }

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.isSubDirChecked()).thenReturn(false)
        _when(mockModel.getGdriveFolderId("id2", subDir)).thenReturn(getFolderIdSingle)
        _when(mockModel.getConfig(configs[selectedItemPos])).thenReturn(savedConfig)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onFilesSelected(files)

        verify(mockView, times(1)).hideProgress()
        verify(mockView, times(1)).showErrorPrepareGdriveToast()
        verify(mockView, never()).recoverGoogleAuthToken(nullableAny())
        verify(mockModel, never()).sendFile(anyList(), anyString(), anyString())
        verify(mockModel, never()).saveConfig(nullableAny())
    }

    @Test
    fun onPhotoSelected_WhenNotGdriveConfigSelectedAndSubDirIsChecked() {
        val files = listOf("file_path")
        val subDir = "sub_dir"
        val selectedItemPos = 0

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.isSubDirChecked()).thenReturn(true)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onFilesSelected(files)

        verify(mockModel, times(1)).sendFile(files, configs[selectedItemPos].id, subDir)
    }

    @Test
    fun onPhotoSelected_WhenNotGdriveConfigSelectedAndSubDirNotChecked() {
        val files = listOf("file_path")
        val subDir = "sub_dir"
        val selectedItemPos = 0

        val configs = listOf(
                ConfigUIModel("id1", TYPE_LAN, "alias", "desc"),
                ConfigUIModel("id2", TYPE_GDRIVE, "alias", "desc"))

        _when(mockModel.getAllConfigs()).thenReturn(configs)
        _when(mockView.getEnteredSubDir()).thenReturn(subDir)
        _when(mockView.isSubDirChecked()).thenReturn(false)

        presenter.onViewCreated(mockView)
        presenter.onViewResumed()
        presenter.onItemSelected(selectedItemPos)
        presenter.onFilesSelected(files)

        verify(mockModel, times(1)).sendFile(files, configs[selectedItemPos].id, "")
    }

    @Test
    fun onCameraExplanationDialogYesButtonClick() {
        presenter.onViewCreated(mockView)
        presenter.onCameraExplanationDialogYesButtonClick()
        verify(mockView, times(1)).hideExplanationDialog()
        verify(mockView, times(1)).requestCameraPermission()
    }

    @Test
    fun onGalleryExplanationDialogYesButtonClick() {
        presenter.onViewCreated(mockView)
        presenter.onGalleryExplanationDialogYesButtonClick()
        verify(mockView, times(1)).hideExplanationDialog()
        verify(mockView, times(1)).requestGalleryPermission()
    }

    @Test
    fun onAccountsExplanationDialogForCameraYesButtonClick() {
        presenter.onViewCreated(mockView)
        presenter.onAccountsExplanationDialogForCameraYesButtonClick()
        verify(mockView, times(1)).hideExplanationDialog()
        verify(mockView, times(1)).requestAccountsPermissionForCamera()
    }

    @Test
    fun onAccountsExplanationDialogForGalleryYesButtonClick() {
        presenter.onViewCreated(mockView)
        presenter.onAccountsExplanationDialogForGalleryYesButtonClick()
        verify(mockView, times(1)).hideExplanationDialog()
        verify(mockView, times(1)).requestAccountsPermissionForGallery()
    }

    @Test
    fun onCameraAndAccountExplanationDialogYesButtonClick() {
        presenter.onViewCreated(mockView)
        presenter.onCameraAndAccountExplanationDialogYesButtonClick()
        verify(mockView, times(1)).hideExplanationDialog()
        verify(mockView, times(1)).requestCameraAndAccountsPermissions()
    }

    @Test
    fun onGalleryAndAccountExplanationDialogYesButtonClick() {
        presenter.onViewCreated(mockView)
        presenter.onGalleryAndAccountExplanationDialogYesButtonClick()
        verify(mockView, times(1)).hideExplanationDialog()
        verify(mockView, times(1)).requestGalleryAndAccountsPermissions()
    }

    @Test
    fun onExplanationDialogNoButtonClick() {
        presenter.onViewCreated(mockView)
        presenter.onExplanationDialogNoButtonClick()
        verify(mockView, times(1)).hideExplanationDialog()
    }

    @Test
    fun onExplanationDialogSettingsClick() {
        presenter.onViewCreated(mockView)
        presenter.onExplanationDialogSettingsClick()
        verify(mockView, times(1)).hideExplanationDialog()
        verify(mockView, times(1)).startSettingsApp()
    }

    @Test
    fun onCameraPermissionGranted() {
        presenter.onViewCreated(mockView)
        presenter.onCameraPermissionGranted()
        verify(mockView, times(1)).startCameraApp()
    }

    @Test
    fun onCameraPermissionDenied() {
        presenter.onViewCreated(mockView)
        presenter.onCameraPermissionDenied()
        verify(mockView, times(1)).showExplanationDialogForCamera()
    }

    @Test
    fun onCameraPermissionDeniedNeverAsk() {
        presenter.onViewCreated(mockView)
        presenter.onCameraPermissionDeniedNeverAsk()
        verify(mockView, times(1)).showExplanationDialogForCameraNeverAsk()
    }

    @Test
    fun onGalleryPermissionGranted() {
        presenter.onViewCreated(mockView)
        presenter.onGalleryPermissionGranted()
        verify(mockView, times(1)).startGalleryApp()
    }

    @Test
    fun onGalleryPermissionDenied() {
        presenter.onViewCreated(mockView)
        presenter.onGalleryPermissionDenied()
        verify(mockView, times(1)).showExplanationDialogForGallery()
    }

    @Test
    fun onGalleryPermissionDeniedNeverAsk() {
        presenter.onViewCreated(mockView)
        presenter.onGalleryPermissionDeniedNeverAsk()
        verify(mockView, times(1)).showExplanationDialogForGalleryNeverAsk()
    }

    @Test
    fun onAccountsPermissionDeniedForCamera() {
        presenter.onViewCreated(mockView)
        presenter.onAccountsPermissionDeniedForCamera()
        verify(mockView, times(1)).showExplanationDialogAccountsForCamera()
    }

    @Test
    fun onAccountsPermissionDeniedForGallery() {
        presenter.onViewCreated(mockView)
        presenter.onAccountsPermissionDeniedForGallery()
        verify(mockView, times(1)).showExplanationDialogAccountsForGallery()
    }

    @Test
    fun onAccountsPermissionDeniedNeverAsk() {
        presenter.onViewCreated(mockView)
        presenter.onAccountsPermissionDeniedNeverAsk()
        verify(mockView, times(1)).showExplanationDialogForAccountsNeverAsk()
    }

    @Test
    fun onCameraAndAccountsPermissionsDenied() {
        presenter.onViewCreated(mockView)
        presenter.onCameraAndAccountsPermissionsDenied()
        verify(mockView, times(1)).showExplanationDialogForCameraAndAccounts()
    }

    @Test
    fun onCameraAndAccountsPermissionsDeniedNeverAsk() {
        presenter.onViewCreated(mockView)
        presenter.onCameraAndAccountsPermissionsDeniedNeverAsk()
        verify(mockView, times(1)).showExplanationDialogForCameraAndAccountsNeverAsk()
    }

    @Test
    fun onGalleryAndAccountsPermissionsDenied() {
        presenter.onViewCreated(mockView)
        presenter.onGalleryAndAccountsPermissionsDenied()
        verify(mockView, times(1)).showExplanationDialogForGalleryAndAccounts()
    }

    @Test
    fun onGalleryAndAccountsPermissionsDeniedNeverAsk() {
        presenter.onViewCreated(mockView)
        presenter.onGalleryAndAccountsPermissionsDeniedNeverAsk()
        verify(mockView, times(1)).showExplanationDialogForGalleryAndAccountsNeverAsk()
    }

    @Test
    fun onCloseAdButtonClick() {
        presenter.onViewCreated(mockView)
        presenter.onCloseAdButtonClick()
        verify(mockModel, times(1)).startAdDisablingFlow(mockView)
    }
}