package ru.sevoro.photosaver.ui.screen.main

import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.Purchase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.config.*
import ru.sevoro.photosaver.data.sender.Sender
import ru.sevoro.photosaver.data.sender.SenderLazyFactory
import ru.sevoro.photosaver.helper.*
import org.mockito.Mockito.`when` as _when


/**
 * @author Sergey Vorobyev
 */
@Suppress("FunctionName")
class MainModelImplTest {

    private val mockLocalStorage = Mockito.mock(LocalStorage::class.java)
    private val mockGdriveHelper = Mockito.mock(GdriveHelper::class.java)
    private val mockLanSender = Mockito.mock(Sender::class.java)
    private val mockFtpSender = Mockito.mock(Sender::class.java)
    private val mockHttpSender = Mockito.mock(Sender::class.java)
    private val mockGdriveSender = Mockito.mock(Sender::class.java)
    private val mockSenderLazyFactory = Mockito.mock(SenderLazyFactory::class.java)
    private val mockBillingHelper = Mockito.mock(BillingHelper::class.java)
    private val mockRemoteConfigHelper = Mockito.mock(RemoteConfigHelper::class.java)

    private val model = MainModelImpl(
            mockLocalStorage,
            mockGdriveHelper,
            mockRemoteConfigHelper,
            mockBillingHelper,
            mockSenderLazyFactory
    )

    @Before
    fun setUp() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }

        _when(mockSenderLazyFactory.getSender(TYPE_LAN)).thenReturn(mockLanSender)
        _when(mockSenderLazyFactory.getSender(TYPE_HTTP)).thenReturn(mockHttpSender)
        _when(mockSenderLazyFactory.getSender(TYPE_FTP)).thenReturn(mockFtpSender)
        _when(mockSenderLazyFactory.getSender(TYPE_GDRIVE)).thenReturn(mockGdriveSender)
    }

    @Test
    fun getAllConfigs() {
        val lanConfig = LanConfig("lanConfigName", "lanConfigPath")
        lanConfig.id = "lanConfigId"

        val expectedResult = ConfigUIModel(lanConfig.id, TYPE_LAN, lanConfig.alias, lanConfig.path,
                false)

        _when(mockLocalStorage.getAllConfigs()).thenReturn(listOf(lanConfig))

        val result = model.getAllConfigs()

        assertEquals(listOf(expectedResult), result)
    }

    @Test
    fun getConfig() {
        val configId = "id"
        val configType = TYPE_GDRIVE
        val configUIModel = ConfigUIModel(configId, configType)
        val expectedConfig = GdriveConfig("alias", "path", account = "account@gmail.com",
                folderId = "folderId")
        expectedConfig.id = configId

        _when(mockLocalStorage.getConfig<GdriveConfig>(configId)).thenReturn(expectedConfig)

        val result = model.getConfig(configUIModel)

        verify(mockLocalStorage, only()).getConfig<GdriveConfig>(configId)
        assertEquals(expectedConfig, result)
    }

    @Test
    fun saveConfig() {
        val config = GdriveConfig("alias", "path", account = "account@gmail.com",
                folderId = "folderId")
        model.saveConfig(config)
        verify(mockLocalStorage, only()).saveConfig(config)
    }

    @Test
    fun deleteConfig() {
        val configId = "id"
        model.deleteConfig(configId)
        verify(mockLocalStorage, only()).deleteConfig(configId)
    }

    @Test
    fun saveSelectedConfigId() {
        val configId = "id"
        model.saveSelectedConfigId(configId)
        verify(mockLocalStorage, only()).saveSelectedConfigId(configId)
    }

    @Test
    fun getSelectedConfigId() {
        val configId = "id"
        _when(mockLocalStorage.getSelectedConfigId()).thenReturn(configId)
        val result = model.getSelectedConfigId()
        assertEquals(configId, result)
    }

    @Test
    fun sendFile_TypeLan() {
        val config: Config = LanConfig("alias", "path")
        val files = listOf("file_path")
        val configId = "configId"
        val subDir = "subDir"

        _when(mockLocalStorage.getConfig<Config>(configId)).thenReturn(config)

        model.sendFile(files, configId, subDir)

        verify(mockLanSender, times(1)).send(files, config, subDir)
    }

    @Test
    fun sendFile_TypeFtp() {
        val config: Config = FtpConfig("alias", "path")
        val files = listOf("file_path")
        val configId = "configId"
        val subDir = "subDir"

        _when(mockLocalStorage.getConfig<Config>(configId)).thenReturn(config)

        model.sendFile(files, configId, subDir)

        verify(mockFtpSender, times(1)).send(files, config, subDir)
    }

    @Test
    fun sendFile_TypeHttp() {
        val config: Config = HttpConfig("alias", "path")
        val files = listOf("file_path")
        val configId = "configId"
        val subDir = "subDir"

        _when(mockLocalStorage.getConfig<Config>(configId)).thenReturn(config)

        model.sendFile(files, configId, subDir)

        verify(mockHttpSender, times(1)).send(files, config, subDir)
    }

    @Test
    fun sendFile_TypeGdrive() {
        val config: Config = GdriveConfig("alias", "path")
        val files = listOf("file_path")
        val configId = "configId"
        val subDir = "subDir"

        _when(mockLocalStorage.getConfig<Config>(configId)).thenReturn(config)

        model.sendFile(files, configId, subDir)

        verify(mockGdriveSender, times(1)).send(files, config, subDir)
    }

    @Test
    fun getGdriveFolderId_WhenSubDirIsNotEmpty() {
        val configId = "id"
        val subDir = "subDir"
        val folderPath = "folder/test"
        val fullPath = "$folderPath/$subDir"
        val account = "account@gmail.com"
        val config = GdriveConfig("alias", folderPath, account = account)
        config.id = configId
        val expectedFolderId = "folderId"

        _when(mockLocalStorage.getConfig<GdriveConfig>(configId)).thenReturn(config)
        _when(mockGdriveHelper.getFolderIdFromPath(fullPath, account))
                .thenReturn(Single.just(expectedFolderId))

        val testObserver = model.getGdriveFolderId(configId, subDir).test()

        testObserver.assertNoErrors()
        assertEquals(1, testObserver.valueCount())
        assertEquals(expectedFolderId, testObserver.values()[0])

        verify(mockLocalStorage, times(1)).getConfig<GdriveConfig>(configId)
        verify(mockGdriveHelper, times(1)).getFolderIdFromPath(fullPath, account)
    }

    @Test
    fun startAdDisablingFlow() {
        val mockRemoteConfig = mock(FirebaseRemoteConfig::class.java)
        val activityInstance = "activity_instance"
        val sku = "sku"

        _when(mockRemoteConfigHelper.getCachedConfig()).thenReturn(mockRemoteConfig)
        _when(mockRemoteConfig.getString(REMOTE_CONFIG_AD_DISABLING_PURCHASE_SKU))
                .thenReturn(sku)

        model.startAdDisablingFlow(activityInstance)

        verify(mockBillingHelper, only()).launchInAppBillingFlow(activityInstance, sku)
    }

    @Test
    fun getAdDisablingUpdateObs_WhenAdDisabledByRemoteConfig() {
        val mockRemoteConfig = mock(FirebaseRemoteConfig::class.java)

        _when(mockRemoteConfigHelper.getCachedConfig()).thenReturn(mockRemoteConfig)
        _when(mockRemoteConfig.getBoolean(REMOTE_CONFIG_AD_DISABLED)).thenReturn(true)
        _when(mockRemoteConfigHelper.getUpdatedConfig()).thenReturn(Single.just(mockRemoteConfig))

        val testObserver = model.getAdDisablingUpdateObs().test()

        //assertEquals(1, testObserver.valueCount())
        assertTrue(testObserver.values()[0])
    }

    @Test
    fun getAdDisablingUpdateObs_WhenAdNotDisabledByRemoteConfigAndPurchased() {
        val mockRemoteConfig = mock(FirebaseRemoteConfig::class.java)
        val mockPurchase = mock(Purchase::class.java)
        val sku = "sku"
        val purchasesUpdateResult = PurchasesUpdateResult(BillingClient.BillingResponse.OK,
                mutableListOf(mockPurchase))

        _when(mockRemoteConfigHelper.getUpdatedConfig()).thenReturn(Single.just(mockRemoteConfig))
        _when(mockRemoteConfigHelper.getCachedConfig()).thenReturn(mockRemoteConfig)
        _when(mockRemoteConfig.getBoolean(REMOTE_CONFIG_AD_DISABLED)).thenReturn(false)
        _when(mockRemoteConfig.getString(REMOTE_CONFIG_AD_DISABLING_PURCHASE_SKU))
                .thenReturn(sku)
        _when(mockBillingHelper.getUpdatePurchasesSubject())
                .thenReturn(Observable.just(purchasesUpdateResult))
        _when(mockPurchase.sku).thenReturn(sku)

        val testObserver = model.getAdDisablingUpdateObs().test()

        assertTrue(testObserver.values()[0])
    }

    @Test
    fun getAdDisablingUpdateObs_WhenAdNotDisabledByRemoteConfigAndPurchasedEmpty() {
        val mockRemoteConfig = mock(FirebaseRemoteConfig::class.java)
        val purchasesUpdateResult = PurchasesUpdateResult(BillingClient.BillingResponse.OK,
                mutableListOf())

        _when(mockRemoteConfigHelper.getUpdatedConfig()).thenReturn(Single.just(mockRemoteConfig))
        _when(mockRemoteConfigHelper.getCachedConfig()).thenReturn(mockRemoteConfig)
        _when(mockRemoteConfig.getBoolean(REMOTE_CONFIG_AD_DISABLED)).thenReturn(false)
        _when(mockBillingHelper.getUpdatePurchasesSubject())
                .thenReturn(Observable.just(purchasesUpdateResult))

        val testObserver = model.getAdDisablingUpdateObs().test()

        assertFalse(testObserver.values()[0])
    }
}