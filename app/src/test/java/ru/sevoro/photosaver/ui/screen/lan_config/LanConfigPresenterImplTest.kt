package ru.sevoro.photosaver.ui.screen.lan_config

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import ru.sevoro.photosaver.data.config.Config
import ru.sevoro.photosaver.data.config.LanConfig
import ru.sevoro.photosaver.nullableAny
import ru.sevoro.photosaver.ui.screen.base.ConfigModel
import org.mockito.Mockito.`when` as _when

/**
 * @author Sergey Vorobyev
 */
@Suppress("FunctionName")
class LanConfigPresenterImplTest {

    private val mockView = Mockito.mock(LanConfigView::class.java)
    private val mockModel = Mockito.mock(ConfigModel::class.java)
    private val presenter = LanConfigPresenterImpl(mockModel)

    @Before
    fun setUp() {
        presenter.onViewCreated(mockView)
    }

    @Test
    fun onViewCreatedInEditMode() {
        val id = "id"
        val alias = "alias"
        val path = "path"
        val domain = "domain"
        val username = "username"
        val password = "password"

        _when(mockModel.getConfig<Config>(id)).thenReturn(LanConfig(alias, path, domain = domain,
                username = username, password = password))
        presenter.onViewCreatedInEditMode(mockView, id)

        verify(mockView, times(1)).setInputAlias(alias)
        verify(mockView, times(1)).setInputPath(path)
        verify(mockView, times(1)).setInputDomain(domain)
        verify(mockView, times(1)).setInputUsername(username)
        verify(mockView, times(1)).setInputPassword(password)
    }

    @Test
    fun onAliasTextChanged_WhenAliasIsValid() {
        val alias = "alias"
        presenter.onAliasTextChanged(alias)
        verify(mockView, times(1)).enableAliasInputError(false)
    }

    @Test
    fun onAliasTextChanged_WhenAliasIsNotValid() {
        val alias = "alias is not valid because one contains more then 20 characters"
        presenter.onAliasTextChanged(alias)
        verify(mockView, times(1)).enableAliasInputError(true)
    }

    @Test
    fun onPathTextChanged_WhenTextIsValid() {
        presenter.onPathTextChanged("pc_name/shared_folder")
        verify(mockView, times(1)).enablePathInputError(false)
    }

    @Test
    fun onPathTextChanged_WhenTextIsNotValid() {
        presenter.onPathTextChanged("not_valid_path")
        verify(mockView, times(1)).enablePathInputError(true)
    }

    @Test
    fun onDomainTextChanged_WhenTextIsValid() {
        val domain = "domain"
        presenter.onDomainTextChanged(domain)
        verify(mockView, times(1)).enableDomainInputError(false)
    }

    @Test
    fun onUsernameTextChanged_WhenTextIsNotValid() {
        val domain = "domain is not valid because one more then 50 characters"
        presenter.onDomainTextChanged(domain)
        verify(mockView, times(1)).enableDomainInputError(true)
    }

    @Test
    fun onDoneActionClick_WhenNotAllInputsIsValid() {
        val enteredAlias = "valid alias"
        val enteredPath = "not valid path"
        val enteredDomain = "domain"

        _when(mockView.getEnteredAlias()).thenReturn(enteredAlias)
        _when(mockView.getEnteredPath()).thenReturn(enteredPath)
        _when(mockView.getEnteredDomain()).thenReturn(enteredDomain)

        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(false)
        verify(mockView, times(1)).enablePathInputError(true)
        verify(mockView, times(1)).enableDomainInputError(false)
        verify(mockView, never()).navigateToMainScreen()
        verify(mockModel, never()).saveConfig(nullableAny())
    }

    @Test
    fun onDoneActionClick_WhenAllInputsIsValidAndNotEditMode() {
        val alias = "alias"
        val path = "pc_name/shared_folder"
        val domain = "domain"
        val username = "username"
        val password = "pass"
        val expectedConfig = LanConfig(alias, path, domain = domain, username = username, password = password)

        _when(mockView.getEnteredAlias()).thenReturn(alias)
        _when(mockView.getEnteredPath()).thenReturn(path)
        _when(mockView.getEnteredDomain()).thenReturn(domain)
        _when(mockView.getEnteredUsername()).thenReturn(username)
        _when(mockView.getEnteredPassword()).thenReturn(password)

        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(false)
        verify(mockView, times(1)).enablePathInputError(false)
        verify(mockView, times(1)).enableDomainInputError(false)
        verify(mockModel, times(1)).saveConfig(expectedConfig)
        verify(mockView, times(1)).navigateToMainScreen()
    }

    @Test
    fun onDoneActionClick_WhenAllInputsIsValidAndEditMode() {
        val configId = "id"
        val alias = "alias"
        val path = "pc_name/shared_folder"
        val domain = "domain"
        val username = "username"
        val password = "pass"
        val expectedConfig = LanConfig(alias, path, domain = domain, username = username, password = password)
        expectedConfig.id = configId

        _when(mockModel.getConfig<Config>(configId)).thenReturn(expectedConfig)
        _when(mockView.getEnteredAlias()).thenReturn(alias)
        _when(mockView.getEnteredPath()).thenReturn(path)
        _when(mockView.getEnteredDomain()).thenReturn(domain)
        _when(mockView.getEnteredUsername()).thenReturn(username)
        _when(mockView.getEnteredPassword()).thenReturn(password)

        presenter.onViewCreatedInEditMode(mockView, configId)
        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(false)
        verify(mockView, times(1)).enablePathInputError(false)
        verify(mockView, times(1)).enableDomainInputError(false)
        verify(mockModel, times(1)).saveConfig(expectedConfig)
        verify(mockView, times(1)).navigateToMainScreen()
    }
}