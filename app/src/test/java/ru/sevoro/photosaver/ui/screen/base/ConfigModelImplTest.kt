package ru.sevoro.photosaver.ui.screen.base

import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import ru.sevoro.photosaver.data.LocalStorage
import ru.sevoro.photosaver.data.config.Config
import ru.sevoro.photosaver.data.config.FtpConfig

/**
 * @author Sergey Vorobyev
 */
class ConfigModelImplTest {

    private val mockLocalStorage = Mockito.mock(LocalStorage::class.java)
    private val model = ConfigModelImpl(mockLocalStorage)

    @Test
    fun saveConfig() {
        val config = FtpConfig("alias", "path")
        model.saveConfig(config)
        verify(mockLocalStorage, only()).saveConfig(config)
    }

    @Test
    fun getConfig() {
        val configId = "id"
        val expectedConfig = FtpConfig("alias", "path", username = "username", password = "password")
        expectedConfig.id = configId

        `when`(mockLocalStorage.getConfig<Config>(configId)).thenReturn(expectedConfig)

        val result: FtpConfig = model.getConfig(configId)

        assertEquals(expectedConfig, result)
    }
}