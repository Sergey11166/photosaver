@file:Suppress("FunctionName")

package ru.sevoro.photosaver.ui.screen.gdrive_config

import com.google.android.gms.common.ConnectionResult.SERVICE_INVALID
import com.google.android.gms.common.ConnectionResult.SUCCESS
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import ru.sevoro.photosaver.data.config.Config
import ru.sevoro.photosaver.data.config.GdriveConfig
import ru.sevoro.photosaver.nullableAny
import ru.sevoro.photosaver.ui.screen.base.ConfigModel
import org.mockito.Mockito.`when` as _when

/**
 * @author Sergey Vorobyev
 */
class GdriveConfigPresenterImplTest {

    private val mockModel = Mockito.mock(ConfigModel::class.java)
    private val mockView = Mockito.mock(GdriveConfigView::class.java)
    private val presenter = GdriveConfigPresenterImpl(mockModel)

    @Before
    fun setUp() {
        presenter.onViewCreated(mockView)
    }

    @Test
    fun onViewCreatedInEditMode() {
        val id = "id"
        val alias = "alias"
        val path = "path"
        val account = "account"

        _when(mockModel.getConfig<Config>(id)).thenReturn(GdriveConfig(alias, path, account = account))
        presenter.onViewCreatedInEditMode(mockView, id)

        verify(mockView, Mockito.times(1)).setInputAlias(alias)
        verify(mockView, Mockito.times(1)).setInputPath(path)
        verify(mockView, Mockito.times(1)).setSelectedAccount(account)
    }

    @Test
    fun onAccountSelected() {
        val account = "account@gmail.com"
        presenter.onAccountSelected(account)
        verify(mockView, only()).setSelectedAccount(account)
    }

    @Test
    fun onEditAccountClick_WhenPlayServicesAvailable() {
        _when(mockView.getPlayServicesAvailabilityStatusCode()).thenReturn(SUCCESS)

        presenter.onEditAccountClick()

        verify(mockView, times(1)).startChoosingAccount()
        verify(mockView, never()).showResolvePlayServicesErrorDialog(anyInt())
        verify(mockView, never()).showPlayServicesNotAvailableToast()
    }

    @Test
    fun onEditAccountClick_WhenPlayServicesNotAvailableButResolvable() {
        _when(mockView.getPlayServicesAvailabilityStatusCode()).thenReturn(SERVICE_INVALID)
        _when(mockView.isUserResolvablePlayServicesError(SERVICE_INVALID)).thenReturn(true)

        presenter.onEditAccountClick()

        verify(mockView, times(1)).showResolvePlayServicesErrorDialog(SERVICE_INVALID)
        verify(mockView, never()).startChoosingAccount()
        verify(mockView, never()).showPlayServicesNotAvailableToast()
    }

    @Test
    fun onEditAccountClick_WhenPlayServicesNotAvailableAndNotResolvable() {
        _when(mockView.getPlayServicesAvailabilityStatusCode()).thenReturn(SERVICE_INVALID)
        _when(mockView.isUserResolvablePlayServicesError(SERVICE_INVALID)).thenReturn(false)

        presenter.onEditAccountClick()

        verify(mockView, times(1)).showPlayServicesNotAvailableToast()
        verify(mockView, never()).showResolvePlayServicesErrorDialog(anyInt())
        verify(mockView, never()).startChoosingAccount()
    }

    @Test
    fun onDoneActionClick_WhenNotAllInputsIsValid() {
        val enteredAlias = "" // not valid alias
        val enteredPath = "valid path"
        val selectedAccount = "account@gmail.com"

        _when(mockView.getEnteredAlias()).thenReturn(enteredAlias)
        _when(mockView.getEnteredPath()).thenReturn(enteredPath)
        _when(mockView.getSelectedAccount()).thenReturn(selectedAccount)

        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(true)
        verify(mockView, times(1)).enablePathInputError(false)
        verify(mockView, never()).showSelectAccountToast()
        verify(mockView, never()).navigateToMainScreen()
        verify(mockModel, never()).saveConfig(nullableAny())
    }

    @Test
    fun onDoneActionClick_WhenAllInputsIsValidAndEditMode() {
        val enteredAlias = "valid alias"
        val enteredPath = "valid path"
        val selectedAccount = "account@gmail.com"
        val expectedConfig = GdriveConfig(enteredAlias, enteredPath, subDirChecked = false,
                subDirText = "", account = selectedAccount)

        _when(mockView.getEnteredAlias()).thenReturn(enteredAlias)
        _when(mockView.getEnteredPath()).thenReturn(enteredPath)
        _when(mockView.getSelectedAccount()).thenReturn(selectedAccount)

        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(false)
        verify(mockView, times(1)).enablePathInputError(false)
        verify(mockView, times(1)).navigateToMainScreen()
        verify(mockModel, times(1)).saveConfig(expectedConfig)
        verify(mockView, never()).showSelectAccountToast()
    }

    @Test
    fun onDoneActionClick_WhenAllInputsIsValidAndNotEditMode() {
        val enteredAlias = "valid alias"
        val enteredPath = "valid path"
        val selectedAccount = "account@gmail.com"
        val existConfigId = "existConfigId"
        val expectedConfig = GdriveConfig(enteredAlias, enteredPath, subDirChecked = true,
                subDirText = "subDir", account = selectedAccount)
        expectedConfig.id = existConfigId

        _when(mockModel.getConfig<Config>(existConfigId)).thenReturn(expectedConfig)
        _when(mockView.getEnteredAlias()).thenReturn(enteredAlias)
        _when(mockView.getEnteredPath()).thenReturn(enteredPath)
        _when(mockView.getSelectedAccount()).thenReturn(selectedAccount)

        presenter.onViewCreatedInEditMode(mockView, existConfigId)
        presenter.onDoneActionClick()

        verify(mockView, times(1)).enableAliasInputError(false)
        verify(mockView, times(1)).enablePathInputError(false)
        verify(mockView, times(1)).navigateToMainScreen()
        verify(mockModel, times(1)).saveConfig(expectedConfig)
        verify(mockView, never()).showSelectAccountToast()
    }
}