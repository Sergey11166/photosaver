package ru.sevoro.photosaver.data

import android.content.SharedPreferences
import com.google.gson.Gson
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import ru.sevoro.photosaver.data.config.Config
import ru.sevoro.photosaver.data.config.FtpConfig
import ru.sevoro.photosaver.data.config.LanConfig
import ru.sevoro.photosaver.helper.KEY_ALL_CONFIGS
import ru.sevoro.photosaver.helper.KEY_SELECTED_CONFIG_ID
import ru.sevoro.photosaver.helper.KEY_UNIQUE_INT
import ru.sevoro.photosaver.helper.PreferencesHelper
import org.mockito.Mockito.`when` as _when


/**
 * @author Sergey Vorobyev
 */
@Suppress("FunctionName")
class PreferencesHelperTest {

    private val gson = Gson()
    private val mockPrefs = Mockito.mock(SharedPreferences::class.java)
    private val mockEditor = Mockito.mock(SharedPreferences.Editor::class.java)
    private val prefHelper = PreferencesHelper(mockPrefs, gson)

    @Before
    fun setUp() {
        _when(mockPrefs.edit()).thenReturn(mockEditor)
    }

    @Test
    fun saveConfig_WhenStorageIsEmpty() {
        val newItem = LanConfig("Lan Shared Folder", "192.168.0.1/shared folder")
        newItem.id = "id"
        val expectedJson = gson.toJson(listOf(newItem))

        _when(mockPrefs.getString(KEY_ALL_CONFIGS, "")).thenReturn("")

        prefHelper.saveConfig(newItem)
        verify(mockEditor, times(1)).putString(KEY_ALL_CONFIGS, expectedJson)
    }

    @Test
    fun saveConfig_WhenStorageIsNonEmpty() {
        val existItem = LanConfig("Lan Shared Folder", "192.168.0.1/shared folder")
        existItem.id = "id1"

        val beforeList = listOf<Config>(existItem)
        val beforeJson = gson.toJson(beforeList)

        val newItem = LanConfig("Lan Shared Folder", "192.168.0.1/shared folder")
        newItem.id = "id2"

        val afterList = mutableListOf<Config>()
        afterList.addAll(beforeList)
        afterList.add(newItem)
        val afterJson = gson.toJson(afterList)

        _when(mockPrefs.getString(KEY_ALL_CONFIGS, "")).thenReturn(beforeJson)

        prefHelper.saveConfig(newItem)

        verify(mockEditor, times(1)).putString(KEY_ALL_CONFIGS, afterJson)
    }

    @Test(expected = NoSuchElementException::class)
    fun deleteConfig_WhenStorageIsEmpty() {
        _when(mockPrefs.getString(KEY_ALL_CONFIGS, "")).thenReturn("")
        prefHelper.deleteConfig("id")
    }

    @Test
    fun deleteConfig_WhenStorageIsNonEmpty() {
        val existItem = LanConfig("Lan Shared Folder", "192.168.0.1/shared folder")
        existItem.id = "id"

        val beforeJson = gson.toJson(listOf<Config>(existItem))
        val afterJson = gson.toJson(emptyList<Config>())

        _when(mockPrefs.getString(KEY_ALL_CONFIGS, "")).thenReturn(beforeJson)

        prefHelper.deleteConfig("id")

        verify(mockEditor, times(1)).putString(KEY_ALL_CONFIGS, afterJson)
    }

    @Test(expected = NoSuchElementException::class)
    fun getConfig_WhenConfigNotFound() {
        _when(mockPrefs.getString(KEY_ALL_CONFIGS, "")).thenReturn("")
        prefHelper.getConfig<LanConfig>("id")
    }

    @Test
    fun getConfig_WhenConfigFound() {
        val lanConfig = LanConfig("Lan Shared Folder", "192.168.0.1/shared folder")
        lanConfig.id = "id_lan"

        val ftpConfig = FtpConfig("Ftp remote Folder", "ftp://remoteftp.com/shared folder")
        lanConfig.id = "id_ftp"

        val existJson = gson.toJson(listOf(lanConfig, ftpConfig))

        _when(mockPrefs.getString(KEY_ALL_CONFIGS, "")).thenReturn(existJson)

        val foundConfig: Config = prefHelper.getConfig(lanConfig.id)
        assertTrue(foundConfig is LanConfig)
    }

    @Test
    fun getAllConfigs() {
        val lanConfig = LanConfig("Lan Shared Folder", "192.168.0.1/shared folder")
        lanConfig.id = "id_lan"

        val ftpConfig = FtpConfig("Ftp remote Folder", "ftp://remoteftp.com/shared folder")
        lanConfig.id = "id_ftp"

        val existJson = gson.toJson(listOf(lanConfig, ftpConfig))

        _when(mockPrefs.getString(KEY_ALL_CONFIGS, "")).thenReturn(existJson)

        val allItems = prefHelper.getAllConfigs()

        assertTrue(allItems.size == 2)
        assertTrue(allItems[0] is LanConfig)
        assertTrue(allItems[1] is FtpConfig)
    }

    @Test
    fun saveSelectedConfigId() {
        val configId = "config_id"
        prefHelper.saveSelectedConfigId(configId)
        verify(mockEditor, times(1)).putString(KEY_SELECTED_CONFIG_ID, configId)
    }

    @Test
    fun getSelectedConfigId() {
        val expectedValue = "config_id"
        _when(mockPrefs.getString(KEY_SELECTED_CONFIG_ID, "")).thenReturn(expectedValue)

        val result = prefHelper.getSelectedConfigId()

        assertEquals(expectedValue, result)
    }

    @Test
    fun getUniqueInt() {
        val prevValue = 0
        _when(mockPrefs.getInt(KEY_UNIQUE_INT, 0)).thenReturn(prevValue)
        val expectedValue = prevValue + 1

        val result = prefHelper.getUniqueInt()

        assertEquals(expectedValue, result)
    }
}
