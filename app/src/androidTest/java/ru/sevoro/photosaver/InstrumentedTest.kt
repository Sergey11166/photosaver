package ru.sevoro.photosaver

import android.os.Build
import android.os.SystemClock
import android.support.test.espresso.Espresso.*
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeTextIntoFocusedView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import ru.sevoro.photosaver.ui.screen.main.ConfigViewHolder
import ru.sevoro.photosaver.ui.screen.main.MainActivity

/**
 * @author Sergey Vorobyev
 */
private const val LAN_ALIAS = "lanAlias"
private const val LAN_PATH = "192.168.0.7/photos"
private const val FTP_ALIAS = "ftpAlias"
private const val FTP_PATH = "ftp://myftpserver.com/photos"
private const val HTTP_ALIAS = "httpAlias"
private const val HTTP_PATH = "http://myhttpserver.com/photos"
private const val FORM_PARAM = "formParam"
private const val HEADER = "header"
private const val USERNAME = "username"
private const val PASSWORD = "password"
private const val DOMAIN = "domain"

class InstrumentedTest {

    @get:Rule
    private var activityTestRule: ActivityTestRule<MainActivity> = ActivityTestRule(
            MainActivity::class.java, true, false)

    @Before
    fun setUp() {
        App.appComponent.localStorage().deleteAllConfigs()
        activityTestRule.launchActivity(null)
    }

    @Test
    fun test() {
        checkNoConfigsScreen()
        checkTypeChoosingScreen()
        createLanConfig()
        createFtpConfig()
        createHttpConfig()
        checkCreateGdriveConfigScreen()
        checkMainScreen()
        editLanConfig()
        editFtpConfig()
        editHttpConfig()
        checkSettingsScreen()
    }

    private fun checkNoConfigsScreen() {
        // toolbar
        onView(withText(R.string.app_name)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_item_settings)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_item_add)).check(matches(isDisplayed()))

        // content
        onView(withId(R.id.btn_add_config)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_no_config)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_privacy_policy)).check(matches(isDisplayed()))
    }

    private fun checkTypeChoosingScreen() {
        onView(withId(R.id.btn_add_config)).perform(click())

        // toolbar
        onView(withText(R.string.type_choosing_main_title)).check(matches(isDisplayed()))

        // LAN
        onView(withText(R.string.type_choosing_lan_title)).check(matches(isDisplayed()))
        onView(withText(R.string.type_choosing_lan_desc)).check(matches(isDisplayed()))

        // FTP
        onView(withText(R.string.type_choosing_ftp_title)).check(matches(isDisplayed()))
        onView(withText(R.string.type_choosing_ftp_desc)).check(matches(isDisplayed()))

        // HTTP
        onView(withText(R.string.type_choosing_web_title)).check(matches(isDisplayed()))
        onView(withText(R.string.type_choosing_web_desc)).check(matches(isDisplayed()))

        // GDRIVE
        onView(withText(R.string.type_choosing_gdrive_title)).check(matches(isDisplayed()))
        onView(withText(R.string.type_choosing_gdrive_desc)).check(matches(isDisplayed()))
    }

    private fun createLanConfig() {
        onView(withId(R.id.item_lan)).perform(click())
        closeSoftKeyboard()

        // toolbar
        onView(withText(R.string.lan_config_main_title)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_item_done)).check(matches(isDisplayed()))

        // isDisplayed
        onView(withId(R.id.input_alias)).check(matches(isDisplayed()))
        onView(withId(R.id.input_path)).check(matches(isDisplayed()))
        onView(withId(R.id.input_domain)).check(matches(isDisplayed()))
        onView(withId(R.id.input_username)).check(matches(isDisplayed()))
        onView(withId(R.id.input_password)).check(matches(isDisplayed()))

        // input alias
        onView(withId(R.id.et_alias)).perform(click())
        onView(withId(R.id.et_alias)).perform(typeTextIntoFocusedView(LAN_ALIAS))
        closeSoftKeyboard()

        // input path
        onView(withId(R.id.et_path)).perform(click())
        onView(withId(R.id.et_path))
                .perform(typeTextIntoFocusedView(LAN_PATH))
        closeSoftKeyboard()

        // input domain
        onView(withId(R.id.et_domain)).perform(click())
        onView(withId(R.id.et_domain)).perform(typeTextIntoFocusedView(DOMAIN))
        closeSoftKeyboard()

        // input username
        onView(withId(R.id.et_username)).perform(click())
        onView(withId(R.id.et_username)).perform(typeTextIntoFocusedView(USERNAME))
        closeSoftKeyboard()

        // input password
        onView(withId(R.id.et_password)).perform(click())
        onView(withId(R.id.et_password)).perform(typeTextIntoFocusedView(PASSWORD))
        closeSoftKeyboard()

        // press done button
        onView(withId(R.id.menu_item_done)).perform(click())
    }

    private fun createFtpConfig() {
        onView(withId(R.id.menu_item_add)).perform(click())
        onView(withId(R.id.item_ftp)).perform(click())
        closeSoftKeyboard()

        // toolbar
        onView(withText(R.string.ftp_config_main_title)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_item_done)).check(matches(isDisplayed()))

        // isDisplayed
        onView(withId(R.id.input_alias)).check(matches(isDisplayed()))
        onView(withId(R.id.input_path)).check(matches(isDisplayed()))
        onView(withId(R.id.input_username)).check(matches(isDisplayed()))
        onView(withId(R.id.input_password)).check(matches(isDisplayed()))

        // input alias
        onView(withId(R.id.et_alias)).perform(click())
        onView(withId(R.id.et_alias)).perform(typeTextIntoFocusedView(FTP_ALIAS))
        closeSoftKeyboard()

        // input path
        onView(withId(R.id.et_path)).perform(click())
        onView(withId(R.id.et_path))
                .perform(typeTextIntoFocusedView(FTP_PATH))
        closeSoftKeyboard()

        // input username
        onView(withId(R.id.et_username)).perform(click())
        onView(withId(R.id.et_username)).perform(typeTextIntoFocusedView(USERNAME))
        closeSoftKeyboard()

        // input password
        onView(withId(R.id.et_password)).perform(click())
        onView(withId(R.id.et_password)).perform(typeTextIntoFocusedView(PASSWORD))
        closeSoftKeyboard()

        // press done button
        onView(withId(R.id.menu_item_done)).perform(click())
    }

    private fun createHttpConfig() {
        onView(withId(R.id.menu_item_add)).perform(click())
        onView(withId(R.id.item_web)).perform(click())
        closeSoftKeyboard()

        // toolbar
        onView(withText(R.string.http_config_main_title)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_item_done)).check(matches(isDisplayed()))

        // isDisplayed
        onView(withId(R.id.input_alias)).check(matches(isDisplayed()))
        onView(withId(R.id.input_path)).check(matches(isDisplayed()))
        onView(withId(R.id.input_form_param)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_method_title)).check(matches(isDisplayed()))
        onView(withId(R.id.spinner_method)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_auth_title)).check(matches(isDisplayed()))
        onView(withId(R.id.spinner_auth)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_headers_title)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_headers)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_headers)).check(matches(hasDescendant(withText(""))))

        // input alias
        onView(withId(R.id.et_alias)).perform(click())
        onView(withId(R.id.et_alias)).perform(typeTextIntoFocusedView(HTTP_ALIAS))
        closeSoftKeyboard()

        // input path
        onView(withId(R.id.et_path)).perform(click())
        onView(withId(R.id.et_path))
                .perform(typeTextIntoFocusedView(HTTP_PATH))
        closeSoftKeyboard()

        // input form parameter
        onView(withId(R.id.et_form_param)).perform(click())
        onView(withId(R.id.et_form_param))
                .perform(typeTextIntoFocusedView(FORM_PARAM))
        closeSoftKeyboard()

        // choose http-method
        onView(withId(R.id.spinner_method)).perform(click())
        onView(withText("POST")).check(matches(isDisplayed()))
        onView(withText("PUT")).check(matches(isDisplayed()))
        onView(withText("PATCH")).check(matches(isDisplayed()))
        onView(withText("OPTIONS")).check(matches(isDisplayed()))
        pressBack()

        // choose auth type
        onView(withId(R.id.spinner_auth)).perform(click())
        onView(withText("None")).check(matches(isDisplayed()))
        onView(withText("Basic")).check(matches(isDisplayed()))
        pressBack()

        // input header
        onView(withId(R.id.et_header)).perform(click())
        onView(withId(R.id.et_header))
                .perform(typeTextIntoFocusedView(HEADER))
        closeSoftKeyboard()
        SystemClock.sleep(300)

        // check that header added
        onView(withId(R.id.rv_headers)).check(matches(hasDescendant(withText(HEADER))))
        onView(withId(R.id.rv_headers)).check(matches(hasDescendant(withText(""))))
        onView(withId(R.id.rv_headers)).check(matches(hasDescendant(withId(R.id.iv_delete_header))))

        // press done button
        onView(withId(R.id.menu_item_done)).perform(click())
    }

    private fun checkCreateGdriveConfigScreen() {
        onView(withId(R.id.menu_item_add)).perform(click())
        onView(withId(R.id.item_gdrive)).perform(click())
        closeSoftKeyboard()

        // toolbar
        onView(withText(R.string.gdrive_config_main_title)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_item_done)).check(matches(isDisplayed()))

        // isDisplayed
        onView(withId(R.id.input_alias)).check(matches(isDisplayed()))
        onView(withId(R.id.input_path)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_account_title)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_account)).check(matches(isDisplayed()))
        onView(withId(R.id.iv_change_account)).check(matches(isDisplayed()))

        // input alias
        onView(withId(R.id.et_alias)).perform(click())
        onView(withId(R.id.et_alias)).perform(typeTextIntoFocusedView("gdriveAlias"))
        closeSoftKeyboard()

        // input path
        onView(withId(R.id.et_path)).perform(click())
        onView(withId(R.id.et_path))
                .perform(typeTextIntoFocusedView("photos"))
        closeSoftKeyboard()

        // press done button
        onView(withId(R.id.menu_item_done)).perform(click())

        // should show toast. go back
        pressBack()
        pressBack()
    }

    private fun checkMainScreen() {
        // toolbar
        onView(withText(R.string.app_name)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_item_add)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_item_settings)).check(matches(isDisplayed()))

        // lanConfig
        onView(withText(LAN_ALIAS)).check(matches(isDisplayed()))
        onView(withText(LAN_PATH)).check(matches(isDisplayed()))

        // ftpConfig
        onView(withText(FTP_ALIAS)).check(matches(isDisplayed()))
        onView(withText(FTP_PATH)).check(matches(isDisplayed()))

        // httpConfig
        onView(withText(HTTP_ALIAS)).check(matches(isDisplayed()))
        onView(withText(HTTP_PATH)).check(matches(isDisplayed()))

        // saveToSubDir
        onView(withId(R.id.cb_sub_dir)).check(matches(isDisplayed()))
        onView(withId(R.id.input_sub_dir)).check(matches(isDisplayed()))
        onView(withId(R.id.cb_sub_dir)).perform(click())
        onView(withId(R.id.et_sub_dir)).perform(click())
        onView(withId(R.id.et_sub_dir)).perform(typeTextIntoFocusedView("subDir"))
        closeSoftKeyboard()

        // buttons
        onView(withId(R.id.btn_gallery)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_camera)).check(matches(isDisplayed()))
    }

    private fun editLanConfig() {
        // click on item menu button
        onView(withId(R.id.rv_configs)).perform(
                RecyclerViewActions.actionOnItemAtPosition<ConfigViewHolder>(0,
                        clickChildViewWithId(R.id.iv_menu)))

        // check popup menu items
        onView(withText(R.string.menu_delete)).check(matches(isDisplayed()))
        onView(withText(R.string.menu_edit)).check(matches(isDisplayed()))

        // click on edit menu
        onView(withText(R.string.menu_edit)).perform(click())
        closeSoftKeyboard()

        // check is fields filled
        onView(withText(LAN_ALIAS)).check(matches(isDisplayed()))
        onView(withText(LAN_PATH)).check(matches(isDisplayed()))
        onView(withText(DOMAIN)).check(matches(isDisplayed()))
        onView(withText(USERNAME)).check(matches(isDisplayed()))
        onView(withText(PASSWORD)).check(matches(isDisplayed()))

        onView(withId(R.id.menu_item_done)).perform(click())
    }

    private fun editFtpConfig() {
        // click on item menu button
        onView(withId(R.id.rv_configs)).perform(
                RecyclerViewActions.actionOnItemAtPosition<ConfigViewHolder>(1,
                        clickChildViewWithId(R.id.iv_menu)))

        // check popup menu items
        onView(withText(R.string.menu_delete)).check(matches(isDisplayed()))
        onView(withText(R.string.menu_edit)).check(matches(isDisplayed()))

        // click on edit menu
        onView(withText(R.string.menu_edit)).perform(click())
        closeSoftKeyboard()

        // check is fields filled
        onView(withText(FTP_ALIAS)).check(matches(isDisplayed()))
        onView(withText(FTP_PATH)).check(matches(isDisplayed()))
        onView(withText(USERNAME)).check(matches(isDisplayed()))
        onView(withText(PASSWORD)).check(matches(isDisplayed()))

        onView(withId(R.id.menu_item_done)).perform(click())
    }

    private fun editHttpConfig() {
        // click on item menu button
        onView(withId(R.id.rv_configs)).perform(
                RecyclerViewActions.actionOnItemAtPosition<ConfigViewHolder>(2,
                        clickChildViewWithId(R.id.iv_menu)))

        // check popup menu items
        onView(withText(R.string.menu_delete)).check(matches(isDisplayed()))
        onView(withText(R.string.menu_edit)).check(matches(isDisplayed()))

        // click on edit menu
        onView(withText(R.string.menu_edit)).perform(click())
        closeSoftKeyboard()

        // check is fields filled
        onView(withText(HTTP_ALIAS)).check(matches(isDisplayed()))
        onView(withText(HTTP_PATH)).check(matches(isDisplayed()))
        onView(withText(FORM_PARAM)).check(matches(isDisplayed()))
        onView(withText(HEADER)).check(matches(isDisplayed()))

        onView(withId(R.id.menu_item_done)).perform(click())
    }

    private fun checkSettingsScreen() {
        onView(withId(R.id.menu_item_settings)).perform(click())

        onView(withText(R.string.settings_delete_file_success_uploaded_title))
                .check(matches(isDisplayed()))
        onView(withText(R.string.settings_delete_file_success_uploaded_summary))
                .check(matches(isDisplayed()))

        onView(withText(R.string.settings_keep_screen_on_title)).check(matches(isDisplayed()))
        onView(withText(R.string.settings_keep_screen_on_summary)).check(matches(isDisplayed()))

        if (Build.VERSION.SDK_INT >= 24) {
            onView(withText(R.string.settings_summarise_notifications_title))
                    .check(matches(isDisplayed()))
            onView(withText(R.string.settings_summarise_notifications_summary))
                    .check(matches(isDisplayed()))
        }
    }
}
