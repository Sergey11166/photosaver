package ru.sevoro.uploadservice_gdrive;

import android.content.Intent;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.FileContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;

import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadFile;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.UploadTask;

import java.io.IOException;
import java.util.Collections;

/**
 * Implements upload logic to google drive.
 *
 * @author Sergey Vorobyev
 */
public class GdriveUploadTask extends UploadTask {

    static final String PARAM_SUB_DIRECTORY = "PARAM_SUB_DIRECTORY";
    private final String TAG = "GdriveUploadTask";
    private GdriveUploadTaskParameters gdriveParams = null;

    @Override
    protected void init(UploadService service, Intent intent) throws IOException {
        super.init(service, intent);
        gdriveParams = intent.getParcelableExtra(GdriveUploadTaskParameters.PARAM_GDRIVE_TASK_PARAMETERS);
        Log.d(TAG, "gdriveParams: " + gdriveParams);
    }

    @Override
    protected void upload() throws Exception {
        final UploadFile uploadFile = params.files.get(0);
        final String filePath = uploadFile.getPath();
        final String account = gdriveParams.account;
        final String folderId = params.serverUrl;

        Log.d(TAG, "filePath: " + filePath);
        Log.d(TAG, "account: " + gdriveParams.account);

        final GoogleAccountCredential credential = GoogleAccountCredential
                .usingOAuth2(service, Collections.singletonList(DriveScopes.DRIVE_FILE))
                .setBackOff(new ExponentialBackOff())
                .setSelectedAccountName(account);

        final Drive drive = new Drive.Builder(AndroidHttp.newCompatibleTransport(),
                JacksonFactory.getDefaultInstance(), credential)
                .setApplicationName("GoogleDriveUploadService")
                .build();

        final String[] splitFilePath = filePath.split("/");
        final String fileName = splitFilePath[splitFilePath.length - 1];

        final File meta = new File();
        meta.setName(fileName);
        meta.setParents(Collections.singletonList(folderId));

        final java.io.File file = new java.io.File(filePath);
        totalBytes = file.length();
        final FileContent content = new FileContent("image/jpeg", file);

        final Drive.Files.Create create = drive.files().create(meta, content);
        create.getMediaHttpUploader().setChunkSize(262144);
        create.getMediaHttpUploader().setProgressListener(new MediaHttpUploaderProgressListener() {
            @Override
            public void progressChanged(MediaHttpUploader uploader) throws IOException {
                switch (uploader.getUploadState()) {
                    case INITIATION_COMPLETE:
                        Log.d(TAG, "upload started");
                        broadcastProgress(0, totalBytes);
                        break;

                    case MEDIA_IN_PROGRESS:
                        Log.d(TAG, "upload progress = " + uploader.getProgress());
                        broadcastProgress(uploader.getNumBytesUploaded(), totalBytes);
                        break;

                    case MEDIA_COMPLETE:
                        Log.d(TAG, "upload completed");
                        addSuccessfullyUploadedFile(uploadFile);
                        broadcastCompleted(new ServerResponse(TASK_COMPLETED_SUCCESSFULLY,
                                EMPTY_RESPONSE, null));
                        break;
                }
            }
        });
        create.execute();
    }
}
