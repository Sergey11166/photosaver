package ru.sevoro.uploadservice_gdrive;

import android.content.Context;
import android.content.Intent;

import net.gotev.uploadservice.UploadFile;
import net.gotev.uploadservice.UploadRequest;
import net.gotev.uploadservice.UploadTask;

import java.io.FileNotFoundException;

/**
 * @author Sergey Vorobyev
 */

public class GdriveUploadRequest extends UploadRequest<GdriveUploadRequest> {

    private GdriveUploadTaskParameters gdriveParams = new GdriveUploadTaskParameters();

    /**
     * Creates a new upload request.
     *
     * @param context  application context
     * @param uploadId unique ID to assign to this upload request. If is null or empty, a random
     *                 UUID will be automatically generated. It's used in the broadcast receiver
     *                 when receiving updates.
     * @param path     path to the folder in the google drive
     * @throws IllegalArgumentException if one or more arguments are not valid
     */
    public GdriveUploadRequest(Context context, String uploadId, String path) throws IllegalArgumentException {
        super(context, uploadId, path);
    }

    @Override
    protected Class<? extends UploadTask> getTaskClass() {
        return GdriveUploadTask.class;
    }

    @Override
    protected void initializeIntent(Intent intent) {
        super.initializeIntent(intent);
        intent.putExtra(GdriveUploadTaskParameters.PARAM_GDRIVE_TASK_PARAMETERS, gdriveParams);
    }

    /**
     * Setup google drive account
     *
     * @return {@link GdriveUploadRequest}
     */
    public GdriveUploadRequest setAccount(final String account) {
        if (account == null || account.equals("")) {
            throw new IllegalStateException("Specify google drive account");
        }

        gdriveParams.account = account;
        return this;
    }

    /**
     * Add file to be uploaded
     *
     * @param filePath path to the local file on the device
     * @param subDir   sub directory in google drive folder
     * @return {@link GdriveUploadRequest}
     * @throws FileNotFoundException if the local file does not exist
     */
    public GdriveUploadRequest addFileToUpload(final String filePath, final String subDir) throws FileNotFoundException {
        final UploadFile file = new UploadFile(filePath);

        if (subDir != null) {
            file.setProperty(GdriveUploadTask.PARAM_SUB_DIRECTORY, subDir);
        }

        params.files.add(file);
        return this;
    }

    @Override
    public String startUpload() {
        if (params.files.isEmpty()) {
            throw new IllegalArgumentException("Add at least one file to start google drive upload!");
        }

        return super.startUpload();
    }
}
