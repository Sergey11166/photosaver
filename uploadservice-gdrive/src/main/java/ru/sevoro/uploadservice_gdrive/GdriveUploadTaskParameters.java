package ru.sevoro.uploadservice_gdrive;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Google Drive upload parameters.
 *
 * @author Sergey Vorobyev
 */
public class GdriveUploadTaskParameters implements Parcelable {

    public static final Creator<GdriveUploadTaskParameters> CREATOR = new Creator<GdriveUploadTaskParameters>() {
        @Override
        public GdriveUploadTaskParameters createFromParcel(Parcel in) {
            return new GdriveUploadTaskParameters(in);
        }

        @Override
        public GdriveUploadTaskParameters[] newArray(int size) {
            return new GdriveUploadTaskParameters[size];
        }
    };
    static final String PARAM_GDRIVE_TASK_PARAMETERS = "googleDriveTaskParameters";
    String account;

    GdriveUploadTaskParameters() {
    }

    protected GdriveUploadTaskParameters(Parcel in) {
        account = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(account);
    }

    @Override
    public String toString() {
        return "GdriveUploadTaskParameters{" +
                "domain='" + account + '\'' +
                '}';
    }
}
